﻿unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Math, StdCtrls, ComCtrls, sRichEdit, Buttons, sBitBtn, sEdit,
  sSpinEdit, sRadioButton, sGroupBox;

type
  TOilSemForm = class(TForm)
    sGroupBox1: TsGroupBox;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    sDecimalSpinEdit1: TsDecimalSpinEdit;
    sGroupBox2: TsGroupBox;
    sDecimalSpinEdit2: TsDecimalSpinEdit;
    sBitBtn1: TsBitBtn;
    sGroupBox3: TsGroupBox;
    sRichEdit1: TsRichEdit;
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OilSemForm: TOilSemForm;

implementation

{$R *.dfm}

uses DecimalRounding_JH1;

procedure TOilSemForm.sBitBtn1Click(Sender: TObject);
var
  m1, m2: Extended;                 // массы навесок 1,2
  mc1, mc2: Extended;               // массы колб 1,2
  mcOil1, mcOil2: Extended;         // массы колб с жиром 1,2
  Oil_nat, Oil_acb: Extended;       // жир на нат.влагу/а.с.в.
  NewOil_nat, NewOil_acb: Extended; // новые значения жира на нат.влагу/а.с.в.
  Oil_1_nat, Oil_2_nat: Extended;   // жир на нат.влагу 1,2
  Oil_1_acb, Oil_2_acb: Extended;   // жир на а.с.в. 1,2
  Vlaga: Extended;                  // влажность/Влага
  Delta: Extended;                  // разбег параллелей
  Limited: Extended;                // предел / для семян 0.50
  i, j: Integer;
  znak: string;
begin
  Vlaga := sDecimalSpinEdit2.Value;
  // Считываем "масличку" и конвертируем
  // Тут перевод масличности нат.влага --->>> а.с.в.
  if (sRadioButton1.Checked = True) then
    begin
      Oil_nat := sDecimalSpinEdit1.Value;
      Oil_acb := Oil_nat * 100 / ( 100 - Vlaga );
      Oil_acb := DecimalRoundExt(Oil_acb, 2, drHalfPos);
    end;
  // Тут перевод масличности а.с.в. --->>> нат.влага
  if (sRadioButton2.Checked = True) then
    begin
      Oil_acb := sDecimalSpinEdit1.Value;
      Oil_nat := Oil_acb * ( 100 - Vlaga ) / 100;
      Oil_nat := DecimalRoundExt(Oil_nat, 2, drHalfPos);
    end;

  // Для семян предел = 0.50
  Limited := 0.50;

  // Большой цикл для гибкости
  repeat
    // Сгенерируем параллели масличностей
    repeat
      i := RandomRange(0, 100);
      j := Random(2);
      if ( j = 0 ) then
        begin
          Oil_1_acb := Oil_acb - 0.51 * Limited * i / 100;
          Oil_2_acb := Oil_acb + 0.49 * Limited * i / 100;
        end
      else
        begin
          Oil_2_acb := Oil_acb - 0.51 * Limited * i / 100;
          Oil_1_acb := Oil_acb + 0.49 * Limited * i / 100;
        end;
      // Округлим до сотых
      Oil_1_acb := DecimalRoundExt(Oil_1_acb, 2, drHalfPos);
      Oil_2_acb := DecimalRoundExt(Oil_2_acb, 2, drHalfPos);
      Delta := Abs(Oil_1_acb - Oil_2_acb);
    until Delta <= Limited;

    // найдем параллели на нат.влагу
    Oil_1_nat := Oil_1_acb * ( 100 - Vlaga ) / 100;
    Oil_2_nat := Oil_2_acb * ( 100 - Vlaga ) / 100;
    Oil_1_nat := DecimalRoundExt(Oil_1_nat, 2, drHalfPos);
    Oil_2_nat := DecimalRoundExt(Oil_2_nat, 2, drHalfPos);

    // генерируем мыссы навесок
    m1 := RandomRange(80000, 100000) / 10000;
    m2 := RandomRange(80000, 100000) / 10000;

    // генерируем массы колб
    mc1 := RandomRange(1200000, 1500000) / 10000;
    mc2 := RandomRange(1200000, 1500000) / 10000;

    // найдем массу колб с жиром 1,2
    mcOil1 := ( Oil_1_nat * m1 + mc1 * 100 ) / 100;
    mcOil2 := ( Oil_2_nat * m2 + mc2 * 100 ) / 100;
    mcOil1 := DecimalRoundExt(mcOil1, 4, drHalfPos);
    mcOil2 := DecimalRoundExt(mcOil2, 4, drHalfPos);

    // Теперь по новым данным надо сделать прямой расчет
    // и проверить совпадение данных
    // 1-е найдем массу жира и масличность на нат.влагу
    Oil_1_nat := 100 * ( mcOil1 - mc1 ) / m1;
    Oil_2_nat := 100 * ( mcOil2 - mc2 ) / m2;
    // округлим до сотых
    Oil_1_nat := DecimalRoundExt(Oil_1_nat, 2, drHalfPos);
    Oil_2_nat := DecimalRoundExt(Oil_2_nat, 2, drHalfPos);
    Oil_1_acb := Oil_1_nat * 100 / ( 100 - Vlaga );
    Oil_2_acb := Oil_2_nat * 100 / ( 100 - Vlaga );
    Oil_1_acb := DecimalRoundExt(Oil_1_acb, 2, drHalfPos);
    Oil_2_acb := DecimalRoundExt(Oil_2_acb, 2, drHalfPos);
    // найдем новую масличку на нат.влагу
    NewOil_nat := ( Oil_1_nat + Oil_2_nat ) / 2;
    NewOil_nat := DecimalRoundExt(NewOil_nat, 2, drHalfPos);
    // найдем новую масличку на а.с.в.
    NewOil_acb := NewOil_nat * 100 / ( 100 - Vlaga );
    NewOil_acb := DecimalRoundExt(NewOil_acb, 2, drHalfPos);
    Delta := Abs(Oil_1_acb - Oil_2_acb);
  until ( ( Abs(NewOil_acb - Oil_acb) < 0.001 ) and  ( Delta <= Limited ) );

  if ( Delta = Limited ) then znak := '='
  else znak := '<';

  // Сам вывод в поле вывода
  sRichEdit1.Clear;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.Lines.Add('    m, г.   m(колбы), г.   m(колбы+жир), г.   Xнат., %    W, %    Xа.с.в.,%  Разница  Xср.(а.с.в.), %       Вывод');
  sRichEdit1.Lines.Add('');
  // 1-я параллель
  sRichEdit1.Lines.Add('1: '
  + FloatToStrF(m1, ffFixed, 6, 4)
  + '    '
  + FloatToStrF(mc1, ffFixed, 8, 4)
  + '         '
  + FloatToStrF(mcOil1, ffFixed, 8, 4)
  + '          '
  + FloatToStrF(Oil_1_nat, ffFixed, 5, 2)
  + '     '
  + FloatToStrF(Vlaga, ffFixed, 6, 2)
  + '      '
  + FloatToStrF(Oil_1_acb, ffFixed, 6, 2)
  + '       '
  + FloatToStrF(Delta, ffFixed, 6, 2)
  + '       '
  + FloatToStrF(Oil_acb, ffFixed, 6, 2)
  + '        '
  + FloatToStrF(Delta, ffFixed, 6, 2)
  + ' ' + znak + ' '
  + FloatToStrF(Limited, ffFixed, 6, 2)
  );
  // 2-я параллель
  sRichEdit1.Lines.Add('2: '
  + FloatToStrF(m2, ffFixed, 6, 4)
  + '    '
  + FloatToStrF(mc2, ffFixed, 8, 4)
  + '         '
  + FloatToStrF(mcOil2, ffFixed, 8, 4)
  + '          '
  + FloatToStrF(Oil_2_nat, ffFixed, 5, 2)
  + '                '
  + FloatToStrF(Oil_2_acb, ffFixed, 6, 2));

  // Среднее
  sRichEdit1.Lines.Add('');
  sRichEdit1.SelAttributes.Color := clBlue;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.Lines.Add('                              Проверка средних: '
  + FloatToStrF(Oil_nat, ffFixed, 5, 2) +
  '                '
  + FloatToStrF(Oil_acb, ffFixed, 5, 2));
end;

end.
