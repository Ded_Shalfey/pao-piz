﻿unit Unit7;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Math, StdCtrls, ComCtrls, sRichEdit, Buttons, sBitBtn, sEdit,
  sSpinEdit, sRadioButton, sGroupBox, sLabel;

type
  TCellulosaForm = class(TForm)
    sGroupBox1: TsGroupBox;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    sDecimalSpinEdit1: TsDecimalSpinEdit;
    sGroupBox2: TsGroupBox;
    sDecimalSpinEdit2: TsDecimalSpinEdit;
    sBitBtn1: TsBitBtn;
    sGroupBox3: TsGroupBox;
    sRichEdit1: TsRichEdit;
    sGroupBox4: TsGroupBox;
    sRadioButton3: TsRadioButton;
    sRadioButton4: TsRadioButton;
    sGroupBox5: TsGroupBox;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CellulosaForm: TCellulosaForm;

implementation

{$R *.dfm}

uses DecimalRounding_JH1;

procedure TCellulosaForm.sBitBtn1Click(Sender: TObject);
var
  m1, m2: Extended;                 // массы навесок 1,2
  mc1, mc2: Extended;               // массы бюксов с фильтром 1,2
  mcCel1, mcCel2: Extended;         // массы бюксов с фильтром и клетчаткой 1,2
  Cel_nat, Cel_acb: Extended;       // клетчатка на нат.влагу/а.с.в.
  NewCel_nat, NewCel_acb: Extended; // новые значения клетчатки на нат.влагу/а.с.в.
  Cel_1_nat, Cel_2_nat: Extended;   // клетчатка на нат.влагу 1,2
  Cel_1_acb, Cel_2_acb: Extended;   // клетчатка на а.с.в. 1,2
  Vlaga: Extended;                  // влажность/Влага
  Delta: Extended;                  // разбег параллелей
  Limited: Extended;                // предел
  i, j: Integer;
  znak: string;
  tigle1, tigle2: Extended;         // массы тиглей 1,2
  tigZola1, tigZola2: Extended;     // массы тиглей с золой 1,2
  Zola: Extended;                   // зола на нат.влагу
  Zola_acb: Extended;               // Зола на а.с.в.
begin
  Vlaga := sDecimalSpinEdit2.Value;
  // Считываем "клетчатку" и конвертируем
  // Тут перевод клетчатки нат.влага --->>> а.с.в.
  if (sRadioButton1.Checked = True) then
    begin
      Cel_nat := sDecimalSpinEdit1.Value;
      Cel_acb := Cel_nat * 100 / ( 100 - Vlaga );
      Cel_acb := DecimalRoundExt(Cel_acb, 2, drHalfPos);
    end;
  // Тут перевод клетчатки а.с.в. --->>> нат.влага
  if (sRadioButton2.Checked = True) then
    begin
      Cel_acb := sDecimalSpinEdit1.Value;
      Cel_nat := Cel_acb * ( 100 - Vlaga ) / 100;
      Cel_nat := DecimalRoundExt(Cel_nat, 2, drHalfPos);
    end;

  //Считаем предел
  Limited := Cel_acb * 0.05 + 0.34;

  // Большой цикл для гибкости
  repeat
    // Сгенерируем параллели клетчатки
    repeat
      i := RandomRange(0, 100);
      j := Random(2);
      if ( j = 0 ) then
        begin
          Cel_1_acb := Cel_acb - 0.51 * Limited * i / 100;
          Cel_2_acb := Cel_acb + 0.49 * Limited * i / 100;
        end
      else
        begin
          Cel_2_acb := Cel_acb - 0.51 * Limited * i / 100;
          Cel_1_acb := Cel_acb + 0.49 * Limited * i / 100;
        end;
      // Округлим до сотых
      Cel_1_acb := DecimalRoundExt(Cel_1_acb, 2, drHalfPos);
      Cel_2_acb := DecimalRoundExt(Cel_2_acb, 2, drHalfPos);
      Delta := Abs(Cel_1_acb - Cel_2_acb);
    until Delta <= Limited;

    // найдем параллели на нат.влагу
    Cel_1_nat := Cel_1_acb * ( 100 - Vlaga ) / 100;
    Cel_2_nat := Cel_2_acb * ( 100 - Vlaga ) / 100;
    Cel_1_nat := DecimalRoundExt(Cel_1_nat, 2, drHalfPos);
    Cel_2_nat := DecimalRoundExt(Cel_2_nat, 2, drHalfPos);

    // генерируем мыссы навесок
    m1 := RandomRange(9990, 10001) / 10000;
    m2 := RandomRange(9990, 10001) / 10000;

    // генерируем массы бюксов с фильтром
    mc1 := RandomRange(230000, 280000) / 10000;
    mc2 := RandomRange(230000, 280000) / 10000;

    // генерируем массы тиглей
    tigle1 := RandomRange(410000, 480000) / 10000;
    tigle2 := RandomRange(410000, 480000) / 10000;

    // теперь надо сгенерировать золу на а.с.в.
    // и перевести в нат.влагу
    // зола для шрота
    if (sRadioButton3.Checked = True) then
      begin
        Zola_acb := RandomRange(570, 650) / 100;
        Zola := Zola_acb * ( 100 - Vlaga) / 100;
      end;
    // зола для семян и лузги
    if (sRadioButton4.Checked = True) then
      begin
        Zola_acb := RandomRange(275, 320) / 100;
        Zola := Zola_acb * ( 100 - Vlaga) / 100;
      end;

    // найдем массу тигля с золой 1,2
    tigZola1 := tigle1 + m1 * Zola / 100;
    tigZola2 := tigle2 + m2 * Zola / 100;
    tigZola1 := DecimalRoundExt(tigZola1, 4, drHalfPos);
    tigZola2 := DecimalRoundExt(tigZola2, 4, drHalfPos);

    // найдем массу бюксов с фильтром и клетчаткой 1,2
    mcCel1 := ( Cel_1_nat * m1 + 100 * ( mc1 + tigZola1 - tigle1) ) / 100;
    mcCel2 := ( Cel_2_nat * m2 + 100 * ( mc2 + tigZola2 - tigle2) ) / 100;
    mcCel1 := DecimalRoundExt(mcCel1, 4, drHalfPos);
    mcCel2 := DecimalRoundExt(mcCel2, 4, drHalfPos);

    // Теперь по новым данным надо сделать прямой расчет
    // и проверить совпадение данных
    // 1-е найдем массу клетчатки и клетчатку на нат.влагу
    Cel_1_nat := 100 * ( mcCel1 - mc1 - tigZola1 + tigle1 ) / m1;
    Cel_2_nat := 100 * ( mcCel2 - mc2 - tigZola2 + tigle2 ) / m2;
    // округлим до сотых
    Cel_1_nat := DecimalRoundExt(Cel_1_nat, 2, drHalfPos);
    Cel_2_nat := DecimalRoundExt(Cel_2_nat, 2, drHalfPos);
    Cel_1_acb := Cel_1_nat * 100 / ( 100 - Vlaga );
    Cel_2_acb := Cel_2_nat * 100 / ( 100 - Vlaga );
    Cel_1_acb := DecimalRoundExt(Cel_1_acb, 2, drHalfPos);
    Cel_2_acb := DecimalRoundExt(Cel_2_acb, 2, drHalfPos);
    // найдем новую клетчатку на нат.влагу
    NewCel_nat := ( Cel_1_nat + Cel_2_nat ) / 2;
    NewCel_nat := DecimalRoundExt(NewCel_nat, 2, drHalfPos);
    // найдем новую клетчатку на а.с.в.
    NewCel_acb := NewCel_nat * 100 / ( 100 - Vlaga );
    NewCel_acb := DecimalRoundExt(NewCel_acb, 2, drHalfPos);
    Delta := Abs(Cel_1_acb - Cel_2_acb);
  until ( ( Abs(NewCel_acb - Cel_acb) < 0.001 ) and ( Delta <= Limited ) );

  if ( Delta = Limited ) then znak := '='
  else znak := '<';

  // Сам вывод в поле вывода
  sRichEdit1.Clear;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.Lines.Add('    m1, г.   m2, г.   m3, г.   m4, г.   m5, г.   Xнат., %    W, %    Xа.с.в.,%  Разница  Xср.(а.с.в.), %    Вывод');
  sRichEdit1.Lines.Add('');
  // 1-я параллель
  sRichEdit1.Lines.Add('1: '
  + FloatToStrF(m1, ffFixed, 6, 4)
  + '   '
  + FloatToStrF(mc1, ffFixed, 8, 4)
  + '  '
  + FloatToStrF(mcCel1, ffFixed, 8, 4)
  + '  '
  + FloatToStrF(tigle1, ffFixed, 8, 4)
  + '  '
  + FloatToStrF(tigZola1, ffFixed, 8, 4)
  + '    '
  + FloatToStrF(Cel_1_nat, ffFixed, 5, 2)
  + '     '
  + FloatToStrF(Vlaga, ffFixed, 6, 2)
  + '      '
  + FloatToStrF(Cel_1_acb, ffFixed, 6, 2)
  + '      '
  + FloatToStrF(Delta, ffFixed, 6, 2)
  + '       '
  + FloatToStrF(Cel_acb, ffFixed, 6, 2)
  + '       '
  + FloatToStrF(Delta, ffFixed, 6, 2)
  + ' ' + znak + ' '
  + FloatToStrF(Limited, ffFixed, 6, 2)
  );
  // 2-я параллель
  sRichEdit1.Lines.Add('2: '
  + FloatToStrF(m2, ffFixed, 6, 4)
  + '   '
  + FloatToStrF(mc2, ffFixed, 8, 4)
  + '  '
  + FloatToStrF(mcCel2, ffFixed, 8, 4)
  + '  '
  + FloatToStrF(tigle2, ffFixed, 8, 4)
  + '  '
  + FloatToStrF(tigZola2, ffFixed, 8, 4)
  + '    '
  + FloatToStrF(Cel_2_nat, ffFixed, 5, 2)
  + '                '
  + FloatToStrF(Cel_2_acb, ffFixed, 6, 2));

  // Среднее
  sRichEdit1.Lines.Add('');
  sRichEdit1.SelAttributes.Color := clBlue;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.Lines.Add('                                Проверка средних: '
  + FloatToStrF(Cel_nat, ffFixed, 5, 2) +
  '                '
  + FloatToStrF(Cel_acb, ffFixed, 5, 2));
end;

end.
