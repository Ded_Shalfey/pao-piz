﻿unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, sRichEdit, Buttons, sBitBtn, sEdit, sSpinEdit,
  sLabel, math, sGroupBox;

type
  TProteinForm = class(TForm)
    sLabel1: TsLabel;
    sDecimalSpinEdit1: TsDecimalSpinEdit;
    sLabel2: TsLabel;
    sDecimalSpinEdit2: TsDecimalSpinEdit;
    sLabel3: TsLabel;
    sDecimalSpinEdit3: TsDecimalSpinEdit;
    sLabel4: TsLabel;
    sDecimalSpinEdit4: TsDecimalSpinEdit;
    sLabel5: TsLabel;
    sDecimalSpinEdit5: TsDecimalSpinEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    sRichEdit1: TsRichEdit;
    sGroupBox1: TsGroupBox;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sDecimalSpinEdit6: TsDecimalSpinEdit;
    sLabel8: TsLabel;
    sDecimalSpinEdit7: TsDecimalSpinEdit;
    sLabel9: TsLabel;
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProteinForm: TProteinForm;

implementation

{$R *.dfm}

uses DecimalRounding_JH1;

procedure TProteinForm.sBitBtn1Click(Sender: TObject);
var
  nat, acb, vlaga_: Extended;
begin
  acb := sDecimalSpinEdit5.Value;
  vlaga_ := sDecimalSpinEdit2.Value;
  nat := acb * ( 100 - vlaga_)/100;
  nat := DecimalRoundExt(nat, 2, drHalfPos);
  sDecimalSpinEdit1.Value := nat;
end;

procedure TProteinForm.sBitBtn2Click(Sender: TObject);
var
  prot, prot_acb: Extended;         // протеин на нат. влагу/а.с.в.
  prot1, prot2, prot1new, prot2new: Extended;           // параллели протеина
  protNew: Extended;                // новое значение протеина для проверки
  vlaga: Extended;                  // влага и лет. вещества
  limited, Delta: Extended;         // расхождение между параллелями
  m1, m2, v1, v2, m_min, m_max: Extended;
  i, j: Integer;
  znak: string;
begin
  // считываем данные из полей ввода
  // и заносим в соотв. переменные
  prot := sDecimalSpinEdit1.Value;
  vlaga := sDecimalSpinEdit2.Value;
  m_min := sDecimalSpinEdit3.Value;
  m_max := sDecimalSpinEdit4.Value;

  // На основе среднего протеина посчитаем
  // максимальное расхождение между параллелями
  // Округлим до 2-го знака
  limited := DecimalRoundExt(0.3 + 0.008 * prot, 2, drHalfPos);

  if ( sGroupBox1.Checked = True) then
    begin
      m1 := sDecimalSpinEdit6.Value;
      m2 := sDecimalSpinEdit7.Value;
      repeat
        repeat
          // Тут рандомно раскидаем параллели на основе limited
          // генерация простая
          i := Random(100);
          j := Random(2);
          if ( j = 0) then
            begin
              prot1 := prot - DecimalRoundExt(0.51 * limited * i/100, 2, drHalfPos);
              prot2 := prot + DecimalRoundExt(0.49 * limited * i/100, 2, drHalfPos);
            end
          else
            begin
              prot2 := prot - DecimalRoundExt(0.51 * limited * i/100, 2, drHalfPos);
              prot1 := prot + DecimalRoundExt(0.49 * limited * i/100, 2, drHalfPos);
            end;
          Delta := Abs(prot1 - prot2);
        until Delta <= limited;

        // находим объем v1
        v1 := prot1 * m1/0.875;

        // находим объем v2
        v2 := prot2 * m2/0.875;

        // округляем объемы до 1-го знака после запятой
        // с учетом что цена деления 0.1
        v1 := DecimalRoundExt(v1, 1, drHalfPos);
        v2 := DecimalRoundExt(v2, 1, drHalfPos);

        // перерасчитываем новый протеин с округление до 2-го знака
        prot1new := v1 * 0.875/m1;
        prot2new := v2 * 0.875/m2;
        prot1new := DecimalRoundExt(prot1new, 2, drHalfPos);
        prot2new := DecimalRoundExt(prot2new, 2, drHalfPos);
        if prot1new > prot1 then
          begin
            v1 := v1 - 0.1;
          end
        else
          begin
            v1 := v1 + 0.1;
          end;
        if prot2new > prot2 then
          begin
            v2:= v2 - 0.1;
          end
        else
          begin
            v2 := v2 + 0.1;
          end;
        prot1 := v1 * 0.875/m1;
        prot2 := v2 * 0.875/m2;
        prot1 := DecimalRoundExt(prot1, 2, drHalfPos);
        prot2 := DecimalRoundExt(prot2, 2, drHalfPos);

        // Находим новый протеин и округляем до 2-го знака
        protNew := (prot1 + prot2)/2;
        protNew := DecimalRoundExt(protNew, 2, drHalfPos);

        Delta := Abs(prot1 - prot2);
      until ( ( Abs(protNew - prot) <= 0.001 ) and ( Delta <= limited ) );
    end
  else
    begin
      repeat // цикл 0000
        // входим в цикл
        repeat
          // Тут рандомно раскидаем параллели на основе limited
          // генерация простая
          i := Random(10);
          j := Random(2);
          if ( j = 0) then
            begin
              prot1 := prot - DecimalRoundExt(0.51 * limited * i/10, 2, drHalfPos);
              prot2 := prot + DecimalRoundExt(0.49 * limited * i/10, 2, drHalfPos);
            end
          else
            begin
              prot2 := prot - DecimalRoundExt(0.51 * limited * i/10, 2, drHalfPos);
              prot1 := prot + DecimalRoundExt(0.49 * limited * i/10, 2, drHalfPos);
            end;
          Delta := Abs(prot1 - prot2);
        until Delta <= limited;

        // генерируем массу навески 1
        repeat
          m1 := m_min + RandomRange(100, 3000)/10000;
        until m1 <= m_max;

        // генерируем массу навески 2
        repeat
          m2 := m_min + RandomRange(100, 3000)/10000;
        until m2 <= m_max;

        // находим объем v1
        v1 := prot1 * m1/0.875;

        // находим объем v2
        v2 := prot2 * m2/0.875;

        // округляем объемы до 1-го знака после запятой
        // с учетом что цена деления 0.1
        v1 := DecimalRoundExt(v1, 1, drHalfPos);
        v2 := DecimalRoundExt(v2, 1, drHalfPos);

        // перерасчитываем новый протеин с округление до 2-го знака
        prot1 := v1 * 0.875/m1;
        prot2 := v2 * 0.875/m2;
        prot1 := DecimalRoundExt(prot1, 2, drHalfPos);
        prot2 := DecimalRoundExt(prot2, 2, drHalfPos);

        // Находим новый протеин и округляем до 2-го знака
        protNew := (prot1 + prot2)/2;
        protNew := DecimalRoundExt(protNew, 2, drHalfPos);
        Delta := Abs(prot1 - prot2);

      until ((Abs(protNew - prot) <= 0.001) and (Delta <= limited));
    end;

  prot_acb := prot * 100/(100 - vlaga);
  prot_acb := DecimalRoundExt(prot_acb, 2, drHalfPos);
  if ( Delta = Limited ) then znak := '='
  else znak := '<';

  // Тут сам вывод в форму
  sRichEdit1.Clear;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.Lines.Add('    m, г.  ' + ' V,  мл '
  + '   X, %   '
  + '  X, % сред.  ' + 'Предел  ' + '    Вывод   ' + '    W,%  ' + '  X, % на а.с.в.' + #13#10);

  // первая параллель
  sRichEdit1.Lines.Add('1: ' + FloatToStrF(m1, ffFixed, 6, 4)
  + '    ' + FloatToStrF(v1, ffFixed, 5, 1)
  + '    ' + FloatToStrF(prot1, ffFixed, 5, 2)
  + '       ' + FloatToStrF(prot, ffFixed, 5, 2)
  + '      ' + FloatToStrF(limited, ffFixed, 1, 2)
  + '    ' + FloatToStrF(Delta, ffFixed, 1, 2)
  + ' ' + znak + ' ' + FloatToStrF(limited, ffFixed, 1, 2)
  + '   ' + FloatToStrF(vlaga, ffFixed, 5, 2)
  + '        ' + FloatToStrF(prot_acb, ffFixed, 5, 2));

  // Вторая параллель
  sRichEdit1.Lines.Add('2: ' + FloatToStrF(m2, ffFixed, 6, 4)
  + '    ' + FloatToStrF(v2, ffFixed, 5, 1)
  + '    ' + FloatToStrF(prot2, ffFixed, 5, 2));

  sDecimalSpinEdit5.Value := prot_acb;
end;

end.
