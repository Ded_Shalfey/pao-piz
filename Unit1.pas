﻿unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, sSkinManager, sBitBtn, acAlphaHints, Buttons;

type
  TPAOPIZForm = class(TForm)
    sSkinManager1: TsSkinManager;
    sAlphaHints1: TsAlphaHints;
    Protein: TsBitBtn;
    OilContent: TsBitBtn;
    Phosfor: TsBitBtn;
    OilSem: TsBitBtn;
    Cellulose: TsBitBtn;
    Zola: TsBitBtn;
    Parall: TsBitBtn;
    RafGo: TsBitBtn;
    sBitBtn1: TsBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure ProteinClick(Sender: TObject);
    procedure OilContentClick(Sender: TObject);
    procedure PhosforClick(Sender: TObject);
    procedure OilSemClick(Sender: TObject);
    procedure CelluloseClick(Sender: TObject);
    procedure ZolaClick(Sender: TObject);
    procedure ParallClick(Sender: TObject);
    procedure RafGoClick(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PAOPIZForm: TPAOPIZForm;

implementation

uses DecimalRounding_JH1, Unit2, Unit3, Unit4, Unit5,
     Unit6, Unit7, Unit8, Unit9, Unit10, Unit11;

{$R *.dfm}

procedure TPAOPIZForm.CelluloseClick(Sender: TObject);
begin
  CellulosaForm.Show;
end;

procedure TPAOPIZForm.FormCreate(Sender: TObject);
begin
  {Application.CreateForm(TPromtForm, PromtForm);
    case PromtForm.ShowModal of
      mrOK:
        begin
          //
        end;
      mrCANCEL:
        begin
          Application.Minimize;
          PAOPIZForm.Hide;
          Application.Terminate;
        end;
    end;
    PromtForm.Free;}
  // Иницилизируем генератор случайных чисел
  Randomize;
end;

// запуск формы с "МАСЛИЧНОСТЬЮ"
procedure TPAOPIZForm.OilContentClick(Sender: TObject);
begin
  OilForm.Show;
end;

procedure TPAOPIZForm.OilSemClick(Sender: TObject);
begin
  OilSemForm.Show;
end;

// запуск формы с "ФОСФОРОМ"
procedure TPAOPIZForm.ParallClick(Sender: TObject);
begin
  W1W2Form.Show;
end;

procedure TPAOPIZForm.PhosforClick(Sender: TObject);
begin
  PhForm.Show;
  ///////
end;

// запуск формы с "ПРОТЕИНОМ"
procedure TPAOPIZForm.ProteinClick(Sender: TObject);
begin
  ProteinForm.Show;
end;

procedure TPAOPIZForm.RafGoClick(Sender: TObject);
begin
  RafForm.Show;
end;

procedure TPAOPIZForm.sBitBtn1Click(Sender: TObject);
begin
  RozlivForm.Show;
end;

procedure TPAOPIZForm.ZolaClick(Sender: TObject);
begin
  //ZolaForm.Show;
  //ShowMessage('На доработке!');
end;

end.
