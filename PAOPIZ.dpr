program PAOPIZ;

uses
  Forms,
  Unit1 in 'Unit1.pas' {PAOPIZForm},
  DecimalRounding_JH1 in 'DecimalRounding_JH1.pas',
  Unit2 in 'Unit2.pas',
  Unit3 in 'Unit3.pas' {ProteinForm},
  Unit4 in 'Unit4.pas' {OilForm},
  Unit5 in 'Unit5.pas' {PhForm},
  Unit6 in 'Unit6.pas' {OilSemForm},
  Unit7 in 'Unit7.pas' {CellulosaForm},
  Unit8 in 'Unit8.pas' {ZolaForm},
  Unit9 in 'Unit9.pas' {W1W2Form},
  Unit10 in 'Unit10.pas' {RafForm},
  Unit11 in 'Unit11.pas' {RozlivForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'PAO PIZ';
  Application.CreateForm(TPAOPIZForm, PAOPIZForm);
  Application.CreateForm(TProteinForm, ProteinForm);
  Application.CreateForm(TOilForm, OilForm);
  Application.CreateForm(TPhForm, PhForm);
  Application.CreateForm(TOilSemForm, OilSemForm);
  Application.CreateForm(TCellulosaForm, CellulosaForm);
  Application.CreateForm(TZolaForm, ZolaForm);
  Application.CreateForm(TW1W2Form, W1W2Form);
  Application.CreateForm(TRafForm, RafForm);
  Application.CreateForm(TRozlivForm, RozlivForm);
  Application.Run;
end.
