﻿unit Unit10;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, sSkinManager, ComCtrls, sPageControl,
  StdCtrls, sEdit, sSpinEdit, sLabel, sMemo, sGroupBox, sScrollBox,
  sCheckBox,IniFiles, sButton, Printers, Math, Buttons, sBitBtn, sRichEdit, sDialogs, sRadioButton;

type
  TRafForm = class(TForm)
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    sTabSheet4: TsTabSheet;
    sLabel1: TsLabel;
    concKOH: TsDecimalSpinEdit;
    sLabel2: TsLabel;
    concTS: TsDecimalSpinEdit;
    sScrollBox1: TsScrollBox;
    sGroupBox1: TsGroupBox;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    DecAcid1: TsDecimalSpinEdit;
    DecAlkaly1: TsDecimalSpinEdit;
    sGroupBox2: TsGroupBox;
    sGroupBox3: TsGroupBox;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    sDecimalSpinEdit3: TsDecimalSpinEdit;
    sDecimalSpinEdit4: TsDecimalSpinEdit;
    sGroupBox4: TsGroupBox;
    sLabel7: TsLabel;
    sDecimalSpinEdit5: TsDecimalSpinEdit;
    sGroupBox5: TsGroupBox;
    sLabel8: TsLabel;
    sDecimalSpinEdit6: TsDecimalSpinEdit;
    sGroupBox6: TsGroupBox;
    sGroupBox7: TsGroupBox;
    sGroupBox8: TsGroupBox;
    sTabSheet5: TsTabSheet;
    sTabSheet6: TsTabSheet;
    sScrollBox2: TsScrollBox;
    sGroupBox16: TsGroupBox;
    sLabel19: TsLabel;
    sLabel20: TsLabel;
    DecAcid2: TsDecimalSpinEdit;
    DecAlkaly2: TsDecimalSpinEdit;
    sGroupBox31: TsGroupBox;
    sLabel35: TsLabel;
    BaseBak_kch: TsDecimalSpinEdit;
    sLabel36: TsLabel;
    BaseBak_pch: TsDecimalSpinEdit;
    sLabel37: TsLabel;
    BaseBak_vlaga: TsDecimalSpinEdit;
    sLabel38: TsLabel;
    sDecimalSpinEdit36: TsDecimalSpinEdit;
    sLabel9: TsLabel;
    sLabel10: TsLabel;
    sDecimalSpinEdit7: TsDecimalSpinEdit;
    sDecimalSpinEdit8: TsDecimalSpinEdit;
    sLabel11: TsLabel;
    sLabel12: TsLabel;
    sDecimalSpinEdit9: TsDecimalSpinEdit;
    sDecimalSpinEdit10: TsDecimalSpinEdit;
    sLabel13: TsLabel;
    sLabel14: TsLabel;
    sDecimalSpinEdit11: TsDecimalSpinEdit;
    sDecimalSpinEdit12: TsDecimalSpinEdit;
    sScrollBox3: TsScrollBox;
    sGroupBox32: TsGroupBox;
    sGroupBox33: TsGroupBox;
    sLabel54: TsLabel;
    sLabel55: TsLabel;
    sDecimalSpinEdit52: TsDecimalSpinEdit;
    sDecimalSpinEdit53: TsDecimalSpinEdit;
    sGroupBox34: TsGroupBox;
    sLabel57: TsLabel;
    sDecimalSpinEdit55: TsDecimalSpinEdit;
    sGroupBox35: TsGroupBox;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sLabel61: TsLabel;
    sDecimalSpinEdit57: TsDecimalSpinEdit;
    sDecimalSpinEdit58: TsDecimalSpinEdit;
    sDecimalSpinEdit59: TsDecimalSpinEdit;
    sGroupBox36: TsGroupBox;
    sLabel62: TsLabel;
    sLabel63: TsLabel;
    sDecimalSpinEdit60: TsDecimalSpinEdit;
    sDecimalSpinEdit61: TsDecimalSpinEdit;
    sGroupBox37: TsGroupBox;
    sLabel56: TsLabel;
    sLabel58: TsLabel;
    sDecimalSpinEdit54: TsDecimalSpinEdit;
    sDecimalSpinEdit56: TsDecimalSpinEdit;
    sGroupBox38: TsGroupBox;
    sLabel64: TsLabel;
    sDecimalSpinEdit62: TsDecimalSpinEdit;
    sGroupBox39: TsGroupBox;
    sLabel65: TsLabel;
    sLabel66: TsLabel;
    sDecimalSpinEdit63: TsDecimalSpinEdit;
    sDecimalSpinEdit64: TsDecimalSpinEdit;
    sGroupBox40: TsGroupBox;
    sLabel67: TsLabel;
    sDecimalSpinEdit65: TsDecimalSpinEdit;
    sGroupBox41: TsGroupBox;
    sLabel68: TsLabel;
    sLabel69: TsLabel;
    sDecimalSpinEdit66: TsDecimalSpinEdit;
    sDecimalSpinEdit67: TsDecimalSpinEdit;
    sGroupBox42: TsGroupBox;
    sLabel70: TsLabel;
    sDecimalSpinEdit68: TsDecimalSpinEdit;
    sGroupBox9: TsGroupBox;
    sGroupBox10: TsGroupBox;
    sLabel15: TsLabel;
    sLabel16: TsLabel;
    sDecimalSpinEdit13: TsDecimalSpinEdit;
    sDecimalSpinEdit14: TsDecimalSpinEdit;
    sGroupBox11: TsGroupBox;
    sLabel17: TsLabel;
    sDecimalSpinEdit15: TsDecimalSpinEdit;
    sGroupBox12: TsGroupBox;
    sLabel18: TsLabel;
    sLabel21: TsLabel;
    sDecimalSpinEdit16: TsDecimalSpinEdit;
    sDecimalSpinEdit19: TsDecimalSpinEdit;
    sGroupBox13: TsGroupBox;
    sLabel22: TsLabel;
    sLabel23: TsLabel;
    sDecimalSpinEdit20: TsDecimalSpinEdit;
    sDecimalSpinEdit21: TsDecimalSpinEdit;
    sGroupBox14: TsGroupBox;
    sLabel24: TsLabel;
    sLabel25: TsLabel;
    sDecimalSpinEdit22: TsDecimalSpinEdit;
    sDecimalSpinEdit23: TsDecimalSpinEdit;
    sGroupBox15: TsGroupBox;
    sLabel26: TsLabel;
    sLabel27: TsLabel;
    sDecimalSpinEdit24: TsDecimalSpinEdit;
    sDecimalSpinEdit25: TsDecimalSpinEdit;
    sGroupBox17: TsGroupBox;
    sGroupBox18: TsGroupBox;
    sLabel28: TsLabel;
    sLabel29: TsLabel;
    sDecimalSpinEdit26: TsDecimalSpinEdit;
    sDecimalSpinEdit27: TsDecimalSpinEdit;
    sGroupBox19: TsGroupBox;
    sLabel30: TsLabel;
    sDecimalSpinEdit28: TsDecimalSpinEdit;
    sGroupBox20: TsGroupBox;
    sLabel31: TsLabel;
    sLabel32: TsLabel;
    sDecimalSpinEdit29: TsDecimalSpinEdit;
    sDecimalSpinEdit30: TsDecimalSpinEdit;
    sGroupBox21: TsGroupBox;
    sLabel33: TsLabel;
    sLabel34: TsLabel;
    sDecimalSpinEdit31: TsDecimalSpinEdit;
    sDecimalSpinEdit32: TsDecimalSpinEdit;
    sGroupBox22: TsGroupBox;
    sLabel39: TsLabel;
    sLabel40: TsLabel;
    sDecimalSpinEdit37: TsDecimalSpinEdit;
    sDecimalSpinEdit38: TsDecimalSpinEdit;
    sGroupBox23: TsGroupBox;
    sLabel41: TsLabel;
    sLabel42: TsLabel;
    sDecimalSpinEdit39: TsDecimalSpinEdit;
    sDecimalSpinEdit40: TsDecimalSpinEdit;
    sGroupBox24: TsGroupBox;
    sGroupBox25: TsGroupBox;
    sLabel43: TsLabel;
    sLabel44: TsLabel;
    sDecimalSpinEdit41: TsDecimalSpinEdit;
    sDecimalSpinEdit42: TsDecimalSpinEdit;
    sGroupBox26: TsGroupBox;
    sLabel45: TsLabel;
    sDecimalSpinEdit43: TsDecimalSpinEdit;
    sGroupBox27: TsGroupBox;
    sLabel46: TsLabel;
    sLabel47: TsLabel;
    sDecimalSpinEdit44: TsDecimalSpinEdit;
    sDecimalSpinEdit45: TsDecimalSpinEdit;
    sGroupBox28: TsGroupBox;
    sLabel48: TsLabel;
    sLabel49: TsLabel;
    sDecimalSpinEdit46: TsDecimalSpinEdit;
    sDecimalSpinEdit47: TsDecimalSpinEdit;
    sGroupBox29: TsGroupBox;
    sLabel50: TsLabel;
    sLabel51: TsLabel;
    sDecimalSpinEdit48: TsDecimalSpinEdit;
    sDecimalSpinEdit49: TsDecimalSpinEdit;
    sGroupBox30: TsGroupBox;
    sLabel52: TsLabel;
    sLabel53: TsLabel;
    sDecimalSpinEdit50: TsDecimalSpinEdit;
    sDecimalSpinEdit51: TsDecimalSpinEdit;
    sGroupBox43: TsGroupBox;
    sGroupBox44: TsGroupBox;
    sLabel71: TsLabel;
    sLabel72: TsLabel;
    sDecimalSpinEdit69: TsDecimalSpinEdit;
    sDecimalSpinEdit70: TsDecimalSpinEdit;
    sGroupBox45: TsGroupBox;
    sLabel73: TsLabel;
    sDecimalSpinEdit71: TsDecimalSpinEdit;
    sGroupBox46: TsGroupBox;
    sLabel74: TsLabel;
    sLabel75: TsLabel;
    sLabel76: TsLabel;
    sDecimalSpinEdit72: TsDecimalSpinEdit;
    sDecimalSpinEdit73: TsDecimalSpinEdit;
    sDecimalSpinEdit74: TsDecimalSpinEdit;
    sGroupBox47: TsGroupBox;
    sLabel77: TsLabel;
    sLabel78: TsLabel;
    sDecimalSpinEdit75: TsDecimalSpinEdit;
    sDecimalSpinEdit76: TsDecimalSpinEdit;
    sGroupBox48: TsGroupBox;
    sLabel79: TsLabel;
    sLabel80: TsLabel;
    sDecimalSpinEdit77: TsDecimalSpinEdit;
    sDecimalSpinEdit78: TsDecimalSpinEdit;
    sGroupBox49: TsGroupBox;
    sLabel81: TsLabel;
    sDecimalSpinEdit79: TsDecimalSpinEdit;
    sGroupBox50: TsGroupBox;
    sLabel82: TsLabel;
    sLabel83: TsLabel;
    sDecimalSpinEdit80: TsDecimalSpinEdit;
    sDecimalSpinEdit81: TsDecimalSpinEdit;
    sGroupBox51: TsGroupBox;
    sLabel84: TsLabel;
    sDecimalSpinEdit82: TsDecimalSpinEdit;
    sGroupBox52: TsGroupBox;
    sLabel85: TsLabel;
    sLabel86: TsLabel;
    sDecimalSpinEdit83: TsDecimalSpinEdit;
    sDecimalSpinEdit84: TsDecimalSpinEdit;
    sGroupBox53: TsGroupBox;
    sLabel87: TsLabel;
    sDecimalSpinEdit85: TsDecimalSpinEdit;
    sLabel88: TsLabel;
    sSpinEdit1: TsSpinEdit;
    sLabel89: TsLabel;
    sLabel90: TsLabel;
    sSpinEdit2: TsSpinEdit;
    sLabel91: TsLabel;
    sButton1: TsButton;
    sBitBtn1: TsBitBtn;
    sRichEdit1: TsRichEdit;
    sTabSheet7: TsTabSheet;
    sGroupBox54: TsGroupBox;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    sGroupBox55: TsGroupBox;
    sRadioButton3: TsRadioButton;
    sRadioButton4: TsRadioButton;
    sCheckBox1: TsCheckBox;
    procedure sButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }

  public
    procedure kch_oil(decEdt: TsDecimalSpinEdit);
    procedure pch_oil(decEdt: TsDecimalSpinEdit);
    procedure vlaga_oil(decEdt: TsDecimalSpinEdit);
    procedure acid_raf(decEdt: TsDecimalSpinEdit);
    procedure alkaly_raf(decEdt: TsDecimalSpinEdit);
    function AddColorText(Text: String; Color: TColor): String;
    procedure UnderAndBold;
    procedure BoldAndFront(inpt: string);
    function deltaKch(kch: Extended): Extended;
    function deltaPch(pch: Extended): Extended;
    function RoundMultiple(v, p: Extended): Extended;
    procedure PrintRichEd(ARichEd: TRichEdit; APortrait: boolean);
    { Public declarations }
  end;

var
  RafForm: TRafForm;
  Ini: TIniFile;
  m1, m2, m3, m_1, m_2, m_3, w1, w2, v1, v2, kch1, kch2, pch1, pch2, w_sred, w_true: Extended;
  TKOH, TTS, conc1, conc2: Extended;
  check: integer;

implementation

uses DecimalRounding_JH1;

{$R *.dfm}


// Кислота с рафинации
procedure TRafForm.acid_raf(decEdt: TsDecimalSpinEdit);
var k: Integer;
begin
  m1 := 0.05 + Random(2)/10 + Random(10)/100 + Random(10)/1000 + Random(10)/10000;
  m2 := 0.05 + Random(2)/10 + Random(10)/100 + Random(10)/1000 + Random(10)/10000;
  k := Random(4);
  if ( k = 0) then
    begin
      k := Random(2);
      if (k = 0) then
        begin
          conc1 := decEdt.Value;
          conc2 := conc1 - 0.01;
        end
      else
        begin
          conc2 := decEdt.Value;
          conc1 := conc2 - 0.01;
        end;
    end
  else
    if ( k = 1) then
      begin
        k := Random(2);
        if (k = 0) then
          begin
            conc1 := decEdt.Value + 0.01;
            conc2 := decEdt.Value - 0.01;
          end
        else
          begin
            conc1 := decEdt.Value - 0.01;
            conc2 := decEdt.Value + 0.01;
          end;
      end
    else
      if ( k = 2) then
        begin
          k := Random(2);
          if (k = 0) then
            begin
              conc1 := decEdt.Value + 0.01;
              conc2 := decEdt.Value - 0.02;
            end
          else
            begin
              conc2 := decEdt.Value + 0.01;
              conc1 := decEdt.Value - 0.02;
            end;
        end
      else
        if ( k = 3) then
        begin
          k := Random(2);
          if (k = 0) then
            begin
              conc1 := decEdt.Value + 0.02;
              conc2 := decEdt.Value - 0.02;
            end
          else
            begin
              conc1 := decEdt.Value - 0.02;
              conc2 := decEdt.Value + 0.02;
            end;

        end;
  v1 := conc1 * m1 / (0.64);
  v2 := conc2 * m2 / (0.64);
  v1 := DecimalRoundExt(v1, 2, drHalfPos);
  v2 := DecimalRoundExt(v2, 2, drHalfPos);
end;

function TRafForm.AddColorText(Text: String; Color: TColor): String;
begin
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style+[fsBold];
  sRichEdit1.SelAttributes.Color := Color;
  sRichEdit1.SelText := Text;
  sRichEdit1.SelAttributes.Color := clBlack;
end;


// Щелочь с рафинации
procedure TRafForm.alkaly_raf(decEdt: TsDecimalSpinEdit);
var k: Integer;
begin
  m1 := 0.25 + Random(4)/10 + Random(10)/100 + Random(10)/1000 + Random(10)/10000;
  m2 := 0.25 + Random(4)/10 + Random(10)/100 + Random(10)/1000 + Random(10)/10000;
  k := Random(4);
  if ( k = 0) then
    begin
      k := Random(2);
      if (k = 0) then
        begin
          conc1 := decEdt.Value;
          conc2 := conc1 - 0.01;
        end
      else
        begin
          conc2 := decEdt.Value;
          conc1 := conc2 - 0.01;
        end;
    end
  else
    if ( k = 1) then
      begin
        k := Random(2);
        if (k = 0) then
          begin
            conc1 := decEdt.Value + 0.01;
            conc2 := decEdt.Value - 0.01;
          end
        else
          begin
            conc1 := decEdt.Value - 0.01;
            conc2 := decEdt.Value + 0.01;
          end;
      end
    else
      if ( k = 2) then
        begin
          k := Random(2);
          if (k = 0) then
            begin
              conc1 := decEdt.Value + 0.01;
              conc2 := decEdt.Value - 0.02;
            end
          else
            begin
              conc2 := decEdt.Value + 0.01;
              conc1 := decEdt.Value - 0.02;
            end;
        end
      else
        if ( k = 3) then
        begin
          k := Random(2);
          if (k = 0) then
            begin
              conc1 := decEdt.Value + 0.02;
              conc2 := decEdt.Value - 0.02;
            end
          else
            begin
              conc1 := decEdt.Value - 0.02;
              conc2 := decEdt.Value + 0.02;
            end;

        end;
  v1 := conc1 * m1 / (0.40);
  v2 := conc2 * m2 / (0.40);
  v1 := DecimalRoundExt(v1, 2, drHalfPos);
  v2 := DecimalRoundExt(v2, 2, drHalfPos);
end;

procedure TRafForm.BoldAndFront(inpt: string);
begin
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.SelAttributes.Size := 16;
  sRichEdit1.SelText := inpt;
  sRichEdit1.SelAttributes.Color := clBlack;
  sRichEdit1.SelAttributes.Size := 10;
end;

function TRafForm.deltaKch(kch: Extended): Extended;
begin
  if (kch <= 1.0) then
    Result := DecimalRoundExt(kch * 0.06, 2, drHalfPos)
  else
    if ( (kch > 1.0) and (kch <= 6.0) ) then
      Result := DecimalRoundExt(kch * 0.03, 2, drHalfPos)
    else
      if ( (kch > 6.0) and (kch <= 30.0) ) then
        Result := DecimalRoundExt(kch * 0.02, 2, drHalfPos);
end;

function TRafForm.deltaPch(pch: Extended): Extended;
begin
  if (pch < 3.0) then
    Result := DecimalRoundExt(pch * 0.10, 2, drHalfPos)
  else
    if ( pch >= 3.0 ) then
      Result := DecimalRoundExt(pch * 0.05, 2, drHalfPos)
end;

procedure TRafForm.FormActivate(Sender: TObject);
begin
  Ini:=TiniFile.Create(extractfilepath(paramstr(0))+'Settings.ini');
  if  ( (not Ini.ValueExists('KOH', 'k')) and (not Ini.ValueExists('TioSulf', 'k'))) then
    begin
      Ini.WriteFloat('KOH', 'k', 0.100);
      Ini.WriteFloat('TioSulf', 'k', 0.0020);
    end;
  concKOH.Value := Ini.ReadFloat('KOH', 'k', 0.100);
  concTS.Value := Ini.ReadFloat('TioSulf', 'k', 0.0020);
  ini.Free;
end;

procedure TRafForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Ini:=TiniFile.Create(extractfilepath(paramstr(0))+'Settings.ini');
  Ini.WriteFloat('KOH', 'k', concKOH.Value);
  Ini.WriteFloat('TioSulf', 'k', concTS.Value);
  Ini.Free;
end;

procedure TRafForm.FormCreate(Sender: TObject);
begin
  Randomize;
  Ini:=TiniFile.Create(extractfilepath(paramstr(0))+'Settings.ini');
  if  ( (not Ini.ValueExists('KOH', 'k')) and (not Ini.ValueExists('TioSulf', 'k'))) then
    begin
      Ini.WriteFloat('KOH', 'k', 0.100);
      Ini.WriteFloat('TioSulf', 'k', 0.0020);
    end;
  concKOH.Value := Ini.ReadFloat('KOH', 'k', 0.100);
  concTS.Value := Ini.ReadFloat('TioSulf', 'k', 0.0020);
  ini.Free;
  sPageControl1.ActivePageIndex := 4;
end;

// процедура расчета по к.ч. входных параметров
procedure TRafForm.kch_oil(decEdt: TsDecimalSpinEdit);
var k, z, i: Integer;
    Cen_Del, kch1_new, kch2_new, kch_new, delta: Extended;
    v1str, v2str, temp_home, temp_end, tempstr, tempstr2: string;
begin
  if ( sRadioButton1.Checked = True) then
    Cen_Del := 1;
  if ( sRadioButton2.Checked = True) then
    Cen_Del := 5;
  // для рафинированого масла, когда КЧ 0-1
  if ( (decEdt.Value <= 1.00) ) then
    begin
      delta := deltaKch(decEdt.Value);
      if (Cen_Del = 1) then
        begin
          repeat
            if check = 1 then
              begin
                m1 := 9.90 + Random(100)/100;
                m2 := 9.90 + Random(100)/100;
              end
            else
              begin
                m1 := 19.90 + Random(200)/100;
                m2 := 19.90 + Random(200)/100;
              end;
            repeat
              // генерируем параллели
              i := RandomRange(0, 6);
              k := Random(2);
              if (k = 0) then
                begin
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch2 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end
              else
                begin
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch1 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end;
            until Abs(kch1 - kch2) <= delta;

            v1 := kch1 * m1 / (56.11 * TKOH);
            v2 := kch2 * m2 / (56.11 * TKOH);
            v1 := DecimalRoundExt(v1, 2, drHalfPos);
            v2 := DecimalRoundExt(v2, 2, drHalfPos);
            kch1_new := 56.11 * TKOH * v1 / m1;
            kch2_new := 56.11 * TKOH * v2 / m2;
            kch1_new := DecimalRoundExt(kch1_new, 2, drHalfPos);
            kch2_new := DecimalRoundExt(kch2_new, 2, drHalfPos);
            kch_new := DecimalRoundExt((kch1_new + kch2_new)/2, 2, drHalfPos);
          until ( Abs(kch_new - decEdt.Value) <= 0.001 ) and ( Abs(kch1_new - kch2_new) <= delta );
        end;
      if (Cen_Del = 5) then
        begin
          repeat
            if check = 1 then
              begin
                m1 := 9.90 + Random(100)/100;
                m2 := 9.90 + Random(100)/100;
              end
            else
              begin
                m1 := 19.90 + Random(200)/100;
                m2 := 19.90 + Random(200)/100;
              end;
            repeat
              // генерируем параллели
              i := RandomRange(0, 6);
              k := Random(2);
              if (k = 0) then
                begin
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end
              else
                begin
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end;
            until Abs(kch1 - kch2) <= delta;

            v1 := kch1 * m1 / (56.11 * TKOH);
            v2 := kch2 * m2 / (56.11 * TKOH);
            v1 := DecimalRoundExt(v1, 2, drHalfPos);
            v2 := DecimalRoundExt(v2, 2, drHalfPos);

            v1str := FloatToStrF(v1, ffFixed, 5, 2);

            z := StrToInt(v1str[Length(v1str)]);
            if ( z >= 5) then
              v1str[Length(v1str)] := '5'
            else
              v1str[Length(v1str)] := '0';

            v2str := FloatToStrF(v2, ffFixed, 5, 2);
            z := StrToInt(v2str[Length(v2str)]);
            if ( z >= 5) then
              v2str[Length(v2str)] := '5'
            else
              v2str[Length(v2str)] := '0';
            v1 := StrToFloat(v1str);
            v2 := StrToFloat(v2str);
            kch1_new := 56.11 * TKOH * v1 / m1;
            kch2_new := 56.11 * TKOH * v2 / m2;
            kch1_new := DecimalRoundExt(kch1_new, 2, drHalfPos);
            kch2_new := DecimalRoundExt(kch2_new, 2, drHalfPos);
            kch_new := DecimalRoundExt((kch1_new + kch2_new)/2, 2, drHalfPos);
          until ( Abs(kch_new - decEdt.Value) <= 0.001 ) and ( Abs(kch1_new - kch2_new) <= delta );
        end;
    end;
  // для нерафинированого масла, когда КЧ 1-4
  if ( (decEdt.Value > 1.00) and (decEdt.Value <= 4.00) ) then
    begin
      delta := deltaKch(decEdt.Value);
      if (Cen_Del = 1) then
        begin
          repeat
            m1 := 9.90 + Random(100)/100;
            m2 := 9.90 + Random(100)/100;
            repeat
              // генерируем параллели
              i := RandomRange(0, 3);
              k := Random(2);
              if (k = 0) then
                begin
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch2 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end
              else
                begin
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch1 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end;
            until Abs(kch1 - kch2) <= delta;

            v1 := kch1 * m1 / (56.11 * TKOH);
            v2 := kch2 * m2 / (56.11 * TKOH);
            v1 := DecimalRoundExt(v1, 2, drHalfPos);
            v2 := DecimalRoundExt(v2, 2, drHalfPos);
            kch1_new := DecimalRoundExt(56.11 * TKOH * v1 / m1, 2, drHalfPos);
            kch2_new := DecimalRoundExt(56.11 * TKOH * v2 / m2, 2, drHalfPos);
            kch_new := DecimalRoundExt((kch1_new + kch2_new)/2, 2, drHalfPos);
          until ( Abs(kch_new - decEdt.Value) <= 0.001 ) and ( Abs(kch1_new - kch2_new) <= delta );
        end;
      if (Cen_Del = 5) then
        begin
          repeat
            m1 := 9.90 + Random(100)/100;
            m2 := 9.90 + Random(100)/100;
            repeat
              // генерируем параллели
              i := RandomRange(0, 3);
              k := Random(2);
              if (k = 0) then
                begin
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch2 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end
              else
                begin
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch1 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end;
            until Abs(kch1 - kch2) <= delta;

            v1 := kch1 * m1 / (56.11 * TKOH);
            v2 := kch2 * m2 / (56.11 * TKOH);
            v1 := DecimalRoundExt(v1, 2, drHalfPos);
            v2 := DecimalRoundExt(v2, 2, drHalfPos);

            v1str := FloatToStrF(v1, ffFixed, 5, 2);

            z := StrToInt(v1str[Length(v1str)]);
            if ( z >= 5) then
              v1str[Length(v1str)] := '5'
            else
              v1str[Length(v1str)] := '0';

            v2str := FloatToStrF(v2, ffFixed, 5, 2);
            z := StrToInt(v2str[Length(v2str)]);
            if ( z >= 5) then
              v2str[Length(v2str)] := '5'
            else
              v2str[Length(v2str)] := '0';
            v1 := StrToFloat(v1str);
            v2 := StrToFloat(v2str);

            kch1_new := DecimalRoundExt(56.11 * TKOH * v1 / m1, 2, drHalfPos);
            kch2_new := DecimalRoundExt(56.11 * TKOH * v2 / m2, 2, drHalfPos);
            kch_new := DecimalRoundExt((kch1_new + kch2_new)/2, 2, drHalfPos);
          until ( Abs(kch_new - decEdt.Value) <= 0.001 ) and ( Abs(kch1_new - kch2_new) <= delta );
        end;
    end;
  // для нерафинированого масла, когда КЧ 4-15
  if ( (decEdt.Value > 4.00) and (decEdt.Value <= 15.00) ) then
    begin
      delta := deltaKch(decEdt.Value);
      if (Cen_Del = 1) then
        begin
          repeat
            m1 := 2.48 + Random(21)/100;
            m2 := 2.48 + Random(21)/100;
            repeat
              // генерируем параллели
              i := RandomRange(0, 2);
              k := Random(2);
              if (k = 0) then
                begin
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch2 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end
              else
                begin
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch1 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end;
            until Abs(kch1 - kch2) <= delta;

            v1 := kch1 * m1 / (56.11 * TKOH);
            v2 := kch2 * m2 / (56.11 * TKOH);
            v1 := DecimalRoundExt(v1, 2, drHalfPos);
            v2 := DecimalRoundExt(v2, 2, drHalfPos);
            kch1_new := DecimalRoundExt(56.11 * TKOH * v1 / m1, 2, drHalfPos);
            kch2_new := DecimalRoundExt(56.11 * TKOH * v2 / m2, 2, drHalfPos);
            kch_new := DecimalRoundExt((kch1_new + kch2_new)/2, 2, drHalfPos);
          until ( Abs(kch_new - decEdt.Value) <= 0.001 ) and ( Abs(kch1_new - kch2_new) <= delta );
        end;
      if (Cen_Del = 5) then
        begin
          repeat
            m1 := 2.48 + Random(21)/100;
            m2 := 2.48 + Random(21)/100;
            repeat
              // генерируем параллели
              i := RandomRange(0, 3);
              k := Random(2);
              if (k = 0) then
                begin
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch2 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end
              else
                begin
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch1 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end;
            until Abs(kch1 - kch2) <= delta;

            v1 := kch1 * m1 / (56.11 * TKOH);
            v2 := kch2 * m2 / (56.11 * TKOH);
            v1 := DecimalRoundExt(v1, 2, drHalfPos);
            v2 := DecimalRoundExt(v2, 2, drHalfPos);

            v1str := FloatToStrF(v1, ffFixed, 5, 2);

            z := StrToInt(v1str[Length(v1str)]);
            if ( z >= 5) then
              v1str[Length(v1str)] := '5'
            else
              v1str[Length(v1str)] := '0';

            v2str := FloatToStrF(v2, ffFixed, 5, 2);
            z := StrToInt(v2str[Length(v2str)]);
            if ( z >= 5) then
              v2str[Length(v2str)] := '5'
            else
              v2str[Length(v2str)] := '0';
            v1 := StrToFloat(v1str);
            v2 := StrToFloat(v2str);

            kch1_new := DecimalRoundExt(56.11 * TKOH * v1 / m1, 2, drHalfPos);
            kch2_new := DecimalRoundExt(56.11 * TKOH * v2 / m2, 2, drHalfPos);
            kch_new := DecimalRoundExt((kch1_new + kch2_new)/2, 2, drHalfPos);
          until ( Abs(kch_new - decEdt.Value) <= 0.001 ) and ( Abs(kch1_new - kch2_new) <= delta );
        end;
    end;

  kch1 := kch1_new;
  kch2 := kch2_new;

  if (decEdt.Value <= 1.00) then
    begin
      sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(kch1 - kch2), ffFixed, 5, 2) + ' (Max.= '+ FloatToStrF(delta, ffFixed, 5, 2) + ') / К.Ч. = ' + FloatToStrF(decEdt.Value, ffFixed, 5, 2)
                                          + '/' + FloatToStrF(DecimalRoundExt(decEdt.Value, 2, drHalfPos), ffFixed, 5, 2) + ' мг KOH/г.', clGreen));
      sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, 5, 2) + '/' + FloatToStrF(v1, ffFixed, 5, 2) + '/' + FloatToStrF(kch1, ffFixed, 5, 2), clRed));
      sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m2, ffFixed, 5, 2) + '/' + FloatToStrF(v2, ffFixed, 5, 2) + '/' + FloatToStrF(kch2, ffFixed, 5, 2), clBlue));
      sRichEdit1.Lines.Add('---------------------------------------------------------');
    end
  else
    begin
      sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(kch1 - kch2), ffFixed, 5, 2) + ' (Max.= '+ FloatToStrF(delta, ffFixed, 5, 2) + ') / К.Ч. = ' + FloatToStrF(decEdt.Value, ffFixed, 5, 2)
                                         + '/' + FloatToStrF(DecimalRoundExt(decEdt.Value, 1, drHalfPos), ffFixed, 5, 1) + ' мг KOH/г.', clGreen));
      sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, 5, 2) + '/' + FloatToStrF(v1, ffFixed, 5, 2) + '/' + FloatToStrF(kch1, ffFixed, 5, 2), clRed));
      sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m2, ffFixed, 5, 2) + '/' + FloatToStrF(v2, ffFixed, 5, 2) + '/' + FloatToStrF(kch2, ffFixed, 5, 2), clBlue));
      sRichEdit1.Lines.Add('---------------------------------------------------------');
    end;
end;

// процедура расчета по п.ч. входных параметров
procedure TRafForm.pch_oil(decEdt: TsDecimalSpinEdit);
var k, z, i: Integer;
  tst1, tst2, pchOkruglen : string;
  m1bas, m2bas, delta, pch1_new, pch2_new, pch_new, pch: Extended;
  Cen_Del: Integer;
begin
  //
  TTS := concTS.Value;
  if ( sRadioButton1.Checked = True) then
    Cen_Del := 1;
  if ( sRadioButton2.Checked = True) then
    Cen_Del := 5;
  delta := deltaPch(decEdt.Value);
  pch := decEdt.Value;
  repeat
    repeat
      if (decEdt.Value < 6.00) then
        begin
          m1 := 2 + Random(3) + Random(9)/10 + Random(10)/100;
          m2 := 2 + Random(3) + Random(9)/10 + Random(10)/100;
          m1bas := 2.00; m2bas := 2.00;
          repeat
            // генерируем параллели
            i := RandomRange(0, 10);
            k := Random(2);
            if (k = 0) then
              begin
                pch1 := pch - DecimalRoundExt(pch * 0.51 * i / 100, 2, drHalfPos);
                pch2 := pch + DecimalRoundExt(pch * 0.49 * i / 100, 2, drHalfPos);
              end
            else
              begin
                pch2 := pch - DecimalRoundExt(pch * 0.51 * i / 100, 2, drHalfPos);
                pch1 := pch + DecimalRoundExt(pch * 0.49 * i / 100, 2, drHalfPos);
              end;
              // округляем параллели до десятых
            pch1 := DecimalRoundExt(pch1, 2, drHalfPos);
            pch2 := DecimalRoundExt(pch2, 2, drHalfPos);
          until Abs(pch1 - pch2) <= delta;
        end
      else
        begin
          m1 := 1.2 + Random(8)/10 + Random(10)/100;
          m2 := 1.2 + Random(8)/10 + Random(10)/100;
          m1bas := 1.20; m2bas := 1.20;
          repeat
            // генерируем параллели
            i := RandomRange(0, 5);
            k := Random(2);
            if (k = 0) then
              begin
                pch1 := pch - DecimalRoundExt(pch * 0.51 * i / 100, 2, drHalfPos);
                pch2 := pch + DecimalRoundExt(pch * 0.49 * i / 100, 2, drHalfPos);
              end
            else
              begin
                pch2 := pch - DecimalRoundExt(pch * 0.51 * i / 100, 2, drHalfPos);
                pch1 := pch + DecimalRoundExt(pch * 0.49 * i / 100, 2, drHalfPos);
              end;
              // округляем параллели до десятых
            pch1 := DecimalRoundExt(pch1, 2, drHalfPos);
            pch2 := DecimalRoundExt(pch2, 2, drHalfPos);
          until Abs(pch1 - pch2) <= delta;
        end;


      v1 := m1 * pch1 / (TTS * 1000);
      v2 := m2 * pch2 / (TTS * 1000);

      v1 := DecimalRoundExt(v1, 2, drHalfPos);
      v2 := DecimalRoundExt(v2, 2, drHalfPos);

      if (Cen_Del = 5) then
        begin
          tst1 := FloatToStrF(v1, ffFixed, 5, 2);

          z := StrToInt(tst1[Length(tst1)]);
          if ( z >= 5) then
            tst1[Length(tst1)] := '5'
          else
            tst1[Length(tst1)] := '0';

          tst2 := FloatToStrF(v2, ffFixed, 5, 2);
          z := StrToInt(tst2[Length(tst2)]);
          if ( z >= 5) then
            tst2[Length(tst2)] := '5'
          else
            tst2[Length(tst2)] := '0';

          m1 := (StrToFloat(tst1) * TTS * 1000) / pch1;
          m2 := (StrToFloat(tst2) * TTS * 1000) / pch2;
          v1 := StrToFloat(tst1);
          v2 := StrToFloat(tst2);
        end
      else
        if (Cen_Del = 1) then
          begin
            m1 := v1 * TTS * 1000 / pch1;
            m2 := v2 * TTS * 1000 / pch2;
          end;

      m1 := DecimalRoundExt(m1, 2, drHalfPos);
      m2 := DecimalRoundExt(m2, 2, drHalfPos);
    until ( m1 >= m1bas ) and ( m1 <= 5.0 ) and ( m2 >= m2bas ) and ( m2 <= 5.0 );

    pch1_new := DecimalRoundExt(v1 * TTS * 1000/m1, 2, drHalfPos);
    pch2_new := DecimalRoundExt(v2 * TTS * 1000/m2, 2, drHalfPos);
    pch_new := DecimalRoundExt((pch1_new + pch2_new)/2, 2, drHalfPos);

  until ( ( Abs(pch_new - decEdt.Value) < 0.01) and ( Abs(pch1_new - pch2_new) <= delta) );

  pchOkruglen := FloatToStrF(DecimalRoundExt(pch, 1, drHalfPos), ffFixed, 5, 1);

  sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(pch1 - pch2), ffFixed, 5, 2) + ' (Max.= '+ FloatToStrF(delta, ffFixed, 5, 2) + ') / П.Ч. = ' + FloatToStrF(decEdt.Value, ffFixed, 5, 2) + '/' + pchOkruglen + ' ммоль 1/2 O/кг.', clGreen));
  sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, 5, 2) + '/' + FloatToStrF(v1, ffFixed, 5, 2) + '/' + FloatToStrF(pch1, ffFixed, 5, 2), clRed));
  sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m2, ffFixed, 5, 2) + '/' + FloatToStrF(v2, ffFixed, 5, 2) + '/' + FloatToStrF(pch2, ffFixed, 5, 2), clBlue));
  sRichEdit1.Lines.Add('---------------------------------------------------------');
end;


function TRafForm.RoundMultiple(v, p: Extended): Extended;
  var n: int64;
begin
  n := round(v/p);
  Result := n*p;
end;


// ВЛАГА!
procedure TRafForm.vlaga_oil(decEdt: TsDecimalSpinEdit);
var k, i: integer;
  delta, w1new, w2new : Extended;
  m_oil: Integer;
begin
  w_sred := decEdt.Value;
  if ( sRadioButton3.Checked = True) then
    m_oil := 5;
  if ( sRadioButton4.Checked = True) then
    m_oil := 10;
  repeat
    m1 := RandomRange(sSpinEdit1.Value, sSpinEdit2.Value) + RandomRange(1000, 10000)/10000;
    m_1 := RandomRange(sSpinEdit1.Value, sSpinEdit2.Value) + RandomRange(1000, 10000)/10000;
    m2 := m1 + m_oil + RandomRange(-m_oil, m_oil) * 0.01*Random(100)/100;
    m_2 := m_1 + m_oil + RandomRange(-m_oil, m_oil) * 0.01*Random(100)/100;

    repeat
      // генерируем параллели
      i := RandomRange(0, 6);
      k := Random(2);
      if (k = 0) then
        begin
          w1 := w_sred - DecimalRoundExt(0.51 * i / 100, 2, drHalfPos);
          w2 := w_sred + DecimalRoundExt(0.49 * i / 100, 2, drHalfPos);
        end
      else
        begin
          w2 := w_sred - DecimalRoundExt(0.51 * i / 100, 2, drHalfPos);
          w1 := w_sred + DecimalRoundExt(0.49 * i / 100, 2, drHalfPos);
        end;
    until Abs(w1 - w2) <= 0.05;


    m3 := m2 - w1*(m2 - m1)/100;
    m_3 := m_2 - w2*(m_2 - m_1)/100;
    m3 := DecimalRoundExt(m3, 4, drHalfPos);
    m_3 := DecimalRoundExt(m_3, 4, drHalfPos);

    w1new := 100 * (m2 - m3) / (m2 - m1);
    w2new := 100 * (m_2 - m_3) / (m_2 - m_1);

    w1new := DecimalRoundExt(w1new, 2, drHalfPos);
    w2new := DecimalRoundExt(w2new, 2, drHalfPos);

    w_true := (w1new + w2new)/2;

    w_true := DecimalRoundExt(w_true, 2, drHalfPos);

    delta := Abs(w1new - w2new);
  until ( (Abs(w_true - w_sred) < 0.001) and (delta <= 0.05));

  w1 := w1new;
  w2 := w2new;

  sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(delta, ffFixed, 5, 2) + ' % (Max.=0.05%) / W = ' + FloatToStrF(w_true, ffFixed, 5, 2) + ' %', clGreen));
          sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, 7, 4) + '/'
          + FloatToStrF(m2, ffFixed, 7, 4) + '/' + FloatToStrF(m3, ffFixed, 7, 4) + '/'
          + FloatToStrF(w1, ffFixed, 5, 2), clRed));
          sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m_1, ffFixed, 7, 4) + '/'
          + FloatToStrF(m_2, ffFixed, 7, 4) + '/' + FloatToStrF(m_3, ffFixed, 7, 4) + '/'
          + FloatToStrF(w2, ffFixed, 5, 2), clBlue));
end;

// Весь вывод всей информации в Мемо1
procedure TRafForm.sBitBtn1Click(Sender: TObject);
begin
  PrintRichEd(sRichEdit1, True);
end;

procedure TRafForm.sButton1Click(Sender: TObject);
var
  i: Integer;
begin
  if sCheckBox1.Checked = true then
    check := 1 else check := 0;
  TKOH := concKOH.Value;
  TTS := concTS.Value;
  sRichEdit1.Lines.Clear;

  sRichEdit1.Lines.Add('===========================================================');
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.SelAttributes.Size := 9;
  sRichEdit1.Lines.Add('Вводная информация:');
  //sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clGreen;
  sRichEdit1.SelAttributes.Size := 8;
  sRichEdit1.Lines.Add('r - расхождение между параллелями');
  //sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clGreen;
  sRichEdit1.SelAttributes.Size := 8;
  sRichEdit1.Lines.Add('C (KOH) = ' + FloatToStrF(TKOH, ffFixed, 3, 3) + ' моль-экв/л');
  //sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clGreen;
  sRichEdit1.SelAttributes.Size := 8;
  sRichEdit1.Lines.Add('C (Тиосульфат натрия) = ' + FloatToStr(TTS) + ' моль-экв/л');
  sRichEdit1.Lines.Add('===========================================================');



  // исходное масло / нахождение объемов по кч
  if (sGroupBox31.Checked = True) then
    begin
      sRichEdit1.Lines.Add('===========================================================');
      sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clRed;
      sRichEdit1.SelAttributes.Size := 12;
      sRichEdit1.Lines.Add('МАСЛО ИСХОДНОЕ');
      sRichEdit1.Lines.Add('===========================================================');

      // Расчет КЧ
      if ( BaseBak_kch.Value <> 0 ) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add('Расчет кислотного числа (К.Ч.):');
          if sCheckBox1.Checked = true then
            check := 1 else check := 0;
          kch_oil(BaseBak_kch);
        end;

      // Расчет ПЧ
      if ( BaseBak_pch.Value <> 0 ) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add('Расчет перекисного числа (П.Ч.):');
          pch_oil(BaseBak_pch);
        end;

      // Расчет ВЛАГИ!
      if ( BaseBak_vlaga.Value <> 0 ) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add('Расчет м.д. влаги и лет. веществ, %:');
          vlaga_oil(BaseBak_vlaga);
          sRichEdit1.Lines.Add('===========================================================' + #13#10);
        end;
    end;


  if ( sGroupBox1.Checked = True ) then
    begin
      // Рафинация 1 / Кислота / концентрация
      sRichEdit1.Lines.Add('===========================================================');
      sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clRed;
      sRichEdit1.SelAttributes.Size := 12;
      sRichEdit1.Lines.Add('РАФИНАЦИЯ №1 КИСЛОТА/ЩЕЛОЧЬ');
      sRichEdit1.Lines.Add('===========================================================');
      acid_raf(DecAcid1);
      if ( DecAcid1.Value <> 0) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add('Расчет конц. раствора лимонной кислоты, %:');
          sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(conc1 - conc2), ffFixed, 5, 2) + ' % / ω = ' + FloatToStrF(DecAcid1.Value, ffFixed, 5, 2) + ' %', clGreen));
          sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, 7, 4) + '/' + FloatToStrF(v1, ffFixed, 5, 2) + '/' + FloatToStrF(conc1, ffFixed, 5, 2), clRed));
          sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m2, ffFixed, 7, 4) + '/' + FloatToStrF(v2, ffFixed, 5, 2) + '/' + FloatToStrF(conc2, ffFixed, 5, 2), clBlue));
          sRichEdit1.Lines.Add('---------------------------------------------------------');
        end;

      // Рафинация 1 / Щелочь / концентрация
      alkaly_raf(DecAlkaly1);
      if ( DecAlkaly1.Value <> 0) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add('Расчет конц. раствора едкого натра, %:');
          sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(conc1 - conc2), ffFixed, 5, 2) + ' % / ω = ' + FloatToStrF(DecAlkaly1.Value, ffFixed, 5, 2) + ' %', clGreen));
          sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, 7, 4) + '/' + FloatToStrF(v1, ffFixed, 5, 2) + '/' + FloatToStrF(conc1, ffFixed, 5, 2), clRed));
          sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m2, ffFixed, 7, 4) + '/' + FloatToStrF(v2, ffFixed, 5, 2) + '/' + FloatToStrF(conc2, ffFixed, 5, 2), clBlue));
        end;
    end;

  // 1-я Рафиная Глобальный переключатель
  // Линия 1 ********
  if ( sGroupBox2.Checked = True ) then
    begin
      // Мокрая гидратация 1-я линия / 08:00 / 20:00
      sRichEdit1.Lines.Add('===========================================================');
      sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clRed;
      sRichEdit1.SelAttributes.Size := 12;
      sRichEdit1.Lines.Add('1-я Линия Рафинации');
      sRichEdit1.Lines.Add('===========================================================');

      // Мокрая гидратация 1-я линия / 08:00 / 20:00
      if ( sGroupBox3.Checked = True ) then
        begin
          if ( sDecimalSpinEdit3.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit3);
            end;

          if ( sDecimalSpinEdit4.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет м.д. влаги и лет. веществ, %:');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              vlaga_oil(sDecimalSpinEdit4);
              sRichEdit1.Lines.Add('---------------------------------------------------------');
            end;
        end;

      // Мокрая гидратация 1-я линия / 14:00 / 02:00
      if ( sGroupBox4.Checked = True ) then
        begin
          if ( sDecimalSpinEdit5.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit5);
            end;
        end;

      // Масло дезодорированное 1-я линия / 08:00 / 20:00
      if ( sGroupBox5.Checked = True ) then
        begin
          if ( sDecimalSpinEdit6.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              check := 0;
              kch_oil(sDecimalSpinEdit6);
            end;
          if ( sDecimalSpinEdit36.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              pch_oil(sDecimalSpinEdit36);
            end;
        end;
      // Масло дезодорированное 1-я линия / 11:00 / 23:00
      if ( sGroupBox6.Checked = True ) then
        begin
          if ( sDecimalSpinEdit7.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('11:00 / 23:00');
              check := 0;
              kch_oil(sDecimalSpinEdit7);
            end;
          if ( sDecimalSpinEdit8.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('11:00 / 23:00');
              pch_oil(sDecimalSpinEdit8);
            end;
        end;
      // Масло дезодорированное 1-я линия / 14:00 / 02:00
      if ( sGroupBox7.Checked = True ) then
        begin
          if ( sDecimalSpinEdit9.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              check := 0;
              kch_oil(sDecimalSpinEdit9);
            end;
          if ( sDecimalSpinEdit10.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              pch_oil(sDecimalSpinEdit10);
            end;
        end;
      // Масло дезодорированное 1-я линия / 17:00 / 05:00
      if ( sGroupBox8.Checked = True ) then
        begin
          if ( sDecimalSpinEdit11.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('17:00 / 05:00');
              check := 0;
              kch_oil(sDecimalSpinEdit11);
            end;
          if ( sDecimalSpinEdit12.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('17:00 / 05:00');
              pch_oil(sDecimalSpinEdit12);
            end;
        end;
    end;

  // 1-я Рафиная Глобальный переключатель
  // Линия 2 ********
  if ( sGroupBox9.Checked = True ) then
    begin
      // Мокрая гидратация 2-я линия / 08:00 / 20:00
      if ( sGroupBox10.Checked = True ) then
        begin
          sRichEdit1.Lines.Add('===========================================================');
          sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
          sRichEdit1.SelAttributes.Color := clRed;
          sRichEdit1.SelAttributes.Size := 12;
          sRichEdit1.Lines.Add('2-я Линия Рафинации');
          sRichEdit1.Lines.Add('===========================================================');

          if ( sDecimalSpinEdit13.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit13);
            end;


          if ( sDecimalSpinEdit14.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет м.д. влаги и лет. веществ, %:');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              vlaga_oil(sDecimalSpinEdit14);
              sRichEdit1.Lines.Add('---------------------------------------------------------');
            end;
        end;
      // Мокрая гидратация 2-я линия / 14:00 / 02:00
      if ( sGroupBox11.Checked = True ) then
        begin

          if ( sDecimalSpinEdit15.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit15);
            end;
        end;
      // Масло дезодорированное 2-я линия / 08:00 / 20:00
      if ( sGroupBox12.Checked = True ) then
        begin
          if ( sDecimalSpinEdit16.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              check := 0;
              kch_oil(sDecimalSpinEdit16);
            end;

          if ( sDecimalSpinEdit19.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              pch_oil(sDecimalSpinEdit19);
            end;
        end;
      // Масло дезодорированное 2-я линия / 11:00 / 23:00
      if ( sGroupBox13.Checked = True ) then
        begin
          if ( sDecimalSpinEdit20.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('11:00 / 23:00');
              check := 0;
              kch_oil(sDecimalSpinEdit20);
            end;

          if ( sDecimalSpinEdit21.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('11:00 / 23:00');
              pch_oil(sDecimalSpinEdit21);
            end;
        end;
      // Масло дезодорированное 2-я линия / 14:00 / 02:00
      if ( sGroupBox14.Checked = True ) then
        begin
          if ( sDecimalSpinEdit22.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              check := 0;
              kch_oil(sDecimalSpinEdit22);
            end;

          if ( sDecimalSpinEdit23.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              pch_oil(sDecimalSpinEdit23);
            end;
        end;
      // Масло дезодорированное 2-я линия / 17:00 / 05:00
      if ( sGroupBox15.Checked = True ) then
        begin

          if ( sDecimalSpinEdit24.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('17:00 / 05:00');
              check := 0;
              kch_oil(sDecimalSpinEdit24);
            end;

          if ( sDecimalSpinEdit25.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('17:00 / 05:00');
              pch_oil(sDecimalSpinEdit25);
            end;
        end;
    end;

  if ( sGroupBox16.Checked = True ) then
    begin
      // Рафинация 2 / Кислота / концентрация
      sRichEdit1.Lines.Add('===========================================================');
      sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clRed;
      sRichEdit1.SelAttributes.Size := 12;
      sRichEdit1.Lines.Add('РАФИНАЦИЯ №2 КИСЛОТА/ЩЕЛОЧЬ');
      sRichEdit1.Lines.Add('===========================================================');
      acid_raf(DecAcid2);
      if ( DecAcid2.Value <> 0) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add('Расчет конц. раствора лимонной кислоты, %:');
          sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(conc1 - conc2), ffFixed, 5, 2) + ' % / ω = ' + FloatToStrF(DecAcid1.Value, ffFixed, 5, 2) + ' %', clGreen));
          sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, 7, 4) + '/' + FloatToStrF(v1, ffFixed, 5, 2) + '/' + FloatToStrF(conc1, ffFixed, 5, 2), clRed));
          sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m2, ffFixed, 7, 4) + '/' + FloatToStrF(v2, ffFixed, 5, 2) + '/' + FloatToStrF(conc2, ffFixed, 5, 2), clBlue));
          sRichEdit1.Lines.Add('---------------------------------------------------------');
        end;

      // Рафинация 1 / Щелочь / концентрация
      alkaly_raf(DecAlkaly2);
      if ( DecAlkaly2.Value <> 0) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add('Расчет конц. раствора едкого натра, %:');
          sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(conc1 - conc2), ffFixed, 5, 2) + ' % / ω = ' + FloatToStrF(DecAlkaly1.Value, ffFixed, 5, 2) + ' %', clGreen));
          sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, 7, 4) + '/' + FloatToStrF(v1, ffFixed, 5, 2) + '/' + FloatToStrF(conc1, ffFixed, 5, 2), clRed));
          sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m2, ffFixed, 7, 4) + '/' + FloatToStrF(v2, ffFixed, 5, 2) + '/' + FloatToStrF(conc2, ffFixed, 5, 2), clBlue));
        end;
    end;

  // 2-я Рафиная Глобальный переключатель
  // Линия 3 ********
  if ( sGroupBox17.Checked = True ) then
    begin
      // Мокрая гидратация 3-я линия / 08:00 / 20:00
      sRichEdit1.Lines.Add('===========================================================');
      sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clRed;
      sRichEdit1.SelAttributes.Size := 12;
      sRichEdit1.Lines.Add('3-я Линия Рафинации');
      sRichEdit1.Lines.Add('===========================================================');
      if ( sGroupBox18.Checked = True ) then
        begin
          if ( sDecimalSpinEdit26.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit26);
            end;


          if ( sDecimalSpinEdit27.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет м.д. влаги и лет. веществ, %:');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              vlaga_oil(sDecimalSpinEdit27);
              sRichEdit1.Lines.Add('---------------------------------------------------------');
            end;
        end;
      // Мокрая гидратация 3-я линия / 14:00 / 02:00
      if ( sGroupBox19.Checked = True ) then
        begin
          if ( sDecimalSpinEdit28.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit28);
            end;
        end;

      // Масло дезодорированное 3-я линия / 08:00 / 20:00
      if ( sGroupBox20.Checked = True ) then
        begin
          if ( sDecimalSpinEdit29.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              check := 0;
              kch_oil(sDecimalSpinEdit29);
            end;

          if ( sDecimalSpinEdit30.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              pch_oil(sDecimalSpinEdit30);
            end;
        end;
      // Масло дезодорированное 3-я линия / 11:00 / 23:00
      if ( sGroupBox21.Checked = True ) then
        begin
          if ( sDecimalSpinEdit31.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('11:00 / 23:00');
              check := 0;
              kch_oil(sDecimalSpinEdit31);
            end;

          if ( sDecimalSpinEdit32.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('11:00 / 23:00');
              pch_oil(sDecimalSpinEdit32);
            end;
        end;
      // Масло дезодорированное 3-я линия / 14:00 / 02:00
      if ( sGroupBox22.Checked = True ) then
        begin
          if ( sDecimalSpinEdit37.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              check := 0;
              kch_oil(sDecimalSpinEdit37);
            end;

          if ( sDecimalSpinEdit38.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              pch_oil(sDecimalSpinEdit38);
            end;
        end;
      // Масло дезодорированное 3-я линия / 17:00 / 05:00
      if ( sGroupBox23.Checked = True ) then
        begin
          if ( sDecimalSpinEdit39.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('17:00 / 05:00');
              check := 0;
              kch_oil(sDecimalSpinEdit39);
            end;

          if ( sDecimalSpinEdit40.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('17:00 / 05:00');
              pch_oil(sDecimalSpinEdit40);
              sRichEdit1.Lines.Add('');
            end;
        end;
    end;

  // 2-я Рафинация Глобальный переключатель
  // Линия 4 ********
  if ( sGroupBox24.Checked = True ) then
    begin
      sRichEdit1.Lines.Add('===========================================================');
      sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clRed;
      sRichEdit1.SelAttributes.Size := 12;
      sRichEdit1.Lines.Add('2-я Линия Рафинации');
      sRichEdit1.Lines.Add('===========================================================');
      // Мокрая гидратация 4-я линия / 08:00 / 20:00
      if ( sGroupBox25.Checked = True ) then
        begin
          if ( sDecimalSpinEdit41.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit41);
            end;

          if ( sDecimalSpinEdit42.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет м.д. влаги и лет. веществ, %:');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              vlaga_oil(sDecimalSpinEdit42);
              sRichEdit1.Lines.Add('---------------------------------------------------------');
            end;
        end;
      // Мокрая гидратация 4-я линия / 14:00 / 02:00
      if ( sGroupBox26.Checked = True ) then
        begin
          if ( sDecimalSpinEdit43.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло гидратированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit43);
            end;
        end;
      // Масло дезодорированное 4-я линия / 08:00 / 20:00
      if ( sGroupBox27.Checked = True ) then
        begin
          if ( sDecimalSpinEdit44.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              check := 0;
              kch_oil(sDecimalSpinEdit44);
            end;

          if ( sDecimalSpinEdit45.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('08:00 / 20:00');
              pch_oil(sDecimalSpinEdit45);
            end;
        end;
      // Масло дезодорированное 2-я линия / 11:00 / 23:00
      if ( sGroupBox28.Checked = True ) then
        begin
          if ( sDecimalSpinEdit46.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('11:00 / 23:00');
              check := 0;
              kch_oil(sDecimalSpinEdit46);
            end;

          if ( sDecimalSpinEdit47.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('11:00 / 23:00');
              pch_oil(sDecimalSpinEdit47);
            end;
        end;
      // Масло дезодорированное 4-я линия / 14:00 / 02:00
      if ( sGroupBox29.Checked = True ) then
        begin
          if ( sDecimalSpinEdit48.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              check := 0;
              kch_oil(sDecimalSpinEdit48);
            end;

          if ( sDecimalSpinEdit49.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('14:00 / 02:00');
              pch_oil(sDecimalSpinEdit49);
            end;
        end;
      // Масло дезодорированное 4-я линия / 17:00 / 05:00
      if ( sGroupBox30.Checked = True ) then
        begin
          if ( sDecimalSpinEdit50.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('17:00 / 05:00');
              check := 0;
              kch_oil(sDecimalSpinEdit50);
            end;

          if ( sDecimalSpinEdit51.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло дезодорированное. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('17:00 / 05:00');
              pch_oil(sDecimalSpinEdit51);
              sRichEdit1.Lines.Add('');
            end;
        end;
    end;

  // Заводы
  // М-1000
  if (sGroupBox32.Checked = True) then
    begin
      sRichEdit1.Lines.Add('============================================================');
      sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clRed;
      sRichEdit1.SelAttributes.Size := 12;
      sRichEdit1.Lines.Add('Завод М-1000. Прессовое/экстракционное масло.');
      sRichEdit1.Lines.Add('============================================================');
      // Прессовое масло 08-00/20-00
      if (sGroupBox33.Checked = True) then
        begin
          // Прессовое Кислотное число
          if ( sDecimalSpinEdit51.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло прессовое. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('08:00/20:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit52);
            end;
          // Прессовое Перекисное число

          if ( sDecimalSpinEdit52.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло прессовое. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('08:00/20:00');
              pch_oil(sDecimalSpinEdit53);
            end;
        end;

      if (sGroupBox34.Checked = True) then
        begin
          // Экстракционное Кислотное число
          if ( sDecimalSpinEdit55.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло экстракционное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('08:00/20:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit55);
            end;
        end;

      // 11:00 / 23:00
      if (sGroupBox37.Checked = True) then
        begin
          // Прессовое Кислотное число
          if ( sDecimalSpinEdit54.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло прессовое. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('11:00/23:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit54);
            end;

          // Прессовое Перекисное число
          if ( sDecimalSpinEdit56.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло прессовое. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('11:00/23:00');
              pch_oil(sDecimalSpinEdit56);
            end;
        end;

      if (sGroupBox38.Checked = True) then
        begin
          // Экстракционное Кислотное число
          if ( sDecimalSpinEdit62.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло экстракционное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('11:00/23:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit62);
            end;
        end;

      // 14:00 / 02:00
      if (sGroupBox39.Checked = True) then
        begin
          // Прессовое Кислотное число
          if ( sDecimalSpinEdit63.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло прессовое. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('14:00/02:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit63);
            end;

          // Прессовое Перекисное число
          if ( sDecimalSpinEdit64.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло прессовое. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('14:00/02:00');
              pch_oil(sDecimalSpinEdit64);
            end;
        end;

      if (sGroupBox40.Checked = True) then
        begin
          // Экстракционное Кислотное число
          if ( sDecimalSpinEdit65.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло экстракционное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('14:00/02:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit65);
            end;
        end;

      // 17:00 / 05:00
      if (sGroupBox41.Checked = True) then
        begin
          // Прессовое Кислотное число
          if ( sDecimalSpinEdit66.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло прессовое. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('17:00/05:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit66);
            end;

          // Прессовое Перекисное число
          if ( sDecimalSpinEdit67.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло прессовое. Расчет перекисного числа (П.Ч.):');
              sRichEdit1.Lines.Add('17:00/05:00');
              pch_oil(sDecimalSpinEdit67);
            end;
        end;

      if (sGroupBox42.Checked = True) then
        begin
          // Экстракционное Кислотное число
          if ( sDecimalSpinEdit68.Value <> 0 ) then
            begin
              UnderAndBold;
              sRichEdit1.Lines.Add('Масло экстракционное. Расчет кислотного числа (К.Ч.):');
              sRichEdit1.Lines.Add('17:00/05:00');
              if sCheckBox1.Checked = true then
                check := 1 else check := 0;
              kch_oil(sDecimalSpinEdit68);
            end;
        end;

      // ССП
      if ((sGroupBox33.Checked = True) or (sGroupBox37.Checked = True) or (sGroupBox39.Checked = True) or (sGroupBox41.Checked = True)) then
        begin
          if (sGroupBox35.Checked = True) then
            begin
              // Прессовое Кислотное число
              if ( sDecimalSpinEdit57.Value <> 0 ) then
                begin
                  UnderAndBold;
                  sRichEdit1.Lines.Add('Масло прессовое. Расчет кислотного числа (К.Ч.):');
                  sRichEdit1.Lines.Add('СреднеСуточная Проба (ССП)');
                  if sCheckBox1.Checked = true then
                    check := 1 else check := 0;
                  kch_oil(sDecimalSpinEdit57);
                end;

              // Прессовое Перекисное число
              if ( sDecimalSpinEdit58.Value <> 0 ) then
                begin
                  UnderAndBold;
                  sRichEdit1.Lines.Add('Масло прессовое. Расчет перекисного числа (П.Ч.):');
                  sRichEdit1.Lines.Add('СреднеСуточная Проба (ССП)');
                  pch_oil(sDecimalSpinEdit58);
                end;

              if ( sDecimalSpinEdit59.Value <> 0 ) then
                begin
                  UnderAndBold;
                  sRichEdit1.Lines.Add('Масло прессовое. Расчет м.д. влаги и лет. веществ, %:');
                  sRichEdit1.Lines.Add('СреднеСуточная Проба (ССП)');
                  vlaga_oil(sDecimalSpinEdit59);
                  sRichEdit1.Lines.Add('---------------------------------------------------------');
                end;
            end;
        end;

      if ((sGroupBox34.Checked = True) or (sGroupBox38.Checked = True) or (sGroupBox40.Checked = True) or (sGroupBox42.Checked = True)) then
        begin
          if (sGroupBox36.Checked = True) then
            begin
              // Экстракционное Кислотное число
              if ( sDecimalSpinEdit60.Value <> 0 ) then
                begin
                  UnderAndBold;
                  sRichEdit1.Lines.Add('Масло экстракционное. Расчет кислотного числа (К.Ч.):');
                  sRichEdit1.Lines.Add('СреднеСуточная Проба (ССП)');
                  if sCheckBox1.Checked = true then
                    check := 1 else check := 0;
                  kch_oil(sDecimalSpinEdit60);
                end;

              if ( sDecimalSpinEdit61.Value <> 0 ) then
                begin
                  UnderAndBold;
                  sRichEdit1.Lines.Add('Масло экстракционное. Расчет м.д. влаги и лет. веществ, %:');
                  sRichEdit1.Lines.Add('СреднеСуточная Проба (ССП)');
                  vlaga_oil(sDecimalSpinEdit61);
                  sRichEdit1.Lines.Add('');
                end;
            end;
        end;
    end;

  // Заводы
  // М-3000
  if (sGroupBox43.Checked = True) then
    begin
      // Прессовое масло 08-00/20-00
      sRichEdit1.Lines.Add('============================================================');
      sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clRed;
      sRichEdit1.SelAttributes.Size := 12;
      sRichEdit1.Lines.Add('Завод М-3000. Прессовое/экстракционное масло.');
      sRichEdit1.Lines.Add('============================================================');
    if (sGroupBox44.Checked = True) then
      begin
        // Прессовое Кислотное число
        if ( sDecimalSpinEdit69.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло прессовое. Расчет кислотного числа (К.Ч.):');
            sRichEdit1.Lines.Add('08:00/20:00');
            if sCheckBox1.Checked = true then
                check := 1 else check := 0;
            kch_oil(sDecimalSpinEdit69);
          end;

        // Прессовое Перекисное число
        if ( sDecimalSpinEdit70.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло прессовое. Расчет перекисного числа (П.Ч.):');
            sRichEdit1.Lines.Add('08:00/20:00');
            pch_oil(sDecimalSpinEdit70);
          end;
      end;

    if (sGroupBox45.Checked = True) then
      begin
        // Экстракционное Кислотное число
        if ( sDecimalSpinEdit71.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло экстракционное. Расчет кислотного числа (К.Ч.):');
            sRichEdit1.Lines.Add('08:00/20:00');
            if sCheckBox1.Checked = true then
                check := 1 else check := 0;
            kch_oil(sDecimalSpinEdit71);
          end;
      end;

    // 11:00 / 23:00
    if (sGroupBox48.Checked = True) then
      begin
        // Прессовое Кислотное число
        if ( sDecimalSpinEdit77.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло прессовое. Расчет кислотного числа (К.Ч.):');
            sRichEdit1.Lines.Add('11:00/23:00');
            if sCheckBox1.Checked = true then
                check := 1 else check := 0;
            kch_oil(sDecimalSpinEdit77);
          end;

        // Прессовое Перекисное число
        if ( sDecimalSpinEdit78.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло прессовое. Расчет перекисного числа (П.Ч.):');
            sRichEdit1.Lines.Add('11:00/23:00');
            pch_oil(sDecimalSpinEdit78);
          end;
      end;

    if (sGroupBox49.Checked = True) then
      begin
        // Экстракционное Кислотное число
        if ( sDecimalSpinEdit79.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло экстракционное. Расчет кислотного числа (К.Ч.):');
            sRichEdit1.Lines.Add('11:00/23:00');
            if sCheckBox1.Checked = true then
                check := 1 else check := 0;
            kch_oil(sDecimalSpinEdit79);
          end;
      end;

    // 14:00 / 02:00
    if (sGroupBox50.Checked = True) then
      begin
        // Прессовое Кислотное число
        if ( sDecimalSpinEdit80.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло прессовое. Расчет кислотного числа (К.Ч.):');
            sRichEdit1.Lines.Add('14:00/02:00');
            if sCheckBox1.Checked = true then
                check := 1 else check := 0;
            kch_oil(sDecimalSpinEdit80);
          end;

        // Прессовое Перекисное число
        if ( sDecimalSpinEdit81.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло прессовое. Расчет перекисного числа (П.Ч.):');
            sRichEdit1.Lines.Add('14:00/02:00');
            pch_oil(sDecimalSpinEdit81);
          end;
      end;

    if (sGroupBox51.Checked = True) then
      begin
        // Экстракционное Кислотное число
        if ( sDecimalSpinEdit82.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло экстракционное. Расчет кислотного числа (К.Ч.):');
            sRichEdit1.Lines.Add('14:00/02:00');
            if sCheckBox1.Checked = true then
                check := 1 else check := 0;
            kch_oil(sDecimalSpinEdit82);
          end;
      end;

    // 17:00 / 05:00
    if (sGroupBox52.Checked = True) then
      begin
        // Прессовое Кислотное число
        if ( sDecimalSpinEdit83.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло прессовое. Расчет кислотного числа (К.Ч.):');
            sRichEdit1.Lines.Add('17:00/05:00');
            if sCheckBox1.Checked = true then
                check := 1 else check := 0;
            kch_oil(sDecimalSpinEdit83);
          end;

        // Прессовое Перекисное число
        if ( sDecimalSpinEdit84.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло прессовое. Расчет перекисного числа (П.Ч.):');
            sRichEdit1.Lines.Add('17:00/05:00');
            pch_oil(sDecimalSpinEdit84);
          end;
      end;

    if (sGroupBox53.Checked = True) then
      begin
        // Экстракционное Кислотное число
        if ( sDecimalSpinEdit85.Value <> 0 ) then
          begin
            UnderAndBold;
            sRichEdit1.Lines.Add('Масло экстракционное. Расчет кислотного числа (К.Ч.):');
            sRichEdit1.Lines.Add('17:00/05:00');
            if sCheckBox1.Checked = true then
                check := 1 else check := 0;
            kch_oil(sDecimalSpinEdit85);
          end;
      end;

    // ССП
    if ((sGroupBox44.Checked = True) or (sGroupBox48.Checked = True) or (sGroupBox50.Checked = True) or (sGroupBox52.Checked = True)) then
      begin
        if (sGroupBox46.Checked = True) then
          begin
            // Прессовое Кислотное число
            if ( sDecimalSpinEdit72.Value <> 0 ) then
              begin
                UnderAndBold;
                sRichEdit1.Lines.Add('Масло прессовое. Расчет кислотного числа (К.Ч.):');
                sRichEdit1.Lines.Add('СреднеСуточная Проба (ССП)');
                if sCheckBox1.Checked = true then
                  check := 1 else check := 0;
                kch_oil(sDecimalSpinEdit72);
              end;

            // Прессовое Перекисное число
            if ( sDecimalSpinEdit73.Value <> 0 ) then
              begin
                UnderAndBold;
                sRichEdit1.Lines.Add('Масло прессовое. Расчет перекисного числа (П.Ч.):');
                sRichEdit1.Lines.Add('СреднеСуточная Проба (ССП)');
                pch_oil(sDecimalSpinEdit73);
              end;

            // Прессовое ВЛАГА
            if ( sDecimalSpinEdit74.Value <> 0 ) then
              begin
                UnderAndBold;
                sRichEdit1.Lines.Add('Масло прессовое. Расчет м.д. влаги и лет. веществ, %:');
                sRichEdit1.Lines.Add('СреднеСуточная Проба (ССП)');
                vlaga_oil(sDecimalSpinEdit74);
                sRichEdit1.Lines.Add('---------------------------------------------------------');
              end;
          end;
      end;

    if ((sGroupBox45.Checked = True) or (sGroupBox49.Checked = True) or (sGroupBox51.Checked = True) or (sGroupBox53.Checked = True)) then
      begin
        if (sGroupBox47.Checked = True) then
          begin
            // Экстракционное Кислотное число
            if ( sDecimalSpinEdit75.Value <> 0 ) then
              begin
                UnderAndBold;
                sRichEdit1.Lines.Add('Масло экстракционное. Расчет кислотного числа (К.Ч.):');
                sRichEdit1.Lines.Add('СреднеСуточная Проба (ССП)');
                if sCheckBox1.Checked = true then
                  check := 1 else check := 0;
                kch_oil(sDecimalSpinEdit75);
              end;

            // Экстракционнон ВЛАГА
            if ( sDecimalSpinEdit76.Value <> 0 ) then
              begin
                UnderAndBold;
                sRichEdit1.Lines.Add('Масло экстракционное. Расчет м.д. влаги и лет. веществ, %:');
                sRichEdit1.Lines.Add('СреднеСуточная Проба (ССП)');
                vlaga_oil(sDecimalSpinEdit76);
              end;
          end;
      end;
   end;

  sRichEdit1.SelStart:=0;
  sRichEdit1.SelLength := 0;

end;

procedure TRafForm.UnderAndBold;
begin
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style+[fsUnderline];
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style+[fsBold];
end;

procedure TRafForm.PrintRichEd(ARichEd: TRichEdit; APortrait: boolean);
var
   dlg            : TPrintDialog;
   Margins        : TRect;
   iPixPerInchX   : integer;
   iPixPerInchY   : integer;
   prn            : TPrinter;
const
   CmPerInch      = 2.54;
begin
   dlg:=TPrintDialog.Create(nil);
   try
      prn:=Printer();

      if APortrait then
         prn.Orientation:=poPortrait
      else
         prn.Orientation:=poLandscape;

      if dlg.Execute() then
      begin
         iPixPerInchX:=GetDeviceCaps(prn.Handle, LOGPIXELSX);
         iPixPerInchY:=GetDeviceCaps(prn.Handle, LOGPIXELSY);

         //поля страницы в Pixel (Inches->Pixel)
         Margins.Left:=trunc(4.0/CmPerInch*iPixPerInchX);
         Margins.Top:=trunc(0.5/CmPerInch*iPixPerInchX);
         Margins.Right:=prn.PageWidth-trunc(0.5/CmPerInch*iPixPerInchX);
         Margins.Bottom:=prn.PageHeight-trunc(1.0/CmPerInch*iPixPerInchX);

         ARichEd.PageRect:=Margins;

         ARichEd.Print('PrintTask');
      end;
   finally
      dlg.Free();
   end;
end;

end.
