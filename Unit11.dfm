object RozlivForm: TRozlivForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1056#1086#1079#1083#1080#1074#1099' '#1043#1054#1057#1058' 31933-2012 '#1043#1054#1057#1058' 26593-85'
  ClientHeight = 536
  ClientWidth = 749
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001002020000001002000A81000001600000028000000200000004000
    0000010020000000000000100000000000000000000000000000000000000000
    000000000000000000000000000000000000333333023939390C3A3A3A163A3A
    3A1139393908373737032E2E2E00090909000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000002D2D2D013A3A3A164D4D4D5B7373738F4141
    41783A3A3A55393939363A3A3A1E3939390F3838380636363602272727000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000003737370563636349EFEFEFEDFBFBFBFFF2F2
    F2FBCDCDCDE6A3A3A3C76A6A6A9C3B3B3B703A3A3A4C3939392F3A3A3A1A3939
    390C38383805343434011F1F1F00000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000003939390EB7B7B790FAFAFAFFFAFAFAFFF9F9
    F9FFF4F4F4FFF5F5F5FFFAFAFAFFEAEAEAF7C2C2C2DF969696BC595959903939
    3966393939443A3A3A283A3A3A15393939093838380331313101080808000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000282828003A3A3A1DD8D8D8C5FAFAFAFFD9D9D9FF8383
    83FF6C6C6CFF6B6C6CFF888888FFDFDFDFFFFAFAFAFFF9F9F9FFF7F7F7FEDDDD
    DDF1B6B6B6D7888888B1484848833A3A3A5C3939393B393939223A3A3A113939
    3907373737022C2C2C0000000000000000000000000000000000000000000000
    00000000000000000000363636023D3D3D35F2F2F2F0DEDEDEFF787D7BFF5FBB
    8BFF5BE99EFF5BE79DFF65B58BFF7D7F7EFFE5E5E5FFF9F9F9FFF6F6F6FFF3F3
    F3FFF1F1F1FFF1F1F1FFEFEFEFFDD0D0D0EBAAAAAACE777777A63E3E3E793838
    3855353535382F2F2F23232323120F0F0F050000000000000000000000000000
    00000000000000000000383838078C8C8C66FAFAFAFEA4A4A4FF55AD80FF54F0
    9EFF7CFAB8FF78FAB5FF50ED9BFF66A986FFABABABFFF9F9F9FFF7F7F7FFF3F3
    F3FFF1F1F1FFF0F0F0FFEFEFEFFFEEEEEEFFEDEDEDFFEDEDEDFFE6E6E6FAC1C1
    C1E7969696CE585858AF252525821313133F0303030D00000000000000000000
    0000000000000606060039393912C1C1C1A0F9F9F9FF9C9E9DFF35BD77FF56F2
    A0FF6DF8AFFF6BF7AEFF53F19EFF46BB7EFFA4A4A4FFF9F9F9FFF7F7F7FFF4F4
    F4FFF1F1F1FFF0F0F0FFE6E6E6FFA3A3A3FF808080FF838383FFAAAAAAFFE3E3
    E3FFE5E5E5FFE8E8E8FECDCDCDFA999999D21B1B1B3700000004000000000000
    0000000000002C2C2C0139393923DFDFDFD2F9F9F9FFB1B2B1FF31AF6FFF4CEA
    97FF68F6ACFF66F5ABFF48E893FF45AF79FFB7B7B7FFFAFAFAFFF8F8F8FFF5F5
    F5FFF2F2F2FFE3E3E3FF7D7E7EFF6E9D99FF72CCC8FF71C7C2FF73928FFF8787
    87FFE2E2E2FFE3E3E3FFEBEBEBFFC3C3C3FF9292927A0000000A000000000000
    000000000000353535034D4D4D3FF6F6F6F7FAFAFAFFD1D1D1FF69A388FF48CC
    87FF71ECACFF74ECADFF44C381FF82AE99FFD3D3D3FFFAFAFAFFF9F9F9FFF6F6
    F6FFF3F3F3FFA4A4A4FF64A29CFF69EDEAFFA6F7FBFF9AF7FAFF5EE4DFFF7898
    95FFB1B1B1FFE5E5E5FFE8E8E8FFC8C8C8FFA6A6A6910000000D000000000000
    0000000000003838380A9F9F9F76FAFAFAFFFAFAFAFFF3F3F3FFD2D4D3FF7EAC
    97FF5AA782FF5FA784FF8BB1A0FFD4D5D5FFF5F5F5FFFAFAFAFFFAFAFAFFF8F8
    F8FFF3F3F3FF969797FF41BDB3FF78F4F5FF9AF7FAFF95F6F9FF6EF2F1FF56B3
    ABFFA2A2A2FFE7E7E7FFD3D3D3FFD4D4D4FFB4B4B4A000000010000000000000
    0000151515003A3A3A15CBCBCBAEFAFAFAFFFAFAFAFFFAFAFAFFF6F6F6FFE9E9
    E9FFE9E9E9FFE9E9E9FFE9E9E9FFF7F7F7FFFAFAFAFFFAFAFAFFFAFAFAFFF9F9
    F9FFF4F4F4FFA0A4A3FF36B6AAFF68F0EDFF86F7F7FF82F7F6FF60ECE8FF4BAE
    A5FFAFAFAFFFE9E9E9FFC5C6C5FFD5D5D5FFBFBFBFAE00000014000000000000
    0000323232013A3A3A29E6E6E6DEFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
    FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
    FAFFF8F8F8FFC4C4C4FF479790FF5BDDD4FF79F1EBFF7CF0EAFF50D1C7FF6EA3
    9EFFC7C7C7FFEFEFEFFFB0B7B7FFDCDCDCFFC9C9C9BC00000018000000000000
    0000373737056262624BF9F9F9FCFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
    FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
    FAFFF9F9F9FFE7E7E7FFB9C2C1FF4B948EFF60BDB5FF5FB7AFFF5C9893FFC8CB
    CAFFE5E5E5FFE5E8E7FFB7BCBCFFE9E9E9FFD3D3D3C90000001C000000000000
    00003939390CAEAEAE86F8F8F8FFF9F9F9FFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
    FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
    FAFFFAFAFAFFF9F9F9FFEBEBEBFFE0E1E1FFC0C9C9FFC7CECDFFE0E0E0FFE7E7
    E7FFEFEFEFFFD7D8D8FFD5D5D5FFF7F7F7FFDDDDDDD600000021000000012020
    20003939391AD1D1D1BCF7F7F7FFF9F9F9FFF9F9F9FFF3F3F3FFF6F6F6FFF9F9
    F9FFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
    FAFFFAFAFAFFFAFAFAFFF9F9F9FFF8F8F8FFF2F2F2FFF1F1F1FFF1F1F1FFF0F0
    F0FFF0F0F0FFD0D0D0FFEEEEEEFFF9F9F9FFE6E6E6E200000026000000013333
    33023A3A3A2FEBEBEBE9F6F6F6FFC7C7C7FF797979FF6C6B6AFF6B6A6AFF8F8F
    8FFFE8E8E8FFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
    FAFFFAFAFAFFFAFAFAFFFAFAFAFFF9F9F9FFF8F8F8FFF6F6F6FFF4F4F4FFF2F2
    F2FFF4F4F4FFCECECEFFF5F5F5FFFAFAFAFFEEEEEEED0000002C000000023737
    37067B7B7B5AF6F6F6FEC9C9C8FF817873FFC99C5CFFEBC04FFFE5B954FFAC8C
    69FF838383FFF0F0F0FFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
    FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFF9F9F9FFF8F8F8FFF6F6
    F6FFEAEAEAFFD9D9D9FFF8F8F8FFFAFAFAFFF5F5F5F800000032000000033939
    390FB9B9B995F2F2F2FF939393FFBD824AFFF3D150FFFBE155FFF9DD54FFE8B8
    48FFA18672FFBBBBBBFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
    FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFF9F9F9FFF9F9
    F9FFDCDCDCFFE8E8E8FFF9F9F9FFFAFAFAFFF9F9F9FD2323233E000000043939
    391FD7D7D7C9E9E9E9FF978E89FFCF8937FFF4D351FFF8DB55FFF7D953FFEEC4
    4CFFB58054FFB0B0B0FFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFE5E5
    E5FF9F9F9FFF858585FF919191FFCACACAFFF9F9F9FFFAFAFAFFFAFAFAFFFAFA
    FAFFD2D2D2FFF1F1F1FFFAFAFAFFFAFAFAFFFAFAFAFF4E4E4E4D000000054141
    4137EDEDEDF2ECECEBFFA8A19DFFBF732FFFEDC151FFF6D963FFF5D661FFE4AB
    43FFAF7B59FFC0C0C0FFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFD9D9D9FF7976
    79FFA96CA7FFCD73CAFFBB6FB9FF817281FFACACACFFF9F9F9FFFAFAFAFFF8F8
    F8FFCFCFCFFFF6F6F6FFFAFAFAFFFAFAFAFFFAFAFAFF7171715E000000069696
    9665F1F1F0FFEFEFEFFFC7C7C7FFA77255FFD59852FFECC370FFEABF74FFBB75
    3BFFB9A59CFFDDDDDDFFFAFAFAFFFAFAFAFFFAFAFAFFF8F8F8FF909090FFB458
    B2FFF378EFFFFCACF9FFF88DF4FFD458D1FF8A7F8AFFDBDBDBFFFAFAFAFFE1E1
    E1FFBDBDBDFFDADADAFFF1F1F1FFFAFAFAFFFAFAFAFF8989896E00000008CFCF
    CF80EEEEEDFFEEEEEEFFE6E6E5FFD1CECCFFAF8975FFB18260FFAE8165FFBFAA
    A0FFD7D7D7FFF8F8F8FFFAFAFAFFFAFAFAFFFAFAFAFFE5E5E5FF908092FFD74B
    D3FFF782F3FFFA9EF7FFF98FF5FFED62E8FFA469A5FFBCBCBCFFFAFAFAFFD3C9
    D3FFDDC2DCFFE0CBDFFFCFCECFFFECECECFFFAFAFAFF9C9C9C7D0000000AB4B4
    B439F3F3F3F2EEEEEDFFEFEFEEFFEBEBEAFFE4E4E4FFE7E7E6FFE7E7E7FFE6E6
    E6FFF5F5F5FFF8F8F8FFF9F9F9FFFAFAFAFFFAFAFAFFE1E1E1FF9B849EFFD245
    CEFFF472EFFFF887F5FFF67DF3FFE758E2FFA563A6FFC2C2C2FFFAFAFAFFD0BD
    CFFFF3DAF3FFF8D0F6FFE6C6E5FFD5D5D5FFF8F8F8FFAAAAAA8D0000000D3737
    3703A8A8A824DDDDDD6EE8E8E8AFF1F1F1E9F3F3F2FEF1F1F1FFF3F3F2FFF4F4
    F3FFF5F5F5FFF6F6F6FFF7F7F7FFF9F9F9FFF9F9F9FFF5F5F5FFB6B2B7FFA034
    A2FFE66AE3FFF177EFFFED79EBFFBF40BEFFAE91AFFFD9D9D9FFF5F3F5FFD1B6
    D0FFF7D9F6FFF9D3F7FFF0C2EFFFDAD2DAFFEEEEEEFFB6B6B69C000000100000
    000022222200353535023939390649494910C1C1C13ED7D7D787E7E7E6E7F4F4
    F4FFF5F5F5FFF5F5F4FFF6F6F6FFF7F7F7FFF8F8F8FFF9F9F9FFDDDDDDFFB59F
    B8FF9B469EFFC76DC8FFAD55B0FFA274A5FFD0D0D0FFF7F7F7FFE2D8E1FFE1BE
    E0FFF8D3F7FFF8CEF6FFEEC0EDFFDDD3DEFFEEEEEEFFC1C1C1AA000000130000
    0000000000000000000000000000070707002D2D2D000B0B0B0FCCCCCCAFCCCC
    CCFFCBB8ACFFE8D9BFFFF1EEE1FFF8F8F6FFF7F7F7FFF8F8F8FFF8F8F8FFE7E7
    E7FFDCDADCFFC6BAC8FFD2CDD2FFE3E3E3FFF4F4F4FFFAFAFAFFD0BED0FFE9C5
    E9FFF6CEF6FFF3CBF2FFDEB9DFFFE3E0E3FFF4F4F4FFCACACAB8000000160000
    000000000000000000000000000000000000000000000000000ACDCDCD9EDFDF
    DFFFC9B7AEFFDBB286FFE9D49FFFE6D49EFFE5D3B5FFDFD0C6FFF1F1F1FFFAFA
    FAFFF8F8F8FFF4F4F4FFF7F7F7FFFAFAFAFFFAFAFAFFFBFBFBFFBDB3BEFFDABD
    DBFFE8CBE8FFDABDDBFFE0D7E1FFECECECFFF6F6F6FFD4D4D4C3000000180000
    0000000000000000000000000000000000000000000000000008C5C5C58DEEEE
    EEFFDAD9D9FFC6A999FFDBBB9EFFE5CAA9FFCDA88DFFCDBFB8FFD8D8D8FFE7E7
    E7FFEBEBEAFFEFEFEFFFF8F8F8FFFAFAFAFFFAFAFAFFF1F1F1FFDBDBDBFFE8E7
    E8FFE5E0E5FFEBEAEBFFEEEEEEFFF4F4F4FFF4F4F4FFE1E1E1C2000000100000
    0000000000000000000000000000000000000000000000000005C4C4C378EFEF
    EEFFEBEBEAFFE5E5E4FFDAD1CDFFD6C9C3FFDFDCDAFFE6E6E6FFF1F1F1FFF2F2
    F2FFF1F1F0FFEEEEEDFFE7E7E7FFE8E8E7FFEBEBEBFFE6E6E6FFF0F0F0FFF3F3
    F2FFF1F1F1FFF3F3F2FFF3F3F3FFF3F3F2FFF4F4F4F9D1D1D162000000040000
    0000000000000000000000000000000000000000000000000001BBBBBB3FEFEF
    EFFDEEEEEDFFEEEEEDFFEBEBEAFFEAEAE9FFEDEDEDFFF1F1F0FFF2F2F1FFF3F3
    F2FDF2F2F1F4F0F0EFE5EDEDEDD6EAEAEAC6E7E7E7B5E5E5E5A5E3E3E294DFDF
    DF83DBDBDB71D6D6D660CECECE4FC3C3C33C9999991D00000004000000000000
    000000000000000000000000000000000000000000000000000000000004DBDB
    DB4EE3E3E28ADCDCDC7ED8D8D86DD3D3D35BCBCBCB4ABCBCBC389D9D9D275D5D
    5D171818180E0000000A00000008000000060000000500000004000000030000
    000200000001000000010000000000000000000000000000000000000000F81F
    FFFFF001FFFFF0001FFFF00001FFF000003FE0000007E0000003E0000001C000
    0001C0000001C0000001C0000001800000018000000180000000800000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000C0000000FC000000FC000000FC000000FC000000FC000001FE00001F}
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object sPageControl1: TsPageControl
    Left = 0
    Top = 0
    Width = 749
    Height = 536
    Cursor = crHandPoint
    ActivePage = sTabSheet1
    Align = alClient
    TabOrder = 0
    object sTabSheet1: TsTabSheet
      Caption = #1042#1074#1086#1076#1085#1072#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
      object sScrollBox1: TsScrollBox
        Left = 0
        Top = 0
        Width = 741
        Height = 508
        VertScrollBar.Increment = 25
        VertScrollBar.Margin = 8
        VertScrollBar.Tracking = True
        Align = alClient
        AutoMouseWheel = True
        TabOrder = 0
        object sGroupBox1: TsGroupBox
          Left = 12
          Top = 11
          Width = 565
          Height = 102
          Caption = #1056#1086#1079#1083#1080#1074':'
          TabOrder = 0
          CaptionSkin = 'PAGECONTROLLEFT'
          CheckBoxVisible = True
          Checked = True
          OnCheckBoxChanged = sGroupBox1CheckBoxChanged
          object sLabel1: TsLabel
            Left = 16
            Top = 67
            Width = 41
            Height = 13
            Caption = #1057#1090#1088#1072#1085#1072':'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsUnderline]
          end
          object sLabel2: TsLabel
            Left = 232
            Top = 44
            Width = 100
            Height = 13
            Caption = #1050#1080#1089#1083#1086#1090#1085#1086#1077' '#1095#1080#1089#1083#1086':'
            Color = clBlack
            ParentColor = False
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
          end
          object sLabel3: TsLabel
            Left = 232
            Top = 70
            Width = 108
            Height = 13
            Caption = #1055#1077#1088#1077#1082#1080#1089#1085#1086#1077' '#1095#1080#1089#1083#1086':'
            Color = clBlack
            ParentColor = False
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
          end
          object sLabel4: TsLabel
            Left = 464
            Top = 44
            Width = 48
            Height = 13
            Caption = #1084#1075' KOH/'#1075'.'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
          end
          object sLabel5: TsLabel
            Left = 464
            Top = 70
            Width = 79
            Height = 13
            Caption = #1084#1084#1086#1083#1100' 1/2 O/'#1082#1075'.'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
          end
          object sComboBox1: TsComboBox
            AlignWithMargins = True
            Left = 16
            Top = 27
            Width = 73
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = 0
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 0
            Text = '1/1'
            OnChange = sComboBox1Change
            Items.Strings = (
              '1/1'
              '1/2'
              '2/1'
              '2/2/1'
              '2/2/2'
              '2/2/4')
          end
          object sComboBox2: TsComboBox
            AlignWithMargins = True
            Left = 95
            Top = 27
            Width = 113
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = 0
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 1
            Text = #1056#1044#1055' '#1040#1085#1085'.'
            OnChange = sComboBox1Change
            Items.Strings = (
              #1056#1044#1055' '#1040#1085#1085'.'
              #1056#1044#1055' '#1047'.'#1057'.'
              #1056#1044#1055' '#1047'.'#1057'.('#1090'/'#1091')'
              #1056#1044#1055' '#1047'.'#1057'.('#1072#1082#1094'.)'
              #1056#1044#1055' '#1047#1083#1072#1090#1086
              #1056#1044#1055' 100'#1056#1077#1094'.'
              #1056#1044#1055' '#1050'.'#1044'.'
              #1056#1044#1055' '#1050'.'#1062'.'
              #1056#1044#1055' '#1070#1075' '#1056#1091#1089#1080
              #1056#1044#1055' '#1070'.'#1057
              #1056#1072#1089#1090'. '#1040#1085#1085'.'
              #1056#1072#1089#1090'. '#1070#1075' '#1056#1091#1089#1080
              #1056#1072#1089#1090'. '#1070' '#1057'.'
              #1053'/'#1088' '#1047'.'#1057'.'
              #1053'/'#1088' '#1070#1075' '#1056#1091#1089#1080)
          end
          object sComboBox3: TsComboBox
            AlignWithMargins = True
            Left = 71
            Top = 64
            Width = 137
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = -1
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 2
            OnChange = sComboBox1Change
            Items.Strings = (
              ''
              #1040#1088#1084#1077#1085#1080#1103
              #1040#1092#1075#1072#1085#1080#1089#1090#1072#1085
              #1041#1077#1083#1086#1088#1091#1089#1100
              #1043#1077#1088#1084#1072#1085#1080#1103
              #1043#1088#1091#1079#1080#1103
              #1050#1072#1079#1072#1093#1089#1090#1072#1085
              #1050#1080#1088#1075#1080#1079#1080#1103
              #1051#1072#1090#1074#1080#1103
              #1051#1080#1090#1074#1072
              #1059#1079#1073#1077#1082#1080#1089#1090#1072#1085
              #1063#1077#1093#1080#1103
              #1069#1089#1090#1086#1085#1080#1103)
          end
          object sDecimalSpinEdit1: TsDecimalSpinEdit
            Left = 360
            Top = 41
            Width = 81
            Height = 21
            Cursor = crHandPoint
            Alignment = taCenter
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            Text = '0,09'
            Increment = 0.010000000000000000
            MaxValue = 10.000000000000000000
            MinValue = 0.060000000000000000
            Value = 0.090000000000000000
          end
          object sDecimalSpinEdit2: TsDecimalSpinEdit
            Left = 360
            Top = 67
            Width = 81
            Height = 21
            Cursor = crHandPoint
            Alignment = taCenter
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Text = '0,60'
            Increment = 0.010000000000000000
            MaxValue = 10.000000000000000000
            MinValue = 0.200000000000000000
            Value = 0.600000000000000000
          end
        end
        object sGroupBox2: TsGroupBox
          Left = 12
          Top = 118
          Width = 565
          Height = 102
          Caption = #1056#1086#1079#1083#1080#1074':'
          TabOrder = 1
          CaptionSkin = 'PAGECONTROLLEFT'
          CheckBoxVisible = True
          OnCheckBoxChanged = sGroupBox2CheckBoxChanged
          object sLabel6: TsLabel
            Left = 16
            Top = 68
            Width = 41
            Height = 13
            Caption = #1057#1090#1088#1072#1085#1072':'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsUnderline]
          end
          object sLabel7: TsLabel
            Left = 232
            Top = 44
            Width = 100
            Height = 13
            Caption = #1050#1080#1089#1083#1086#1090#1085#1086#1077' '#1095#1080#1089#1083#1086':'
            Color = clBlack
            ParentColor = False
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
          end
          object sLabel8: TsLabel
            Left = 232
            Top = 70
            Width = 108
            Height = 13
            Caption = #1055#1077#1088#1077#1082#1080#1089#1085#1086#1077' '#1095#1080#1089#1083#1086':'
            Color = clBlack
            ParentColor = False
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
          end
          object sLabel9: TsLabel
            Left = 464
            Top = 44
            Width = 48
            Height = 13
            Caption = #1084#1075' KOH/'#1075'.'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
          end
          object sLabel10: TsLabel
            Left = 464
            Top = 70
            Width = 79
            Height = 13
            Caption = #1084#1084#1086#1083#1100' 1/2 O/'#1082#1075'.'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
          end
          object sComboBox4: TsComboBox
            AlignWithMargins = True
            Left = 16
            Top = 27
            Width = 73
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = 0
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 0
            Text = '1/1'
            OnChange = sComboBox4Change
            Items.Strings = (
              '1/1'
              '1/2'
              '2/1'
              '2/2/1'
              '2/2/2'
              '2/2/4')
          end
          object sComboBox5: TsComboBox
            AlignWithMargins = True
            Left = 95
            Top = 27
            Width = 113
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = -1
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 1
            OnChange = sComboBox4Change
            Items.Strings = (
              #1056#1044#1055' '#1040#1085#1085'.'
              #1056#1044#1055' '#1047'.'#1057'.'
              #1056#1044#1055' '#1047'.'#1057'.('#1090'/'#1091')'
              #1056#1044#1055' '#1047'.'#1057'.('#1072#1082#1094'.)'
              #1056#1044#1055' '#1047#1083#1072#1090#1086
              #1056#1044#1055' 100'#1056#1077#1094'.'
              #1056#1044#1055' '#1050'.'#1044'.'
              #1056#1044#1055' '#1050'.'#1062'.'
              #1056#1044#1055' '#1070#1075' '#1056#1091#1089#1080
              #1056#1044#1055' '#1070'.'#1057
              #1056#1072#1089#1090'. '#1040#1085#1085'.'
              #1056#1072#1089#1090'. '#1070#1075' '#1056#1091#1089#1080
              #1056#1072#1089#1090'. '#1070' '#1057'.'
              #1053'/'#1088' '#1047'.'#1057'.'
              #1053'/'#1088' '#1070#1075' '#1056#1091#1089#1080)
          end
          object sComboBox6: TsComboBox
            AlignWithMargins = True
            Left = 71
            Top = 64
            Width = 137
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = -1
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 2
            OnChange = sComboBox4Change
            Items.Strings = (
              ''
              #1040#1088#1084#1077#1085#1080#1103
              #1040#1092#1075#1072#1085#1080#1089#1090#1072#1085
              #1041#1077#1083#1086#1088#1091#1089#1100
              #1043#1077#1088#1084#1072#1085#1080#1103
              #1043#1088#1091#1079#1080#1103
              #1050#1072#1079#1072#1093#1089#1090#1072#1085
              #1050#1080#1088#1075#1080#1079#1080#1103
              #1051#1072#1090#1074#1080#1103
              #1051#1080#1090#1074#1072
              #1059#1079#1073#1077#1082#1080#1089#1090#1072#1085
              #1063#1077#1093#1080#1103
              #1069#1089#1090#1086#1085#1080#1103)
          end
          object sDecimalSpinEdit3: TsDecimalSpinEdit
            Left = 360
            Top = 41
            Width = 81
            Height = 21
            Cursor = crHandPoint
            Alignment = taCenter
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            Text = '0,09'
            Increment = 0.010000000000000000
            MaxValue = 10.000000000000000000
            MinValue = 0.060000000000000000
            Value = 0.090000000000000000
          end
          object sDecimalSpinEdit4: TsDecimalSpinEdit
            Left = 360
            Top = 67
            Width = 81
            Height = 21
            Cursor = crHandPoint
            Alignment = taCenter
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Text = '0,60'
            Increment = 0.010000000000000000
            MaxValue = 10.000000000000000000
            MinValue = 0.200000000000000000
            Value = 0.600000000000000000
          end
        end
        object sGroupBox3: TsGroupBox
          Left = 12
          Top = 225
          Width = 565
          Height = 102
          Caption = #1056#1086#1079#1083#1080#1074':'
          TabOrder = 2
          CaptionSkin = 'PAGECONTROLLEFT'
          CheckBoxVisible = True
          OnCheckBoxChanged = sGroupBox3CheckBoxChanged
          object sLabel11: TsLabel
            Left = 16
            Top = 68
            Width = 41
            Height = 13
            Caption = #1057#1090#1088#1072#1085#1072':'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsUnderline]
          end
          object sLabel12: TsLabel
            Left = 232
            Top = 44
            Width = 100
            Height = 13
            Caption = #1050#1080#1089#1083#1086#1090#1085#1086#1077' '#1095#1080#1089#1083#1086':'
            Color = clBlack
            ParentColor = False
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
          end
          object sLabel13: TsLabel
            Left = 232
            Top = 70
            Width = 108
            Height = 13
            Caption = #1055#1077#1088#1077#1082#1080#1089#1085#1086#1077' '#1095#1080#1089#1083#1086':'
            Color = clBlack
            ParentColor = False
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
          end
          object sLabel14: TsLabel
            Left = 464
            Top = 44
            Width = 48
            Height = 13
            Caption = #1084#1075' KOH/'#1075'.'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
          end
          object sLabel15: TsLabel
            Left = 464
            Top = 70
            Width = 79
            Height = 13
            Caption = #1084#1084#1086#1083#1100' 1/2 O/'#1082#1075'.'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
          end
          object sComboBox7: TsComboBox
            AlignWithMargins = True
            Left = 16
            Top = 27
            Width = 73
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = 0
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 0
            Text = '1/1'
            OnChange = sComboBox7Change
            Items.Strings = (
              '1/1'
              '1/2'
              '2/1'
              '2/2/1'
              '2/2/2'
              '2/2/4')
          end
          object sComboBox8: TsComboBox
            AlignWithMargins = True
            Left = 95
            Top = 27
            Width = 113
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = -1
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 1
            OnChange = sComboBox7Change
            Items.Strings = (
              #1056#1044#1055' '#1040#1085#1085'.'
              #1056#1044#1055' '#1047'.'#1057'.'
              #1056#1044#1055' '#1047'.'#1057'.('#1090'/'#1091')'
              #1056#1044#1055' '#1047'.'#1057'.('#1072#1082#1094'.)'
              #1056#1044#1055' '#1047#1083#1072#1090#1086
              #1056#1044#1055' 100'#1056#1077#1094'.'
              #1056#1044#1055' '#1050'.'#1044'.'
              #1056#1044#1055' '#1050'.'#1062'.'
              #1056#1044#1055' '#1070#1075' '#1056#1091#1089#1080
              #1056#1044#1055' '#1070'.'#1057
              #1056#1072#1089#1090'. '#1040#1085#1085'.'
              #1056#1072#1089#1090'. '#1070#1075' '#1056#1091#1089#1080
              #1056#1072#1089#1090'. '#1070' '#1057'.'
              #1053'/'#1088' '#1047'.'#1057'.'
              #1053'/'#1088' '#1070#1075' '#1056#1091#1089#1080)
          end
          object sComboBox9: TsComboBox
            AlignWithMargins = True
            Left = 71
            Top = 64
            Width = 137
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = -1
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 2
            OnChange = sComboBox7Change
            Items.Strings = (
              ''
              #1040#1088#1084#1077#1085#1080#1103
              #1040#1092#1075#1072#1085#1080#1089#1090#1072#1085
              #1041#1077#1083#1086#1088#1091#1089#1100
              #1043#1077#1088#1084#1072#1085#1080#1103
              #1043#1088#1091#1079#1080#1103
              #1050#1072#1079#1072#1093#1089#1090#1072#1085
              #1050#1080#1088#1075#1080#1079#1080#1103
              #1051#1072#1090#1074#1080#1103
              #1051#1080#1090#1074#1072
              #1059#1079#1073#1077#1082#1080#1089#1090#1072#1085
              #1063#1077#1093#1080#1103
              #1069#1089#1090#1086#1085#1080#1103)
          end
          object sDecimalSpinEdit5: TsDecimalSpinEdit
            Left = 360
            Top = 41
            Width = 81
            Height = 21
            Cursor = crHandPoint
            Alignment = taCenter
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            Text = '0,09'
            Increment = 0.010000000000000000
            MaxValue = 10.000000000000000000
            MinValue = 0.060000000000000000
            Value = 0.090000000000000000
          end
          object sDecimalSpinEdit6: TsDecimalSpinEdit
            Left = 360
            Top = 67
            Width = 81
            Height = 21
            Cursor = crHandPoint
            Alignment = taCenter
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Text = '0,60'
            Increment = 0.010000000000000000
            MaxValue = 10.000000000000000000
            MinValue = 0.200000000000000000
            Value = 0.600000000000000000
          end
        end
        object sGroupBox4: TsGroupBox
          Left = 12
          Top = 332
          Width = 565
          Height = 102
          Caption = #1056#1086#1079#1083#1080#1074':'
          TabOrder = 3
          CaptionSkin = 'PAGECONTROLLEFT'
          CheckBoxVisible = True
          OnCheckBoxChanged = sGroupBox4CheckBoxChanged
          object sLabel16: TsLabel
            Left = 16
            Top = 68
            Width = 41
            Height = 13
            Caption = #1057#1090#1088#1072#1085#1072':'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsUnderline]
          end
          object sLabel17: TsLabel
            Left = 232
            Top = 44
            Width = 100
            Height = 13
            Caption = #1050#1080#1089#1083#1086#1090#1085#1086#1077' '#1095#1080#1089#1083#1086':'
            Color = clBlack
            ParentColor = False
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
          end
          object sLabel18: TsLabel
            Left = 232
            Top = 70
            Width = 108
            Height = 13
            Caption = #1055#1077#1088#1077#1082#1080#1089#1085#1086#1077' '#1095#1080#1089#1083#1086':'
            Color = clBlack
            ParentColor = False
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
          end
          object sLabel19: TsLabel
            Left = 464
            Top = 44
            Width = 48
            Height = 13
            Caption = #1084#1075' KOH/'#1075'.'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
          end
          object sLabel20: TsLabel
            Left = 464
            Top = 70
            Width = 79
            Height = 13
            Caption = #1084#1084#1086#1083#1100' 1/2 O/'#1082#1075'.'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
          end
          object sComboBox10: TsComboBox
            AlignWithMargins = True
            Left = 16
            Top = 27
            Width = 73
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = 0
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 0
            Text = '1/1'
            OnChange = sComboBox10Change
            Items.Strings = (
              '1/1'
              '1/2'
              '2/1'
              '2/2/1'
              '2/2/2'
              '2/2/4')
          end
          object sComboBox11: TsComboBox
            AlignWithMargins = True
            Left = 95
            Top = 27
            Width = 113
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = -1
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 1
            OnChange = sComboBox10Change
            Items.Strings = (
              #1056#1044#1055' '#1040#1085#1085'.'
              #1056#1044#1055' '#1047'.'#1057'.'
              #1056#1044#1055' '#1047'.'#1057'.('#1090'/'#1091')'
              #1056#1044#1055' '#1047'.'#1057'.('#1072#1082#1094'.)'
              #1056#1044#1055' '#1047#1083#1072#1090#1086
              #1056#1044#1055' 100'#1056#1077#1094'.'
              #1056#1044#1055' '#1050'.'#1044'.'
              #1056#1044#1055' '#1050'.'#1062'.'
              #1056#1044#1055' '#1070#1075' '#1056#1091#1089#1080
              #1056#1044#1055' '#1070'.'#1057
              #1056#1072#1089#1090'. '#1040#1085#1085'.'
              #1056#1072#1089#1090'. '#1070#1075' '#1056#1091#1089#1080
              #1056#1072#1089#1090'. '#1070' '#1057'.'
              #1053'/'#1088' '#1047'.'#1057'.'
              #1053'/'#1088' '#1070#1075' '#1056#1091#1089#1080)
          end
          object sComboBox12: TsComboBox
            AlignWithMargins = True
            Left = 71
            Top = 64
            Width = 137
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = -1
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 2
            OnChange = sComboBox10Change
            Items.Strings = (
              ''
              #1040#1088#1084#1077#1085#1080#1103
              #1040#1092#1075#1072#1085#1080#1089#1090#1072#1085
              #1041#1077#1083#1086#1088#1091#1089#1100
              #1043#1077#1088#1084#1072#1085#1080#1103
              #1043#1088#1091#1079#1080#1103
              #1050#1072#1079#1072#1093#1089#1090#1072#1085
              #1050#1080#1088#1075#1080#1079#1080#1103
              #1051#1072#1090#1074#1080#1103
              #1051#1080#1090#1074#1072
              #1059#1079#1073#1077#1082#1080#1089#1090#1072#1085
              #1063#1077#1093#1080#1103
              #1069#1089#1090#1086#1085#1080#1103)
          end
          object sDecimalSpinEdit7: TsDecimalSpinEdit
            Left = 360
            Top = 41
            Width = 81
            Height = 21
            Cursor = crHandPoint
            Alignment = taCenter
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            Text = '0,09'
            Increment = 0.010000000000000000
            MaxValue = 10.000000000000000000
            MinValue = 0.060000000000000000
            Value = 0.090000000000000000
          end
          object sDecimalSpinEdit8: TsDecimalSpinEdit
            Left = 360
            Top = 67
            Width = 81
            Height = 21
            Cursor = crHandPoint
            Alignment = taCenter
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Text = '0,60'
            Increment = 0.010000000000000000
            MaxValue = 10.000000000000000000
            MinValue = 0.200000000000000000
            Value = 0.600000000000000000
          end
        end
        object sGroupBox5: TsGroupBox
          Left = 12
          Top = 439
          Width = 565
          Height = 102
          Caption = #1056#1086#1079#1083#1080#1074':'
          TabOrder = 4
          CaptionSkin = 'PAGECONTROLLEFT'
          CheckBoxVisible = True
          OnCheckBoxChanged = sGroupBox5CheckBoxChanged
          object sLabel21: TsLabel
            Left = 16
            Top = 68
            Width = 41
            Height = 13
            Caption = #1057#1090#1088#1072#1085#1072':'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsUnderline]
          end
          object sLabel22: TsLabel
            Left = 232
            Top = 44
            Width = 100
            Height = 13
            Caption = #1050#1080#1089#1083#1086#1090#1085#1086#1077' '#1095#1080#1089#1083#1086':'
            Color = clBlack
            ParentColor = False
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
          end
          object sLabel23: TsLabel
            Left = 232
            Top = 70
            Width = 108
            Height = 13
            Caption = #1055#1077#1088#1077#1082#1080#1089#1085#1086#1077' '#1095#1080#1089#1083#1086':'
            Color = clBlack
            ParentColor = False
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
          end
          object sLabel24: TsLabel
            Left = 464
            Top = 44
            Width = 48
            Height = 13
            Caption = #1084#1075' KOH/'#1075'.'
            ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
          end
          object sLabel25: TsLabel
            Left = 464
            Top = 70
            Width = 79
            Height = 13
            Caption = #1084#1084#1086#1083#1100' 1/2 O/'#1082#1075'.'
          end
          object sComboBox13: TsComboBox
            AlignWithMargins = True
            Left = 16
            Top = 27
            Width = 73
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = 0
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 0
            Text = '1/1'
            OnChange = sComboBox13Change
            Items.Strings = (
              '1/1'
              '1/2'
              '2/1'
              '2/2/1'
              '2/2/2'
              '2/2/4')
          end
          object sComboBox14: TsComboBox
            AlignWithMargins = True
            Left = 95
            Top = 27
            Width = 113
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = -1
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 1
            OnChange = sComboBox13Change
            Items.Strings = (
              #1056#1044#1055' '#1040#1085#1085'.'
              #1056#1044#1055' '#1047'.'#1057'.'
              #1056#1044#1055' '#1047'.'#1057'.('#1090'/'#1091')'
              #1056#1044#1055' '#1047'.'#1057'.('#1072#1082#1094'.)'
              #1056#1044#1055' '#1047#1083#1072#1090#1086
              #1056#1044#1055' 100'#1056#1077#1094'.'
              #1056#1044#1055' '#1050'.'#1044'.'
              #1056#1044#1055' '#1050'.'#1062'.'
              #1056#1044#1055' '#1070#1075' '#1056#1091#1089#1080
              #1056#1044#1055' '#1070'.'#1057
              #1056#1072#1089#1090'. '#1040#1085#1085'.'
              #1056#1072#1089#1090'. '#1070#1075' '#1056#1091#1089#1080
              #1056#1072#1089#1090'. '#1070' '#1057'.'
              #1053'/'#1088' '#1047'.'#1057'.'
              #1053'/'#1088' '#1070#1075' '#1056#1091#1089#1080)
          end
          object sComboBox15: TsComboBox
            AlignWithMargins = True
            Left = 71
            Top = 64
            Width = 137
            Height = 21
            Cursor = crHandPoint
            AutoCloseUp = True
            AutoDropDown = True
            Alignment = taCenter
            VerticalAlignment = taVerticalCenter
            Style = csDropDownList
            BiDiMode = bdLeftToRight
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = -1
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 2
            OnChange = sComboBox13Change
            Items.Strings = (
              ''
              #1040#1088#1084#1077#1085#1080#1103
              #1040#1092#1075#1072#1085#1080#1089#1090#1072#1085
              #1041#1077#1083#1086#1088#1091#1089#1100
              #1043#1077#1088#1084#1072#1085#1080#1103
              #1043#1088#1091#1079#1080#1103
              #1050#1072#1079#1072#1093#1089#1090#1072#1085
              #1050#1080#1088#1075#1080#1079#1080#1103
              #1051#1072#1090#1074#1080#1103
              #1051#1080#1090#1074#1072
              #1059#1079#1073#1077#1082#1080#1089#1090#1072#1085
              #1063#1077#1093#1080#1103
              #1069#1089#1090#1086#1085#1080#1103)
          end
          object sDecimalSpinEdit9: TsDecimalSpinEdit
            Left = 360
            Top = 41
            Width = 81
            Height = 21
            Cursor = crHandPoint
            Alignment = taCenter
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            Text = '0,09'
            Increment = 0.010000000000000000
            MaxValue = 10.000000000000000000
            MinValue = 0.060000000000000000
            Value = 0.090000000000000000
          end
          object sDecimalSpinEdit10: TsDecimalSpinEdit
            Left = 360
            Top = 67
            Width = 81
            Height = 21
            Cursor = crHandPoint
            Alignment = taCenter
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Text = '0,60'
            Increment = 0.010000000000000000
            MaxValue = 10.000000000000000000
            MinValue = 0.200000000000000000
            Value = 0.600000000000000000
          end
        end
        object sBitBtn1: TsBitBtn
          Left = 592
          Top = 18
          Width = 97
          Height = 62
          Cursor = crHandPoint
          Caption = #1057#1095#1080#1090#1072#1090#1100
          DoubleBuffered = True
          ParentDoubleBuffered = False
          TabOrder = 5
          OnClick = sBitBtn1Click
          Grayed = True
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099' / '#1050'.'#1063'. / '
      object sRichEdit1: TsRichEdit
        Left = 0
        Top = 0
        Width = 741
        Height = 508
        Align = alClient
        Color = clBtnFace
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099' / '#1055'.'#1063'. / '
      object sRichEdit2: TsRichEdit
        Left = 0
        Top = 0
        Width = 741
        Height = 508
        Align = alClient
        Color = clBtnFace
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = #1050#1086#1088#1088#1077#1082#1090#1080#1088#1091#1102#1097#1080#1077
      object sLabel26: TsLabel
        Left = 24
        Top = 16
        Width = 101
        Height = 13
        Caption = #1050#1086#1085#1094#1077#1085#1090#1088#1072#1094#1080#1103' KOH:'
        ParentFont = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
      end
      object sLabel27: TsLabel
        Left = 24
        Top = 48
        Width = 127
        Height = 13
        Caption = #1050#1086#1085#1094#1077#1085#1090#1088'. '#1090#1080#1086#1089#1091#1083#1100#1092#1072#1090#1072':'
        ParentFont = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
      end
      object sLabel28: TsLabel
        Left = 218
        Top = 16
        Width = 59
        Height = 13
        Caption = #1084#1086#1083#1100'-'#1101#1082#1074'./'#1083
        ParentFont = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
      end
      object sLabel29: TsLabel
        Left = 256
        Top = 48
        Width = 59
        Height = 13
        Caption = #1084#1086#1083#1100'-'#1101#1082#1074'./'#1083
        ParentFont = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
      end
      object sLabel30: TsLabel
        Left = 24
        Top = 80
        Width = 159
        Height = 13
        Caption = #1044#1080#1072#1087#1072#1079#1086#1085' '#1085#1072#1074#1077#1089#1086#1082' '#1076#1083#1103' '#1055'.'#1063'. '#1086#1090':'
        ParentFont = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
      end
      object sLabel31: TsLabel
        Left = 275
        Top = 80
        Width = 13
        Height = 13
        Caption = #1076#1086
        ParentFont = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
      end
      object sLabel32: TsLabel
        Left = 391
        Top = 80
        Width = 9
        Height = 13
        Caption = #1075'.'
        ParentFont = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
      end
      object concKOH: TsDecimalSpinEdit
        Left = 131
        Top = 13
        Width = 73
        Height = 21
        Cursor = crHandPoint
        Alignment = taCenter
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Text = '0,100'
        OnExit = concKOHExit
        Increment = 0.001000000000000000
        MaxValue = 0.500000000000000000
        MinValue = 0.005000000000000000
        Value = 0.100000000000000000
        DecimalPlaces = 3
      end
      object concTS: TsDecimalSpinEdit
        Left = 157
        Top = 45
        Width = 84
        Height = 21
        Cursor = crHandPoint
        Alignment = taCenter
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = '0,0025'
        OnExit = concTSExit
        Increment = 0.000100000000000000
        MaxValue = 0.003000000000000000
        MinValue = 0.002000000000000000
        Value = 0.002500000000000000
        DecimalPlaces = 4
      end
      object sDecimalSpinEdit13: TsDecimalSpinEdit
        Left = 192
        Top = 77
        Width = 67
        Height = 21
        Cursor = crHandPoint
        Alignment = taCenter
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        Text = '3,00'
        Increment = 1.000000000000000000
        MaxValue = 3.000000000000000000
        MinValue = 2.000000000000000000
        Value = 3.000000000000000000
      end
      object sDecimalSpinEdit14: TsDecimalSpinEdit
        Left = 304
        Top = 77
        Width = 73
        Height = 21
        Cursor = crHandPoint
        Alignment = taCenter
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        Text = '4,00'
        Increment = 1.000000000000000000
        MaxValue = 5.000000000000000000
        MinValue = 4.000000000000000000
        Value = 4.000000000000000000
      end
      object sGroupBox54: TsGroupBox
        Left = 24
        Top = 109
        Width = 140
        Height = 47
        Cursor = crHelp
        Hint = #1056#1072#1073#1086#1090#1072#1077#1090' '#1076#1083#1103' '#1050#1063'/ '#1055#1063'! '#1042' '#1085#1072#1096#1077#1084' '#1089#1083#1091#1095#1072#1077' '#1088#1077#1082#1086#1084#1077#1085#1076#1091#1077#1090#1089#1103' 0.05!'
        Caption = #1062#1077#1085#1072' '#1076#1077#1083#1077#1085#1080#1103' '#1073#1102#1088#1077#1090#1082#1080':'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        object sRadioButton1: TsRadioButton
          Left = 12
          Top = 19
          Width = 46
          Height = 19
          Cursor = crHandPoint
          Caption = '0,01'
          TabOrder = 0
        end
        object sRadioButton2: TsRadioButton
          Left = 79
          Top = 19
          Width = 46
          Height = 19
          Cursor = crHandPoint
          Caption = '0,05'
          Checked = True
          TabOrder = 1
          TabStop = True
        end
      end
    end
  end
end
