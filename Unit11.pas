﻿unit Unit11;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, sSkinManager, StdCtrls, sGroupBox,
  ComCtrls, sPageControl, sComboBox, sEdit, sSpinEdit, sLabel, sCheckBox,
  sScrollBox, Buttons, sBitBtn, sRichEdit, IniFiles, sRadioButton, Math;

type
  TRozlivForm = class(TForm)
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    sTabSheet4: TsTabSheet;
    sScrollBox1: TsScrollBox;
    sGroupBox1: TsGroupBox;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    sComboBox1: TsComboBox;
    sComboBox2: TsComboBox;
    sComboBox3: TsComboBox;
    sDecimalSpinEdit1: TsDecimalSpinEdit;
    sDecimalSpinEdit2: TsDecimalSpinEdit;
    sGroupBox2: TsGroupBox;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    sLabel10: TsLabel;
    sComboBox4: TsComboBox;
    sComboBox5: TsComboBox;
    sComboBox6: TsComboBox;
    sDecimalSpinEdit3: TsDecimalSpinEdit;
    sDecimalSpinEdit4: TsDecimalSpinEdit;
    sGroupBox3: TsGroupBox;
    sLabel11: TsLabel;
    sLabel12: TsLabel;
    sLabel13: TsLabel;
    sLabel14: TsLabel;
    sLabel15: TsLabel;
    sComboBox7: TsComboBox;
    sComboBox8: TsComboBox;
    sComboBox9: TsComboBox;
    sDecimalSpinEdit5: TsDecimalSpinEdit;
    sDecimalSpinEdit6: TsDecimalSpinEdit;
    sGroupBox4: TsGroupBox;
    sLabel16: TsLabel;
    sLabel17: TsLabel;
    sLabel18: TsLabel;
    sLabel19: TsLabel;
    sLabel20: TsLabel;
    sComboBox10: TsComboBox;
    sComboBox11: TsComboBox;
    sComboBox12: TsComboBox;
    sDecimalSpinEdit7: TsDecimalSpinEdit;
    sDecimalSpinEdit8: TsDecimalSpinEdit;
    sGroupBox5: TsGroupBox;
    sLabel21: TsLabel;
    sLabel22: TsLabel;
    sLabel23: TsLabel;
    sLabel24: TsLabel;
    sLabel25: TsLabel;
    sComboBox13: TsComboBox;
    sComboBox14: TsComboBox;
    sComboBox15: TsComboBox;
    sDecimalSpinEdit9: TsDecimalSpinEdit;
    sDecimalSpinEdit10: TsDecimalSpinEdit;
    sBitBtn1: TsBitBtn;
    sLabel26: TsLabel;
    sLabel27: TsLabel;
    sLabel28: TsLabel;
    concKOH: TsDecimalSpinEdit;
    concTS: TsDecimalSpinEdit;
    sDecimalSpinEdit13: TsDecimalSpinEdit;
    sDecimalSpinEdit14: TsDecimalSpinEdit;
    sLabel29: TsLabel;
    sLabel30: TsLabel;
    sLabel31: TsLabel;
    sLabel32: TsLabel;
    sRichEdit1: TsRichEdit;
    sRichEdit2: TsRichEdit;
    sGroupBox54: TsGroupBox;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure sCheckBox1Click(Sender: TObject);
    procedure sComboBox1Change(Sender: TObject);
    procedure sCheckBox2Click(Sender: TObject);
    procedure sCheckBox3Click(Sender: TObject);
    procedure sCheckBox4Click(Sender: TObject);
    procedure sCheckBox5Click(Sender: TObject);
    procedure sComboBox4Change(Sender: TObject);
    procedure sComboBox7Change(Sender: TObject);
    procedure sComboBox10Change(Sender: TObject);
    procedure sComboBox13Change(Sender: TObject);
    procedure sGroupBox1CheckBoxChanged(Sender: TObject);
    procedure sGroupBox2CheckBoxChanged(Sender: TObject);
    procedure sGroupBox3CheckBoxChanged(Sender: TObject);
    procedure sGroupBox4CheckBoxChanged(Sender: TObject);
    procedure sGroupBox5CheckBoxChanged(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure concTSExit(Sender: TObject);
    procedure concKOHExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure changeBox1Caption;
    procedure changeBox2Caption;
    procedure changeBox3Caption;
    procedure changeBox4Caption;
    procedure changeBox5Caption;
    procedure UnderAndBold;
    procedure UnderAndBold2;
    function AddColorText(Text: String; Color: TColor): String;
    function AddColorText2(Text: String; Color: TColor): String;
    procedure kch_oil(decEdt: TsDecimalSpinEdit);
    procedure pch_oil(decEdt: TsDecimalSpinEdit);
    function deltaKch(kch: Extended): Extended;
    function deltaPch(pch: Extended): Extended;
    { Public declarations }
  end;

var
  RozlivForm: TRozlivForm;
  Ini: TIniFile;
  m1, m2, m_1, m_2, v1, v2, kch1, kch2, pch1, pch2: Extended;
  TKOH, TTS, d1d2: Extended;

implementation

uses DecimalRounding_JH1, Unit2;

{$R *.dfm}

{ TForm1 }

// Переименовывание Гроупбоксов
procedure TRozlivForm.changeBox1Caption;
begin
  if ( sGroupBox1.Checked = True) then
    begin
      sGroupBox1.Caption := 'Розлив ' + sComboBox1.Text + ' ' + sComboBox2.Text + ' ' + sComboBox3.Text;
    end;
end;

procedure TRozlivForm.changeBox2Caption;
begin
  if ( sGroupBox2.Checked = True) then
    begin
      sGroupBox2.Caption := 'Розлив ' + sComboBox4.Text + ' ' + sComboBox5.Text + ' ' + sComboBox6.Text;
    end;
end;

procedure TRozlivForm.changeBox3Caption;
begin
  if ( sGroupBox3.Checked = True) then
    begin
      sGroupBox3.Caption := 'Розлив ' + sComboBox7.Text + ' ' + sComboBox8.Text + ' ' + sComboBox9.Text;
    end;
end;

procedure TRozlivForm.changeBox4Caption;
begin
  if ( sGroupBox4.Checked = True) then
    begin
      sGroupBox4.Caption := 'Розлив ' + sComboBox10.Text + ' ' + sComboBox11.Text + ' ' + sComboBox12.Text;
    end;
end;

procedure TRozlivForm.changeBox5Caption;
begin
  if ( sGroupBox5.Checked = True) then
    begin
      sGroupBox5.Caption := 'Розлив ' + sComboBox13.Text + ' ' + sComboBox14.Text + ' ' + sComboBox15.Text;
    end;
end;

procedure TRozlivForm.concKOHExit(Sender: TObject);
begin
  if concKOH.Value = 0 then
    concKOH.Value := 0.100;
end;

procedure TRozlivForm.concTSExit(Sender: TObject);
begin
  if concTS.Value = 0 then
    concTS.Value := 0.0025;
end;

function TRozlivForm.deltaKch(kch: Extended): Extended;
begin
  if (kch <= 1.0) then
    Result := DecimalRoundExt(kch * 0.06, 2, drHalfPos)
  else
    if ( (kch > 1.0) and (kch <= 6.0) ) then
      Result := DecimalRoundExt(kch * 0.03, 2, drHalfPos)
    else
      if ( (kch > 6.0) and (kch <= 30.0) ) then
        Result := DecimalRoundExt(kch * 0.02, 2, drHalfPos);
end;

function TRozlivForm.deltaPch(pch: Extended): Extended;
begin
  if (pch < 3.0) then
    Result := DecimalRoundExt(pch * 0.08, 2, drHalfPos)
  else
    if ( pch >= 3.0 ) then
      Result := DecimalRoundExt(pch * 0.04, 2, drHalfPos)
end;

procedure TRozlivForm.FormActivate(Sender: TObject);
begin
  Ini:=TiniFile.Create(extractfilepath(paramstr(0))+'Settings.ini');
  if  ( (not Ini.ValueExists('KOH', 'k')) and (not Ini.ValueExists('TioSulf', 'k'))) then
    begin
      Ini.WriteFloat('KOH', 'k', 0.100);
      Ini.WriteFloat('TioSulf', 'k', 0.0020);
    end;
  concKOH.Value := Ini.ReadFloat('KOH', 'k', 0.100);
  concTS.Value := Ini.ReadFloat('TioSulf', 'k', 0.0020);
  ini.Free;
end;

procedure TRozlivForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Ini:=TiniFile.Create(extractfilepath(paramstr(0))+'Settings.ini');
  Ini.WriteFloat('KOH', 'k', concKOH.Value);
  Ini.WriteFloat('TioSulf', 'k', concTS.Value);
  Ini.Free;
end;

procedure TRozlivForm.FormCreate(Sender: TObject);
begin
  changeBox1Caption;
  Randomize;
  Ini:=TiniFile.Create(extractfilepath(paramstr(0))+'Settings.ini');
  if  ( (not Ini.ValueExists('KOH', 'k')) and (not Ini.ValueExists('TioSulf', 'k'))) then
    begin
      Ini.WriteFloat('KOH', 'k', 0.100);
      Ini.WriteFloat('TioSulf', 'k', 0.0020);
    end;
  concKOH.Value := Ini.ReadFloat('KOH', 'k', 0.100);
  concTS.Value := Ini.ReadFloat('TioSulf', 'k', 0.0020);
  ini.Free;
  sPageControl1.ActivePageIndex := 0;
end;

// процедура расчета по к.ч. входных параметров
procedure TRozlivForm.kch_oil(decEdt: TsDecimalSpinEdit);
var r, MyOutput, space: string;
    k, z, i: Integer;
    Cen_Del, kch1_new, kch2_new, kch_new, delta, MyDelta: Extended;
    v1str, v2str, delta_str: string;
begin
  if ( sRadioButton1.Checked = True) then
    Cen_Del := 1;
  if ( sRadioButton2.Checked = True) then
    Cen_Del := 5;

  // для рафинированого масла, когда КЧ 0-1
  if ( decEdt.Value <= 1.00) then
    begin
      delta := deltaKch(decEdt.Value);
      if (Cen_Del = 1) then
        begin
          repeat
            m1 := 19.90 + Random(200)/100;
            m2 := 19.90 + Random(200)/100;
            repeat
              // генерируем параллели
              i := RandomRange(0, 6);
              k := Random(2);
              if (k = 0) then
                begin
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch2 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end
              else
                begin
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch1 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end;
            until Abs(kch1 - kch2) <= delta;

            v1 := kch1 * m1 / (56.11 * TKOH);
            v2 := kch2 * m2 / (56.11 * TKOH);
            v1 := DecimalRoundExt(v1, 2, drHalfPos);
            v2 := DecimalRoundExt(v2, 2, drHalfPos);
            kch1_new := DecimalRoundExt(56.11 * TKOH * v1 / m1, 2, drHalfPos);
            kch2_new := DecimalRoundExt(56.11 * TKOH * v2 / m2, 2, drHalfPos);
            kch_new := DecimalRoundExt((kch1_new + kch2_new)/2, 2, drHalfPos);
          until ( Abs(kch_new - decEdt.Value) <= 0.001 ) and ( Abs(kch1_new - kch2_new) <= delta );
        end;
      if (Cen_Del = 5) then
        begin
          repeat
            m1 := 19.90 + Random(200)/100;
            m2 := 19.90 + Random(200)/100;
            repeat
              // генерируем параллели
              i := RandomRange(0, 6);
              k := Random(2);
              if (k = 0) then
                begin
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch2 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end
              else
                begin
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch1 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end;
            until Abs(kch1 - kch2) <= delta;

            v1 := kch1 * m1 / (56.11 * TKOH);
            v2 := kch2 * m2 / (56.11 * TKOH);
            v1 := DecimalRoundExt(v1, 2, drHalfPos);
            v2 := DecimalRoundExt(v2, 2, drHalfPos);

            v1str := FloatToStrF(v1, ffFixed, 5, 2);

            z := StrToInt(v1str[Length(v1str)]);
            if ( z >= 5) then
              v1str[Length(v1str)] := '5'
            else
              v1str[Length(v1str)] := '0';

            v2str := FloatToStrF(v2, ffFixed, 5, 2);
            z := StrToInt(v2str[Length(v2str)]);
            if ( z >= 5) then
              v2str[Length(v2str)] := '5'
            else
              v2str[Length(v2str)] := '0';
            v1 := StrToFloat(v1str);
            v2 := StrToFloat(v2str);

            kch1_new := DecimalRoundExt(56.11 * TKOH * v1 / m1, 2, drHalfPos);
            kch2_new := DecimalRoundExt(56.11 * TKOH * v2 / m2, 2, drHalfPos);
            kch_new := DecimalRoundExt((kch1_new + kch2_new)/2, 2, drHalfPos);
          until ( Abs(kch_new - decEdt.Value) <= 0.001 ) and ( Abs(kch1_new - kch2_new) <= delta );
        end;
      MyOutput := '0';
    end;
  // для нерафинированого масла, когда КЧ 1-4
  if ( (decEdt.Value > 1.00) and (decEdt.Value <= 4.00) ) then
    begin
      delta := deltaKch(decEdt.Value);
      if (Cen_Del = 1) then
        begin
          repeat
            m1 := 9.90 + Random(100)/100;
            m2 := 9.90 + Random(100)/100;
            repeat
              // генерируем параллели
              i := RandomRange(0, 3);
              k := Random(2);
              if (k = 0) then
                begin
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch2 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end
              else
                begin
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch1 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end;
            until Abs(kch1 - kch2) < delta;

            v1 := kch1 * m1 / (56.11 * TKOH);
            v2 := kch2 * m2 / (56.11 * TKOH);
            v1 := DecimalRoundExt(v1, 2, drHalfPos);
            v2 := DecimalRoundExt(v2, 2, drHalfPos);
            kch1_new := DecimalRoundExt(56.11 * TKOH * v1 / m1, 2, drHalfPos);
            kch2_new := DecimalRoundExt(56.11 * TKOH * v2 / m2, 2, drHalfPos);
            kch_new := DecimalRoundExt((kch1_new + kch2_new)/2, 2, drHalfPos);
          until ( Abs(kch_new - decEdt.Value) <= 0.001 ) and ( Abs(kch1_new - kch2_new) <= delta );
        end;
      if (Cen_Del = 5) then
        begin
          repeat
            m1 := 9.90 + Random(100)/100;
            m2 := 9.90 + Random(100)/100;
            repeat
              // генерируем параллели
              i := RandomRange(0, 3);
              k := Random(2);
              if (k = 0) then
                begin
                  kch1 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch2 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end
              else
                begin
                  kch2 := decEdt.Value - DecimalRoundExt(decEdt.Value * 0.51 * i / 100, 2, drHalfPos);
                  kch1 := decEdt.Value + DecimalRoundExt(decEdt.Value * 0.49 * i / 100, 2, drHalfPos);
                end;
            until Abs(kch1 - kch2) < delta;

            v1 := kch1 * m1 / (56.11 * TKOH);
            v2 := kch2 * m2 / (56.11 * TKOH);
            v1 := DecimalRoundExt(v1, 2, drHalfPos);
            v2 := DecimalRoundExt(v2, 2, drHalfPos);

            v1str := FloatToStrF(v1, ffFixed, 5, 2);

            z := StrToInt(v1str[Length(v1str)]);
            if ( z >= 5) then
              v1str[Length(v1str)] := '5'
            else
              v1str[Length(v1str)] := '0';

            v2str := FloatToStrF(v2, ffFixed, 5, 2);
            z := StrToInt(v2str[Length(v2str)]);
            if ( z >= 5) then
              v2str[Length(v2str)] := '5'
            else
              v2str[Length(v2str)] := '0';
            v1 := StrToFloat(v1str);
            v2 := StrToFloat(v2str);

            kch1_new := DecimalRoundExt(56.11 * TKOH * v1 / m1, 2, drHalfPos);
            kch2_new := DecimalRoundExt(56.11 * TKOH * v2 / m2, 2, drHalfPos);
            kch_new := DecimalRoundExt((kch1_new + kch2_new)/2, 2, drHalfPos);
          until ( Abs(kch_new - decEdt.Value) < 0.001 ) and ( Abs(kch1_new - kch2_new) < delta );
        end;
      MyOutput := '1';
    end;

    kch1 := kch1_new;
    kch2 := kch2_new;

    MyDelta := Abs(kch1 - kch2);

    if MyDelta < 0.01 then
      r := '0'
    else
      r := FloatToStrF(MyDelta, ffFixed, 5, 2);


    // вывод для раф. масла
    if MyOutput = '0' then
      begin
        //sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(kch1 - kch2), ffFixed, 5, 2) + ' / К.Ч. = ' + FloatToStrF(decEdt.Value, ffFixed, 5, 2) + ' мг KOH/г.', clGreen));
        sRichEdit1.Lines.Add(AddColorText('    m (масла), г.  v (KOH), мл.   К.Ч.  К.Ч.(сред.)    r     Δ', clOlive));
        sRichEdit1.Lines.Add(AddColorText('1:     ' + FloatToStrF(m1, ffFixed, 5, 2) + '          ' + FloatToStrF(v1, ffFixed, 5, 2) + '        ' + FloatToStrF(kch1, ffFixed, 5, 2) + '     ' + FloatToStrF(decEdt.Value, ffFixed, 5, 2) + '        ' + r + '    ' + FloatToStrF(DecimalRoundExt(delta, 2, drHalfPos), ffFixed, 5, 2), clRed));
        sRichEdit1.Lines.Add(AddColorText('2:     ' + FloatToStrF(m2, ffFixed, 5, 2) + '          ' + FloatToStrF(v2, ffFixed, 5, 2) + '        ' + FloatToStrF(kch2, ffFixed, 5, 2), clBlue));
        sRichEdit1.Lines.Add('');
      end;

    // вывод для нераф. масла
    if MyOutput = '1' then
      begin
        //sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(kch1 - kch2), ffFixed, 5, 2) + ' / К.Ч. = ' + FloatToStrF(decEdt.Value, ffFixed, 5, 2) + ' мг KOH/г.', clGreen));
        sRichEdit1.Lines.Add(AddColorText('    m (масла), г.  v (KOH), мл.   К.Ч.  К.Ч.(сред.)    r     Δ', clOlive));
        if ( m1 < 10.0) then space := ' ' else space := '';
        sRichEdit1.Lines.Add(AddColorText('1:     ' + space +  FloatToStrF(m1, ffFixed, 5, 2) + '          ' + FloatToStrF(v1, ffFixed, 5, 2) + '        ' + FloatToStrF(kch1, ffFixed, 5, 2) + '     ' + FloatToStrF(decEdt.Value, ffFixed, 5, 2) + '       ' + r + '  ' + FloatToStrF(DecimalRoundExt(delta, 2, drHalfPos), ffFixed, 5, 2), clRed));
        if ( m2 < 10.0) then space := ' ' else space := '';
        sRichEdit1.Lines.Add(AddColorText('2:     ' + space +  FloatToStrF(m2, ffFixed, 5, 2) + '          ' + FloatToStrF(v2, ffFixed, 5, 2) + '        ' + FloatToStrF(kch2, ffFixed, 5, 2), clBlue));
        sRichEdit1.Lines.Add('');
      end;
end;

// процедура расчета по п.ч. входных параметров
procedure TRozlivForm.pch_oil(decEdt: TsDecimalSpinEdit);
var k, z: Integer;
  tst1, tst2, deltaSTR : string;
  x0, x1, Cen_Del, pch1_new, pch2_new, pch_new, delta: Extended;
begin
  //
  TTS := concTS.Value;
  x0 := sDecimalSpinEdit13.Value;
  x1 := sDecimalSpinEdit14.Value;
  if ( sRadioButton1.Checked = True) then
    Cen_Del := 1;
  if ( sRadioButton2.Checked = True) then
    Cen_Del := 5;
  if (decEdt.Value < 3.0) then
    begin
      delta := DecimalRoundExt(decEdt.Value * 0.1, 2, drHalfPos);
      deltaSTR := FloatToStrF(delta, ffFixed, 5, 2);
    end
  else
    if (decEdt.Value >= 3.0) then
      begin
        delta := DecimalRoundExt(decEdt.Value * 0.05, 2, drHalfPos);
        deltaSTR := FloatToStrF(delta, ffFixed, 5, 2);
      end;
  repeat
    repeat
      m1 := 2 + Random(3) + Random(9)/10 + Random(10)/100;
      m2 := 2 + Random(3) + Random(9)/10 + Random(10)/100;

      k := Random(4);
        if ( k = 0) then
          begin
            k := Random(2);
            if (k = 0) then
              begin
                pch1 := decEdt.Value;
                pch2 := pch1 - 0.01;
              end
            else
              begin
                pch2 := decEdt.Value;
                pch1 := pch2 - 0.01;
              end;
          end
        else
          if ( k = 1) then
            begin
              k := Random(2);
              if (k = 0) then
                begin
                  pch1 := decEdt.Value + 0.01;
                  pch2 := decEdt.Value - 0.01;
                end
              else
                begin
                  pch1 := decEdt.Value - 0.01;
                  pch2 := decEdt.Value + 0.01;
                end;
            end
          else
            if ( k = 2) then
              begin
                k := Random(2);
                if (k = 0) then
                  begin
                    pch1 := decEdt.Value + 0.01;
                    pch2 := decEdt.Value - 0.02;
                  end
                else
                  begin
                    pch2 := decEdt.Value + 0.01;
                    pch1 := decEdt.Value - 0.02;
                  end;
              end
            else
              if ( k = 3) then
                begin
                  k := Random(2);
                  if (k = 0) then
                    begin
                      pch1 := decEdt.Value + 0.02;
                      pch2 := decEdt.Value - 0.02;
                    end
                  else
                    begin
                      pch1 := decEdt.Value - 0.02;
                      pch2 := decEdt.Value + 0.02;
                    end;
                end;

      v1 := m1 * pch1 / (TTS * 1000);
      v2 := m2 * pch2 / (TTS * 1000);

      v1 := DecimalRoundExt(v1, 2, drHalfPos);
      v2 := DecimalRoundExt(v2, 2, drHalfPos);

      if (Cen_Del = 5) then
        begin
          tst1 := FloatToStrF(v1, ffFixed, 5, 2);

          z := StrToInt(tst1[Length(tst1)]);
          if ( z >= 5) then
            tst1[Length(tst1)] := '5'
          else
            tst1[Length(tst1)] := '0';

          tst2 := FloatToStrF(v2, ffFixed, 5, 2);
          z := StrToInt(tst2[Length(tst2)]);
          if ( z >= 5) then
            tst2[Length(tst2)] := '5'
          else
            tst2[Length(tst2)] := '0';

          m1 := (StrToFloat(tst1) * TTS * 1000) / pch1;
          m2 := (StrToFloat(tst2) * TTS * 1000) / pch2;
          v1 := StrToFloat(tst1);
          v2 := StrToFloat(tst2);
        end
      else
        if (Cen_Del = 1) then
          begin
            m1 := v1 * TTS * 1000 / pch1;
            m2 := v2 * TTS * 1000 / pch2;
          end;

      m1 := DecimalRoundExt(m1, 2, drHalfPos);
      m2 := DecimalRoundExt(m2, 2, drHalfPos);
    until ( m1 >= x0 ) and ( m1 <= x1 ) and ( m2 >= x0 ) and ( m2 <= x1 );
    pch1_new := DecimalRoundExt(v1 * TTS * 1000/m1, 2, drHalfPos);
    pch2_new := DecimalRoundExt(v2 * TTS * 1000/m2, 2, drHalfPos);
    pch_new := DecimalRoundExt((pch1_new + pch2_new)/2, 2, drHalfPos);
  until ( ( Abs(pch_new - decEdt.Value) <= 0.001) and ( Abs(pch1_new - pch2_new) <= delta) );

  pch1 := pch1_new;
  pch2 := pch2_new;

  if deltaSTR = '0,00' then
    deltaSTR := ' 0';

  //sRichEdit2.Lines.Add(AddColorText2('r = ' + FloatToStrF(abs(pch1 - pch2), ffFixed, 5, 2) + ' / П.Ч. = ' + FloatToStrF(decEdt.Value, ffFixed, 5, 2) + ' ммоль 1/2 O/кг.', clGreen));
  sRichEdit2.Lines.Add(AddColorText2('    m (масла), г.  v (Т.Н), мл.   П.Ч.  П.Ч.(сред.)    r       Δ', clOlive));
  sRichEdit2.Lines.Add(AddColorText2('1:     ' + FloatToStrF(m1, ffFixed, 5, 2) + '          ' + FloatToStrF(v1, ffFixed, 5, 2) + '         ' + FloatToStrF(pch1, ffFixed, 5, 2) + '     ' + FloatToStrF(decEdt.Value, ffFixed, 5, 2) + '      ' + FloatToStrF(abs(pch1 - pch2), ffFixed, 5, 2) + '    ' + deltaSTR, clRed));
  sRichEdit2.Lines.Add(AddColorText2('2:     ' + FloatToStrF(m2, ffFixed, 5, 2) + '          ' + FloatToStrF(v2, ffFixed, 5, 2) + '         ' + FloatToStrF(pch2, ffFixed, 5, 2), clBlue));
  sRichEdit2.Lines.Add('');
end;

function TRozlivForm.AddColorText(Text: String; Color: TColor): String;
begin
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style+[fsBold];
  sRichEdit1.SelAttributes.Color := Color;
  sRichEdit1.SelText := Text;
  sRichEdit1.SelAttributes.Color := clBlack;
end;

function TRozlivForm.AddColorText2(Text: String; Color: TColor): String;
begin
  sRichEdit2.SelAttributes.Style := sRichEdit2.SelAttributes.Style+[fsBold];
  sRichEdit2.SelAttributes.Color := Color;
  sRichEdit2.SelText := Text;
  sRichEdit2.SelAttributes.Color := clBlack;
end;

procedure TRozlivForm.sBitBtn1Click(Sender: TObject);
begin
  TKOH := concKOH.Value;
  TTS := concTS.Value;
  sRichEdit1.Lines.Clear;
  sRichEdit2.Lines.Clear;

  sRichEdit1.Lines.Add('===================================================================');
  //sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.SelAttributes.Size := 10;
  sRichEdit1.Lines.Add('Вводная информация:');
  //sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.SelAttributes.Size := 10;
  sRichEdit1.Lines.Add('r - расхождение между параллелями');
  //sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.SelAttributes.Size := 10;
  sRichEdit1.Lines.Add('Δ - максимально допустимое расхождение');
  //sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.SelAttributes.Size := 10;
  sRichEdit1.Lines.Add('C (KOH) = ' + FloatToStrF(TKOH, ffFixed, 3, 3) + ' моль/л');
  sRichEdit1.Lines.Add('===================================================================');

  sRichEdit2.Lines.Add('===================================================================');
  //sRichEdit2.SelAttributes.Style := sRichEdit2.SelAttributes.Style + [fsBold];
  sRichEdit2.SelAttributes.Color := clRed;
  sRichEdit2.SelAttributes.Size := 10;
  sRichEdit2.Lines.Add('Вводная информация:');
  //sRichEdit2.SelAttributes.Style := sRichEdit2.SelAttributes.Style + [fsBold];
  sRichEdit2.SelAttributes.Color := clRed;
  sRichEdit2.SelAttributes.Size := 10;
  sRichEdit2.Lines.Add('r - расхождение между параллелями');
  //sRichEdit2.SelAttributes.Style := sRichEdit2.SelAttributes.Style + [fsBold];
  sRichEdit2.SelAttributes.Color := clRed;
  sRichEdit2.SelAttributes.Size := 10;
  sRichEdit2.Lines.Add('Δ - максимально допустимое расхождение');
  //sRichEdit2.SelAttributes.Style := sRichEdit2.SelAttributes.Style + [fsBold];
  sRichEdit2.SelAttributes.Color := clRed;
  sRichEdit2.SelAttributes.Size := 10;
  sRichEdit2.Lines.Add('C (Тиосульфат натрия) = ' + FloatToStr(TTS) + ' моль/л');
  sRichEdit2.Lines.Add('===================================================================');

  if (sGroupBox1.Checked = True) then
    begin
      // Расчет КЧ
      if ( sDecimalSpinEdit1.Value <> 0 ) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add(sGroupBox1.Caption);
          kch_oil(sDecimalSpinEdit1);
        end;

      // Расчет ПЧ
      if ( sDecimalSpinEdit2.Value <> 0 ) then
        begin
          UnderAndBold2;
          sRichEdit2.Lines.Add(sGroupBox1.Caption);
          pch_oil(sDecimalSpinEdit2);
        end;
    end;

  if (sGroupBox2.Checked = True) then
    begin
      // Расчет КЧ
      if ( sDecimalSpinEdit3.Value <> 0 ) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add(sGroupBox2.Caption);
          kch_oil(sDecimalSpinEdit3);
        end;

      // Расчет ПЧ
      if ( sDecimalSpinEdit4.Value <> 0 ) then
        begin
          UnderAndBold2;
          sRichEdit2.Lines.Add(sGroupBox2.Caption);
          pch_oil(sDecimalSpinEdit4);
        end;
    end;

  if (sGroupBox3.Checked = True) then
    begin
      // Расчет КЧ
      if ( sDecimalSpinEdit5.Value <> 0 ) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add(sGroupBox3.Caption);
          kch_oil(sDecimalSpinEdit5);
        end;

      // Расчет ПЧ
      if ( sDecimalSpinEdit6.Value <> 0 ) then
        begin
          UnderAndBold2;
          sRichEdit2.Lines.Add(sGroupBox3.Caption);
          pch_oil(sDecimalSpinEdit6);
        end;
    end;

  if (sGroupBox4.Checked = True) then
    begin
      // Расчет КЧ
      if ( sDecimalSpinEdit7.Value <> 0 ) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add(sGroupBox4.Caption);
          kch_oil(sDecimalSpinEdit7);
        end;

      // Расчет ПЧ
      if ( sDecimalSpinEdit8.Value <> 0 ) then
        begin
          UnderAndBold2;
          sRichEdit2.Lines.Add(sGroupBox4.Caption);
          pch_oil(sDecimalSpinEdit8);
        end;
    end;

  if (sGroupBox5.Checked = True) then
    begin
      // Расчет КЧ
      if ( sDecimalSpinEdit9.Value <> 0 ) then
        begin
          UnderAndBold;
          sRichEdit1.Lines.Add(sGroupBox5.Caption);
          kch_oil(sDecimalSpinEdit9);
        end;

      // Расчет ПЧ
      if ( sDecimalSpinEdit10.Value <> 0 ) then
        begin
          UnderAndBold2;
          sRichEdit2.Lines.Add(sGroupBox5.Caption);
          pch_oil(sDecimalSpinEdit10);
        end;
    end;

  sRichEdit1.Lines.Delete(sRichEdit1.Lines.Count - 1);
  sRichEdit2.Lines.Delete(sRichEdit2.Lines.Count - 1);

  sRichEdit1.SelLength := 0;
  sRichEdit1.SelStart:=0;
  sRichEdit1.SelLength := 0;
  sRichEdit2.SelStart:=0;
  sRichEdit2.SelLength := 0;
end;

procedure TRozlivForm.UnderAndBold;
begin
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style+[fsUnderline];
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style+[fsBold];
end;

procedure TRozlivForm.UnderAndBold2;
begin
  sRichEdit2.SelAttributes.Style := sRichEdit2.SelAttributes.Style+[fsUnderline];
  sRichEdit2.SelAttributes.Style := sRichEdit2.SelAttributes.Style+[fsBold];
end;

procedure TRozlivForm.sCheckBox1Click(Sender: TObject);
begin
  changeBox1Caption;
end;

procedure TRozlivForm.sCheckBox2Click(Sender: TObject);
begin
  changeBox2Caption;
end;

procedure TRozlivForm.sCheckBox3Click(Sender: TObject);
begin
  changeBox3Caption;
end;

procedure TRozlivForm.sCheckBox4Click(Sender: TObject);
begin
  changeBox4Caption;
end;

procedure TRozlivForm.sCheckBox5Click(Sender: TObject);
begin
  changeBox5Caption;
end;

procedure TRozlivForm.sComboBox10Change(Sender: TObject);
begin
  changeBox4Caption;
end;

procedure TRozlivForm.sComboBox13Change(Sender: TObject);
begin
  changeBox5Caption;
end;

procedure TRozlivForm.sComboBox1Change(Sender: TObject);
begin
  changeBox1Caption;
end;

procedure TRozlivForm.sComboBox4Change(Sender: TObject);
begin
  changeBox2Caption;
end;

procedure TRozlivForm.sComboBox7Change(Sender: TObject);
begin
  changeBox3Caption;
end;

procedure TRozlivForm.sGroupBox1CheckBoxChanged(Sender: TObject);
begin
  if sGroupBox1.Checked = true then
    changeBox1Caption
  else
    sGroupBox1.Caption := 'Розлив:';
end;

procedure TRozlivForm.sGroupBox2CheckBoxChanged(Sender: TObject);
begin
  if sGroupBox2.Checked = true then
    changeBox2Caption
  else
    sGroupBox2.Caption := 'Розлив:';
end;

procedure TRozlivForm.sGroupBox3CheckBoxChanged(Sender: TObject);
begin
  if sGroupBox3.Checked = true then
    changeBox3Caption
  else
    sGroupBox3.Caption := 'Розлив:';
end;

procedure TRozlivForm.sGroupBox4CheckBoxChanged(Sender: TObject);
begin
  if sGroupBox4.Checked = true then
    changeBox4Caption
  else
    sGroupBox4.Caption := 'Розлив:';
end;

procedure TRozlivForm.sGroupBox5CheckBoxChanged(Sender: TObject);
begin
  if sGroupBox5.Checked = true then
    changeBox5Caption
  else
    sGroupBox5.Caption := 'Розлив:';
end;

end.
