﻿unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Math, StdCtrls, Buttons, sBitBtn, sEdit, sSpinEdit, sRadioButton,
  sGroupBox, ComCtrls, sRichEdit, sLabel, Printers;

type
  TOilForm = class(TForm)
    sGroupBox1: TsGroupBox;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    sDecimalSpinEdit1: TsDecimalSpinEdit;
    sGroupBox3: TsGroupBox;
    sRichEdit1: TsRichEdit;
    sDecimalSpinEdit2: TsDecimalSpinEdit;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sGroupBox2: TsGroupBox;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sRadioButton3: TsRadioButton;
    sRadioButton4: TsRadioButton;
    sDecimalSpinEdit3: TsDecimalSpinEdit;
    sDecimalSpinEdit4: TsDecimalSpinEdit;
    sGroupBox4: TsGroupBox;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    sRadioButton5: TsRadioButton;
    sRadioButton6: TsRadioButton;
    sDecimalSpinEdit5: TsDecimalSpinEdit;
    sDecimalSpinEdit6: TsDecimalSpinEdit;
    sGroupBox5: TsGroupBox;
    sLabel7: TsLabel;
    sLabel8: TsLabel;
    sRadioButton7: TsRadioButton;
    sRadioButton8: TsRadioButton;
    sDecimalSpinEdit7: TsDecimalSpinEdit;
    sDecimalSpinEdit8: TsDecimalSpinEdit;
    sBitBtn2: TsBitBtn;
    sBitBtn1: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure MyOutput(check: integer; top_text: string; OilEdit, VlagaEdit: TsDecimalSpinEdit);
    procedure PrintRichEd(ARichEd: TRichEdit; APortrait: boolean);
    { Public declarations }
  end;

var
  OilForm: TOilForm;

implementation

{$R *.dfm}

uses DecimalRounding_JH1;

procedure TOilForm.MyOutput(check: integer; top_text: string; OilEdit,
  VlagaEdit: TsDecimalSpinEdit);
var
  m1, m2: Extended;                 // массы навесок 1,2
  mc1, mc2: Extended;               // массы колб 1,2
  mcOil1, mcOil2: Extended;         // массы колб с жиром 1,2
  Oil_nat, Oil_acb: Extended;       // жир на нат.влагу/а.с.в.
  NewOil_nat, NewOil_acb: Extended; // новые значения жира на нат.влагу/а.с.в.
  Oil_1_nat, Oil_2_nat: Extended;   // жир на нат.влагу 1,2
  Oil_1_acb, Oil_2_acb: Extended;   // жир на а.с.в. 1,2
  Vlaga: Extended;                  // влажность/Влага
  Delta: Extended;                  // разбег параллелей
  Limited: Extended;                // предел
  i, j: Integer;
  znak: string;
begin
  Vlaga := VlagaEdit.Value;
  // Считываем "масличку" и конвертируем
  // Тут перевод масличности нат.влага --->>> а.с.в.
  if (check = 0) then
    begin
      Oil_nat := OilEdit.Value;
      Oil_acb := Oil_nat * 100 / ( 100 - Vlaga );
      Oil_acb := DecimalRoundExt(Oil_acb, 2, drHalfPos);
    end;
  // Тут перевод масличности а.с.в. --->>> нат.влага
  if (check = 1) then
    begin
      Oil_acb := OilEdit.Value;
      Oil_nat := Oil_acb * ( 100 - Vlaga ) / 100;
      Oil_nat := DecimalRoundExt(Oil_nat, 2, drHalfPos);
    end;

  //Считаем предел
  Limited := Oil_acb * 0.05 + 0.34;

  // Большой цикл для гибкости
  repeat
    // Сгенерируем параллели масличностей
    repeat
      i := RandomRange(0, 100);
      j := Random(2);
      if ( j = 0 ) then
        begin
          Oil_1_acb := Oil_acb - 0.51 * Limited * i / 100;
          Oil_2_acb := Oil_acb + 0.49 * Limited * i / 100;
        end
      else
        begin
          Oil_2_acb := Oil_acb - 0.51 * Limited * i / 100;
          Oil_1_acb := Oil_acb + 0.49 * Limited * i / 100;
        end;
      // Округлим до сотых
      Oil_1_acb := DecimalRoundExt(Oil_1_acb, 2, drHalfPos);
      Oil_2_acb := DecimalRoundExt(Oil_2_acb, 2, drHalfPos);
      Delta := Abs(Oil_1_acb - Oil_2_acb);
    until Delta <= Limited;

    // найдем параллели на нат.влагу
    Oil_1_nat := Oil_1_acb * ( 100 - Vlaga ) / 100;
    Oil_2_nat := Oil_2_acb * ( 100 - Vlaga ) / 100;
    Oil_1_nat := DecimalRoundExt(Oil_1_nat, 2, drHalfPos);
    Oil_2_nat := DecimalRoundExt(Oil_2_nat, 2, drHalfPos);

    // генерируем мыссы навесок
    m1 := RandomRange(80000, 100000) / 10000;
    m2 := RandomRange(80000, 100000) / 10000;

    // генерируем массы колб
    mc1 := RandomRange(1200000, 1500000) / 10000;
    mc2 := RandomRange(1200000, 1500000) / 10000;

    // найдем массу колб с жиром 1,2
    mcOil1 := ( Oil_1_nat * m1 + mc1 * 100 ) / 100;
    mcOil2 := ( Oil_2_nat * m2 + mc2 * 100 ) / 100;
    mcOil1 := DecimalRoundExt(mcOil1, 4, drHalfPos);
    mcOil2 := DecimalRoundExt(mcOil2, 4, drHalfPos);

    // Теперь по новым данным надо сделать прямой расчет
    // и проверить совпадение данных
    // 1-е найдем массу жира и масличность на нат.влагу
    Oil_1_nat := 100 * ( mcOil1 - mc1 ) / m1;
    Oil_2_nat := 100 * ( mcOil2 - mc2 ) / m2;
    // округлим до сотых
    Oil_1_nat := DecimalRoundExt(Oil_1_nat, 2, drHalfPos);
    Oil_2_nat := DecimalRoundExt(Oil_2_nat, 2, drHalfPos);
    Oil_1_acb := Oil_1_nat * 100 / ( 100 - Vlaga );
    Oil_2_acb := Oil_2_nat * 100 / ( 100 - Vlaga );
    Oil_1_acb := DecimalRoundExt(Oil_1_acb, 2, drHalfPos);
    Oil_2_acb := DecimalRoundExt(Oil_2_acb, 2, drHalfPos);
    // найдем новую масличку на нат.влагу
    NewOil_nat := ( Oil_1_nat + Oil_2_nat ) / 2;
    NewOil_nat := DecimalRoundExt(NewOil_nat, 2, drHalfPos);
    // найдем новую масличку на а.с.в.
    NewOil_acb := NewOil_nat * 100 / ( 100 - Vlaga );
    NewOil_acb := DecimalRoundExt(NewOil_acb, 2, drHalfPos);
    Delta := Abs(Oil_1_acb - Oil_2_acb);
  until ( ( Abs(NewOil_acb - Oil_acb) < 0.001 ) and  ( Delta <= Limited ) );

  if ( Delta = Limited ) then znak := '='
  else znak := '<';

  // Сам вывод в поле вывода
  sRichEdit1.SelAttributes.Color := clBlack;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsUnderline];
  sRichEdit1.Lines.Add(top_text);
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.Lines.Add('    m, г.   m(колбы), г.   m(колбы+жир), г.   Xнат., %    W, %    Xа.с.в.,%  Разница  Xср.(а.с.в.), %    Вывод');
  // 1-я параллель
  sRichEdit1.Lines.Add('1: '
  + FloatToStrF(m1, ffFixed, 6, 4)
  + '    '
  + FloatToStrF(mc1, ffFixed, 8, 4)
  + '         '
  + FloatToStrF(mcOil1, ffFixed, 8, 4)
  + '          '
  + FloatToStrF(Oil_1_nat, ffFixed, 5, 2)
  + '     '
  + FloatToStrF(Vlaga, ffFixed, 6, 2)
  + '      '
  + FloatToStrF(Oil_1_acb, ffFixed, 6, 2)
  + '       '
  + FloatToStrF(Delta, ffFixed, 6, 2)
  + '       '
  + FloatToStrF(Oil_acb, ffFixed, 6, 2)
  + '        '
  + FloatToStrF(Delta, ffFixed, 6, 2)
  + ' ' + znak + ' '
  + FloatToStrF(Limited, ffFixed, 6, 2)
  );
  // 2-я параллель
  sRichEdit1.Lines.Add('2: '
  + FloatToStrF(m2, ffFixed, 6, 4)
  + '    '
  + FloatToStrF(mc2, ffFixed, 8, 4)
  + '         '
  + FloatToStrF(mcOil2, ffFixed, 8, 4)
  + '          '
  + FloatToStrF(Oil_2_nat, ffFixed, 5, 2)
  + '                '
  + FloatToStrF(Oil_2_acb, ffFixed, 6, 2));

  // Среднее
  sRichEdit1.SelAttributes.Color := clBlue;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.Lines.Add('                              Проверка средних: '
  + FloatToStrF(Oil_nat, ffFixed, 5, 2) +
  '                '
  + FloatToStrF(Oil_acb, ffFixed, 5, 2));
end;

procedure TOilForm.PrintRichEd(ARichEd: TRichEdit; APortrait: boolean);
var
   dlg            : TPrintDialog;
   Margins        : TRect;
   iPixPerInchX   : integer;
   iPixPerInchY   : integer;
   prn            : TPrinter;
const
   CmPerInch      = 2.54;
begin
   dlg:=TPrintDialog.Create(nil);
   try
      prn:=Printer();

      if APortrait then
         prn.Orientation:=poPortrait
      else
         prn.Orientation:=poLandscape;

      if dlg.Execute() then
      begin
         iPixPerInchX:=GetDeviceCaps(prn.Handle, LOGPIXELSX);
         iPixPerInchY:=GetDeviceCaps(prn.Handle, LOGPIXELSY);

         //поля страницы в Pixel (Inches->Pixel)
         Margins.Left:=trunc(4.0/CmPerInch*iPixPerInchX);
         Margins.Top:=trunc(2.0/CmPerInch*iPixPerInchX);
         Margins.Right:=prn.PageWidth-trunc(0.5/CmPerInch*iPixPerInchX);
         Margins.Bottom:=prn.PageHeight-trunc(1.0/CmPerInch*iPixPerInchX);

         ARichEd.PageRect:=Margins;

         ARichEd.Print('PrintTask');
      end;
   finally
      dlg.Free();
   end;
end;

procedure TOilForm.sBitBtn1Click(Sender: TObject);
var check: integer;
begin
  sRichEdit1.Clear;
  if (sRadioButton1.Checked = true) then
    check := 0
  else
    check := 1;
  if ( sGroupBox1.Checked = True ) then
    MyOutput(check, '                                       '
            + 'Лузга подсолнечная гранулированная РВЦ-1000'
            + '                                      ', sDecimalSpinEdit1, sDecimalSpinEdit2);
  if (sRadioButton3.Checked = true) then
    check := 0
  else
    check := 1;
  if ( sGroupBox2.Checked = True ) then
    MyOutput(check, '                                       '
            + 'Лузга подсолнечная гранулированная РВЦ-3000'
            + '                                      ', sDecimalSpinEdit3, sDecimalSpinEdit4);
  if (sRadioButton5.Checked = true) then
    check := 0
  else
    check := 1;
  if ( sGroupBox4.Checked = True ) then
    MyOutput(check, '                                      '
            + 'Лузга подсолнечная негранулированная РВЦ-1000'
            + '                                     ', sDecimalSpinEdit5, sDecimalSpinEdit6);
  if (sRadioButton7.Checked = true) then
    check := 0
  else
    check := 1;
  if ( sGroupBox5.Checked = True ) then
    MyOutput(check, '                                      '
            + 'Лузга подсолнечная негранулированная РВЦ-3000'
            + '                                     ', sDecimalSpinEdit7, sDecimalSpinEdit8);
end;

procedure TOilForm.sBitBtn2Click(Sender: TObject);
begin
  PrintRichEd(sRichEdit1, False);
end;

end.
