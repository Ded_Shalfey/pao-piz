﻿unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, sEdit, Buttons, sBitBtn;

type
  TPromtForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    sEdit1: TsEdit;
    procedure sEdit1Exit(Sender: TObject);
    procedure sEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PromtForm: TPromtForm;
  temp : string;

const pass = '100500';

implementation

uses Unit1, Unit5;

{$R *.dfm}

procedure TPromtForm.BitBtn1Click(Sender: TObject);
begin
  if (temp <> pass) then
    begin
      Application.Minimize;
      PAOPIZForm.Hide;
      Application.Terminate;
    end;
end;

procedure TPromtForm.BitBtn2Click(Sender: TObject);
begin
  Application.Minimize;
  PAOPIZForm.Hide;
  Application.Terminate;
end;

procedure TPromtForm.sEdit1Exit(Sender: TObject);
begin
  temp := sEdit1.Text;
end;


procedure TPromtForm.sEdit1KeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    begin
      temp := sEdit1.Text;
      BitBtn1.Click;
      Key := #0;
    end;
end;



end.
