﻿unit Unit5;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, sRichEdit, Buttons, sBitBtn, sCheckBox,
  sComboBox, sEdit, sSpinEdit, sLabel, sPageControl, IniFiles, Math, Printers;

type
  TPhForm = class(TForm)
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    sLabel12: TsLabel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    sDecimalSpinEdit1: TsDecimalSpinEdit;
    sComboBox1: TsComboBox;
    sDecimalSpinEdit2: TsDecimalSpinEdit;
    sComboBox2: TsComboBox;
    sCheckBox1: TsCheckBox;
    sCheckBox2: TsCheckBox;
    sCheckBox3: TsCheckBox;
    sCheckBox4: TsCheckBox;
    sCheckBox5: TsCheckBox;
    sCheckBox6: TsCheckBox;
    sCheckBox7: TsCheckBox;
    sCheckBox8: TsCheckBox;
    sCheckBox9: TsCheckBox;
    sBitBtn1: TsBitBtn;
    sDecimalSpinEdit3: TsDecimalSpinEdit;
    sComboBox3: TsComboBox;
    sDecimalSpinEdit4: TsDecimalSpinEdit;
    sComboBox4: TsComboBox;
    sDecimalSpinEdit5: TsDecimalSpinEdit;
    sComboBox5: TsComboBox;
    sDecimalSpinEdit6: TsDecimalSpinEdit;
    sComboBox6: TsComboBox;
    sDecimalSpinEdit7: TsDecimalSpinEdit;
    sComboBox7: TsComboBox;
    sDecimalSpinEdit8: TsDecimalSpinEdit;
    sComboBox8: TsComboBox;
    sDecimalSpinEdit9: TsDecimalSpinEdit;
    sComboBox9: TsComboBox;
    sDecimalSpinEdit10: TsDecimalSpinEdit;
    sComboBox10: TsComboBox;
    sTabSheet2: TsTabSheet;
    sRichEdit1: TsRichEdit;
    sTabSheet3: TsTabSheet;
    sLabel13: TsLabel;
    sLabel14: TsLabel;
    NotRafOptK: TsDecimalSpinEdit;
    RafOptK: TsDecimalSpinEdit;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sCheckBox1Click(Sender: TObject);
    procedure sComboBox1Change(Sender: TObject);
    procedure sComboBox2Change(Sender: TObject);
    procedure sComboBox3Change(Sender: TObject);
    procedure sComboBox4Change(Sender: TObject);
    procedure sComboBox5Change(Sender: TObject);
    procedure sComboBox6Change(Sender: TObject);
    procedure sComboBox7Change(Sender: TObject);
    procedure sComboBox8Change(Sender: TObject);
    procedure sComboBox9Change(Sender: TObject);
    procedure sComboBox10Change(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Phospor(InputSOL: TsDecimalSpinEdit; RafNotRaf: TsComboBox);
    procedure chengeRafNotRaf(decEdt: TsDecimalSpinEdit; chengeOil: TsComboBox);
    procedure myOutput(chengeOil: TsComboBox);
    procedure PrintRichEd(ARichEd: TRichEdit; APortrait: boolean);
  end;

var
  PhForm: TPhForm;
  Ini: TIniFile;
  m1, m2, o1, o2, a1, a2, pp1, pp2, pps, ppr, pp, pp3, pp4, pss, kNotRafOptK, kRafOptK: Extended;
  { Краткие сведения по переменным
     m1 - масса 1-й навески
     m2 - масса 2-й навески
     o1 - оптическая плотность 1
     o2 - оптическая плотность 2
     a1 - концентрация фосфора в 1 растворе
     a2 - концентрация фосфора во 2 растворе
     pp1 - содержание фосфора в мг/кг 1 раствора
     pp2 - содержание фосфора в мг/кг 2 раствора
     ppr - разница между параллелями
     pps - среднее значение фосфора в мг/кг
     pp - предел повторяемости
     pp3 - границы для параллелей предварительный
     pp4 - границы для параллелей окончательный
     pss - среднее содержание фосфора в СОЛ
  }

implementation

{$R *.dfm}

uses DecimalRounding_JH1;

procedure TPhForm.chengeRafNotRaf(decEdt: TsDecimalSpinEdit;
  chengeOil: TsComboBox);
begin
  if ( chengeOil.ItemIndex = 0) then
    begin
      decEdt.DecimalPlaces := 2;
      decEdt.Increment := 0.01;
      decEdt.MaxValue := 3.00;
      decEdt.MinValue := 0.11;
      decEdt.Value := 0.60;
    end
  else
    begin
      decEdt.DecimalPlaces := 3;
      decEdt.Increment := 0.001;
      decEdt.MaxValue := 0.05;
      decEdt.MinValue := 0.010;
      decEdt.Value := 0.010;
    end;
end;

procedure TPhForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Ini:=TiniFile.Create(extractfilepath(paramstr(0))+'Settings.ini');
  Ini.WriteFloat('NotRafK', 'k', NotRafOptK.Value);
  Ini.WriteFloat('RafK', 'k', RafOptK.Value);
  Ini.Free;
  sPageControl1.ActivePage := sTabSheet1;
end;

procedure TPhForm.FormCreate(Sender: TObject);
begin
  Ini:=TiniFile.Create(extractfilepath(paramstr(0))+'Settings.ini');
  if  ( (not Ini.ValueExists('NotRafK', 'k')) and (not Ini.ValueExists('RafK', 'k'))) then
    begin
      Ini.WriteFloat('NotRafK', 'k', 0.271);
      Ini.WriteFloat('RafK', 'k', 1.175);
    end;
  NotRafOptK.Value := Ini.ReadFloat('NotRafK', 'k', 0.271);
  RafOptK.Value := Ini.ReadFloat('RafK', 'k', 1.175);
  ini.Free;
end;

procedure TPhForm.sComboBox10Change(Sender: TObject);
begin
  chengeRafNotRaf(sDecimalSpinEdit10, sComboBox10);
end;

procedure TPhForm.sComboBox1Change(Sender: TObject);
begin
  chengeRafNotRaf(sDecimalSpinEdit1, sComboBox1);
end;

procedure TPhForm.sComboBox2Change(Sender: TObject);
begin
  chengeRafNotRaf(sDecimalSpinEdit2, sComboBox2);
end;

procedure TPhForm.sComboBox3Change(Sender: TObject);
begin
  chengeRafNotRaf(sDecimalSpinEdit3, sComboBox3);
end;

procedure TPhForm.sComboBox4Change(Sender: TObject);
begin
  chengeRafNotRaf(sDecimalSpinEdit4, sComboBox4);
end;

procedure TPhForm.sComboBox5Change(Sender: TObject);
begin
  chengeRafNotRaf(sDecimalSpinEdit5, sComboBox5);
end;

procedure TPhForm.sComboBox6Change(Sender: TObject);
begin
  chengeRafNotRaf(sDecimalSpinEdit6, sComboBox6);
end;

procedure TPhForm.sComboBox7Change(Sender: TObject);
begin
  chengeRafNotRaf(sDecimalSpinEdit7, sComboBox7);
end;

procedure TPhForm.sComboBox8Change(Sender: TObject);
begin
  chengeRafNotRaf(sDecimalSpinEdit8, sComboBox8);
end;

procedure TPhForm.sComboBox9Change(Sender: TObject);
begin
  chengeRafNotRaf(sDecimalSpinEdit9, sComboBox9);
end;

procedure TPhForm.sPageControl1Change(Sender: TObject);
var
 pass: string;
begin
  if (sPageControl1.ActivePage = sTabSheet3) then
    begin
      pass := InputBox('Проверка пароля', 'Пожалуйста, введите пароль', '');
      if ( (pass <> 'черный') and (pass <> 'xthysq') ) then
        sPageControl1.ActivePage := sTabSheet1;
    end;
end;

// Изменение текста чекбокса в зависимости checked/not checked
procedure TPhForm.sCheckBox1Click(Sender: TObject);
begin
  if ( TsCheckBox(Sender).Checked = True ) then
    TsCheckBox(Sender).Caption := 'Включено'
  else
    TsCheckBox(Sender).Caption := 'Выключено';
end;

procedure TPhForm.sBitBtn2Click(Sender: TObject);
begin
  Ini:=TiniFile.Create(extractfilepath(paramstr(0))+'Settings.ini');
  Ini.WriteFloat('NotRafK', 'k', NotRafOptK.Value);
  Ini.WriteFloat('RafK', 'k', RafOptK.Value);
  ini.Free;
end;

procedure TPhForm.sBitBtn3Click(Sender: TObject);
begin
  PrintRichEd(sRichEdit1, False);
end;

// Тут будет происходить весь обсчет данных!
procedure TPhForm.sBitBtn1Click(Sender: TObject);
begin
  // Очищаем поле вывода
  sRichEdit1.Lines.Clear;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clGreen;
  sRichEdit1.Lines.Add('   Масса      Опт. плотн.     Конц. ф-ра в р-ре    Ф-р в обр. мг/кг (X)   Ср. знач. Xср.   Предел      Вывод.     Xсол, %');
  // Расчет для 1:
  Phospor(sDecimalSpinEdit1, sComboBox1);
  // Вывод для 1:
  myOutput(sComboBox1);

  // Расчет для 2:
  if sCheckBox1.Checked = true then
    begin
      Phospor(sDecimalSpinEdit2, sComboBox2);
      myOutput(sComboBox2);
    end;

  // Расчет для 3:
  if sCheckBox2.Checked = true then
    begin
      Phospor(sDecimalSpinEdit3, sComboBox3);
      myOutput(sComboBox3);
    end;

  // Расчет для 4:
  if sCheckBox3.Checked = true then
    begin
      Phospor(sDecimalSpinEdit4, sComboBox4);
      myOutput(sComboBox4);
    end;

  // Расчет для 5:
  if sCheckBox4.Checked = true then
    begin
      Phospor(sDecimalSpinEdit5, sComboBox5);
      myOutput(sComboBox5);
    end;

  // Расчет для 6:
  if sCheckBox5.Checked = true then
    begin
      Phospor(sDecimalSpinEdit6, sComboBox6);
      myOutput(sComboBox6);
    end;

  // Расчет для 7:
  if sCheckBox6.Checked = true then
    begin
      Phospor(sDecimalSpinEdit7, sComboBox7);
      myOutput(sComboBox7);
    end;

  // Расчет для 8:
  if sCheckBox7.Checked = true then
    begin
      Phospor(sDecimalSpinEdit8, sComboBox8);
      myOutput(sComboBox8);
    end;

  // Расчет для 9:
  if sCheckBox8.Checked = true then
    begin
      Phospor(sDecimalSpinEdit9, sComboBox9);
      myOutput(sComboBox9);
    end;

  // Расчет для 10:
  if sCheckBox9.Checked = true then
    begin
      Phospor(sDecimalSpinEdit10, sComboBox10);
      myOutput(sComboBox10);
    end;

  // Форма на все окно
  PhForm.WindowState:=wsMaximized;

  // переход вконце расчета на нужную вкладку с выводом
  sPageControl1.ActivePage := sTabSheet2;
  sRichEdit1.SelStart:=0;
  sRichEdit1.SelLength := 0;
end;

// Вывод на поле вывода
procedure TPhForm.myOutput(chengeOil: TsComboBox);
var
  space_: string;
begin
  // нераф
  sRichEdit1.Lines.Add('-------------------------------------------------------------------------------------------------------------------------');
  if chengeOil.ItemIndex = 0 then
    begin
      if ppr < 10.0 then
        space_ := ' '
      else
        space_ := '';

      // 1-я параллель
      sRichEdit1.Lines.Add('1: ' + FloatToStrF(m1, ffFixed, 5, 4) + '        '
        + FloatToStrF(o1, ffFixed, 5, 3) + '              '
        + FloatToStrF(a1, ffFixed, 4, 3) + '                 '
        + FloatToStrF(pp1, ffFixed, 5, 1) + '                 '
        + FloatToStrF(pps, ffFixed, 4, 0) + '         '
        + FloatToStrF(pp, ffFixed, 3, 1) + '    ' + space_
        + FloatToStrF(ppr, ffFixed, 4, 1) + ' < ' + FloatToStrF(pp, ffFixed, 4, 1)
        + '    '
        + FloatToStrF(pss, ffFixed, 4, 2));

      // 2-я параллель
      sRichEdit1.Lines.Add('2: ' + FloatToStrF(m2, ffFixed, 5, 4) + '        '
        + FloatToStrF(o2, ffFixed, 5, 3) + '              '
        + FloatToStrF(a2, ffFixed, 4, 3) + '                 '
        + FloatToStrF(pp2, ffFixed, 5, 1));
    end;

  // раф
  if chengeOil.ItemIndex = 1 then
    begin
      // 1-я параллель
      sRichEdit1.Lines.Add('1: ' + FloatToStrF(m1, ffFixed, 5, 4) + '        '
        + FloatToStrF(o1, ffFixed, 5, 3) + '              '
        + FloatToStrF(a1, ffFixed, 4, 3) + '                 '
        + FloatToStrF(pp1, ffFixed, 6, 2) + '                  '
        + FloatToStrF(pps, ffFixed, 4, 1) + '         '
        + FloatToStrF(pp, ffFixed, 3, 1) + '     '
        + FloatToStrF(ppr, ffFixed, 4, 2) + ' < ' + FloatToStrF(pp, ffFixed, 4, 1)
        + '     '
        + FloatToStrF(pss, ffFixed, 4, 3));

      // 2-параллель
      sRichEdit1.Lines.Add('2: ' + FloatToStrF(m2, ffFixed, 5, 4) + '        '
        + FloatToStrF(o2, ffFixed, 5, 3) + '              '
        + FloatToStrF(a2, ffFixed, 4, 3) + '                 '
        + FloatToStrF(pp2, ffFixed, 6, 2));
    end;
  sRichEdit1.Lines.Add('-------------------------------------------------------------------------------------------------------------------------');
end;

procedure TPhForm.Phospor(InputSOL: TsDecimalSpinEdit; RafNotRaf: TsComboBox);
var
  i, k: Integer;
  ppc, pp11, pp22: Extended;
begin
  // заносим коэффициенты из вкладки настройки в соотв. переменные
  kNotRafOptK := NotRafOptk.Value;
  kRafOptK := RafOptK.Value;

  // расчет для нерафа
  if RafNotRaf.ItemIndex = 0 then
    begin
      pss := InputSOL.Value;                                // фосфор СОЛ из поля
      pps := DecimalRoundExt(pss/0.002544, 0, drHalfPos);   // фосфор в ппм до целых
      pp := DecimalRoundExt(pps*0.08, 1, drHalfPos);        // предел повторяемости до десятых
      repeat
        // входим в цикл
        repeat
          repeat
            // генерируем параллели
            i := RandomRange(0, 80);
            k := Random(2);
            if (k = 0) then
              begin
                pp1 := pps - pps * 0.51 * i / 1000;
                pp2 := pps + pps * 0.49 * i / 1000;
              end
            else
              begin
                pp2 := pps - pps * 0.51 * i / 1000;
                pp1 := pps + pps * 0.49 * i / 1000;
              end;
              // округляем параллели до десятых
            pp1 := DecimalRoundExt(pp1, 1, drHalfPos);
            pp2 := DecimalRoundExt(pp2, 1, drHalfPos);
          until Abs(pp1 - pp2) < pp;

          // считаем ср. значение (pp1 + pp2)/2 с округлением до целого
          // и сравниваем с pps на равенство!
          ppc := ( pp1 + pp2)/2;
          ppc := DecimalRoundExt(ppc, 0, drHalfPos);
        until ( ppc = pps );

        // входим в цикл
        repeat
          // генерируем массы навесок до 4-го знака после запятой
          if pss < 0.25 then
            m1 := RandomRange(4000, 9000)/10000
          else
            m1 := RandomRange(2000, 4000)/10000;

          // генерируем оптические плотности до 3-го знака
          for i := 100 to 800 do
            begin
              o1 := i/1000;
              pp11 := o1 * 100/kNotRafOptK/m1;
              pp11 := DecimalRoundExt(pp11, 1, drHalfPos);
              if ( pp11 = pp1) then
                break;
            end;
        until ( pp1 = pp11 );

        // входим в цикл
        repeat
          // генерируем массы навесок до 4-го знака после запятой
          if pss < 0.25 then
            m2 := RandomRange(4000, 9000)/10000
          else
            m2 := RandomRange(2000, 4000)/10000;

          // генерируем оптические плотности до 3-го знака
          for i := 100 to 800 do
            begin
              o2 := i/1000;
              pp22 := o2 * 100/kNotRafOptK/m2;
              pp22 := DecimalRoundExt(pp22, 1, drHalfPos);
              if ( pp22 = pp2) then
                break;
            end;
        until ( pp2 = pp22 );

        // находим массовую концентрацию
        a1 := o1 / kNotRafOptK;
        a2 := o2 / kNotRafOptK;

        // разница между параллелями
        ppr := Abs(pp1 - pp2);

        // округляем оставшееся
        a1 := DecimalRoundExt(a1, 3, drHalfPos);
        a2 := DecimalRoundExt(a2, 3, drHalfPos);
        ppr := DecimalRoundExt(ppr, 1, drHalfPos);

        pp11 := a1 * 100 / m1;
        pp22 := a2 * 100 / m2;

      until (abs(pp1 - pp11) < 0.01) and (abs(pp2 - pp22) < 0.01);
    end;

  // расчет для рафа
  if RafNotRaf.ItemIndex = 1 then
    begin
      repeat
        pss := InputSOL.Value;                                // фосфор СОЛ из поля
        pps := DecimalRoundExt(pss/0.002544, 1, drHalfPos);   // фосфор в ппм до десятых
        pp := 1.2;                                            // предел повторяемости до десятых
        // входим в цикл
        repeat
          // генерируем параллели
          repeat
            i := RandomRange(0, 100);
            k := Random(2);
            if (k = 0) then
              begin
                pp1 := pps - 0.51 * 1.2 * i / 100;
                pp2 := pps + 0.49 * 1.2 * i / 100;
              end
            else
              begin
                pp2 := pps - 0.51 * 1.2 * i / 100;
                pp1 := pps + 0.49 * 1.2 * i / 100;
              end;
            // округляем параллели до десятых
            pp1 := DecimalRoundExt(pp1, 2, drHalfPos);
            pp2 := DecimalRoundExt(pp2, 2, drHalfPos);
          until Abs (pp1 - pp2) < 1.2;

          // считаем ср. значение (pp1 + pp2)/2 с округлением до десятых
          // и сравниваем с pps на равенство!
          ppc := ( pp1 + pp2)/2;
          ppc := DecimalRoundExt(ppc, 1, drHalfPos);
        until ( ppc = pps );

        // параллели сгенерированы!
        // идем дальше по цепочке ...
        // входим в цикл
        repeat
          // генерируем массы навесок до 4-го знака после запятой
          if pss > 0.05 then
          m1 := RandomRange(1000, 1500)/1000 + Random(10)/10000;
          if pss <= 0.05 then
          m1 := RandomRange(1500, 4000)/1000 + Random(10)/10000;

          // генерируем оптические плотности до 3-го знака
          //o1 := RandomRange(100, 800)/1000;
          for i := 100 to 800 do
            begin
              o1 := i/1000;
              pp11 := o1 * 100/kRafOptK/m1;
              pp11 := DecimalRoundExt(pp11, 2, drHalfPos);
              if ( pp11 = pp1) then
                break;
            end;
        until ( pp1 = pp11 );

        // входим в цикл
        repeat
          // генерируем массы навесок до 4-го знака после запятой
          if pss > 0.05 then
          m2 := RandomRange(1000, 1500)/1000 + Random(10)/10000;
          if pss <= 0.05 then
          m2 := RandomRange(1500, 4000)/1000 + Random(10)/10000;

          // генерируем оптические плотности до 3-го знака
          //o2 := RandomRange(100, 800)/1000;
          for i := 100 to 800 do
            begin
              o2 := i/1000;
              pp22 := o2 * 100/kRafOptK/m2;
              pp22 := DecimalRoundExt(pp22, 2, drHalfPos);
              if ( pp22 = pp2) then
                break;
            end;
        until ( pp2 = pp22 );

        // находим массовую концентрацию
        a1 := o1 / kRafOptK;
        a2 := o2 / kRafOptK;

        // разница между параллелями
        ppr := Abs(pp1 - pp2);

        // округляем оставшееся
        a1 := DecimalRoundExt(a1, 3, drHalfPos);
        a2 := DecimalRoundExt(a2, 3, drHalfPos);
        ppr := DecimalRoundExt(ppr, 2, drHalfPos);

        pp11 := a1 * 100 / m1;
        pp22 := a2 * 100 / m2;

      until (abs(pp1 - pp11) < 0.001) and (abs(pp2 - pp22) < 0.001);
    end;
end;

procedure TPhForm.PrintRichEd(ARichEd: TRichEdit; APortrait: boolean);
var
   dlg            : TPrintDialog;
   Margins        : TRect;
   iPixPerInchX   : integer;
   iPixPerInchY   : integer;
   prn            : TPrinter;
const
   CmPerInch      = 2.54;
begin
   dlg:=TPrintDialog.Create(nil);
   try
      prn:=Printer();

      if APortrait then
         prn.Orientation:=poPortrait
      else
         prn.Orientation:=poLandscape;

      if dlg.Execute() then
      begin
         iPixPerInchX:=GetDeviceCaps(prn.Handle, LOGPIXELSX);
         iPixPerInchY:=GetDeviceCaps(prn.Handle, LOGPIXELSY);

         //поля страницы в Pixel (Inches->Pixel)
         Margins.Left:=trunc(2.0/CmPerInch*iPixPerInchX);
         Margins.Top:=trunc(2.5/CmPerInch*iPixPerInchX);
         Margins.Right:=prn.PageWidth-trunc(0.5/CmPerInch*iPixPerInchX);
         Margins.Bottom:=prn.PageHeight-trunc(1.0/CmPerInch*iPixPerInchX);

         ARichEd.PageRect:=Margins;

         ARichEd.Print('PrintTask');
      end;
   finally
      dlg.Free();
   end;
end;

end.
