﻿unit Unit8;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Math, StdCtrls, ComCtrls, sRichEdit, Buttons, sBitBtn, sEdit,
  sSpinEdit, sRadioButton, sGroupBox;

type
  TZolaForm = class(TForm)
    sGroupBox1: TsGroupBox;
    sDecimalSpinEdit1: TsDecimalSpinEdit;
    sGroupBox2: TsGroupBox;
    sDecimalSpinEdit3: TsDecimalSpinEdit;
    sBitBtn1: TsBitBtn;
    sGroupBox3: TsGroupBox;
    sRichEdit1: TsRichEdit;
    sGroupBox4: TsGroupBox;
    sDecimalSpinEdit2: TsDecimalSpinEdit;
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ZolaForm: TZolaForm;

implementation

{$R *.dfm}

uses DecimalRounding_JH1;

procedure TZolaForm.sBitBtn1Click(Sender: TObject);
var
  m1, m2: Extended;                   // массы тиглей 1,2 для общей золы
  m_1, m_2: Extended;                 // массы тиглей 3,4 для золы нераств. в HCl
  m1Obr, m2Obr: Extended;             // массы тиглей 1,2 с образцом
  m_1Obr, m_2Obr: Extended;           // массы тиглей 3,4 с образцом
  m1Zol, m2Zol: Extended;             // массы тиглей 1,2 с золой
  m_1Zol, m_2Zol: Extended;           // массы тиглей 3,4 с золой
  Zola1_nat, Zola2_nat: Extended;     // зола общая на нат.влагу (обратный расчет)
  Zola1_acb, Zola2_acb: Extended;     // зола общая на а.с.в. (обратный расчет)
  Zola_nat, Zola_acb: Extended;       // зола общая на нат./а.с.в. (обратный расчет)
  Zola_1_nat, Zola_2_nat: Extended;   // зола HCl на нат.влагу (обратный расчет)
  Zola_1_acb, Zola_2_acb: Extended;   // зола HCl на а.с.в. (обратный расчет)
  Zola_nat2, Zola_acb2: Extended;     // зола HCl на нат./а.с.в. (обратный расчет)
  _Zola1_nat, _Zola2_nat: Extended;   // зола общая на нат.влагу (прямой расчет)
  _Zola1_acb, _Zola2_acb: Extended;   // зола общая на а.с.в. (прямой расчет)
  _Zola_nat, _Zola_acb: Extended;     // зола общая на нат./а.с.в. (прямой расчет)
  _Zola_1_nat, _Zola_2_nat: Extended; // зола HCl на нат.влагу (прямой расчет)
  _Zola_1_acb, _Zola_2_acb: Extended; // зола HCl на а.с.в. (прямой расчет)
  _Zola_nat2, _Zola_acb2: Extended;   // зола HCl на нат./а.с.в. (прямой расчет)
  Vlaga: Extended;                    // влажность/Влага
  Delta, Delta2: Extended;  // разбег параллелей
  Limited, Limited2: Extended;        // предел
  i, j: Integer;
  znak1, znak2, probel: string;
begin
  Limited := 0.05; Limited2 := 0.03;
  Vlaga := sDecimalSpinEdit3.Value;
  if Vlaga >= 10.00 then probel := ''
  else probel := ' ';
  // Данные по обратному расчету
  Zola_acb := sDecimalSpinEdit1.Value;
  Zola_acb2 := sDecimalSpinEdit2.Value;

  // Общая зола
  repeat
    // разобьем параллели
    repeat
      i := Random(6);
      j := Random(2);
      if ( j = 0 ) then
        begin
          Zola1_acb := Zola_acb - DecimalRoundExt(0.51 * i / 10, 2, drHalfPos);
          Zola2_acb := Zola_acb + DecimalRoundExt(0.49 * i / 10, 2, drHalfPos);
        end
      else
        begin
          Zola2_acb := Zola_acb - DecimalRoundExt(0.51 * i / 10, 2, drHalfPos);
          Zola1_acb := Zola_acb + DecimalRoundExt(0.49 * i / 10, 2, drHalfPos);
        end;
      Delta := Abs(Zola1_acb - Zola2_acb);
    until Delta <= Limited;

    // найдем золу на нат. влагу
    Zola1_nat := Zola1_acb * ( 100 - Vlaga ) / 100;
    Zola2_nat := Zola2_acb * ( 100 - Vlaga ) / 100;

    // генерируем массу тигля 1,2
    m1 := RandomRange(410000, 480000) / 10000;
    m2 := RandomRange(410000, 480000) / 10000;

    // генерируем массу тигля 1,2 с навеской
    m1Obr := m1 + RandomRange(49900, 50100) / 10000;
    m2Obr := m2 + RandomRange(49900, 50100) / 10000;

    // найдем массу тигля 1,2 с золой
    m1Zol := ( Zola1_nat * ( m1Obr - m1 ) + 100 * m1 ) / 100;
    m2Zol := ( Zola2_nat * ( m2Obr - m2 ) + 100 * m2 ) / 100;

    // Теперь нужно сделать прямой расчет и сравнить

    // Найдем золу на нат. влагу
    _Zola1_nat := 100 * (  m1Zol - m1 ) / ( m1Obr - m1 );
    _Zola2_nat := 100 * (  m2Zol - m2 ) / ( m2Obr - m2 );

    // округлим до сотых
    _Zola1_nat := DecimalRoundExt(_Zola1_nat, 2, drHalfPos);
    _Zola2_nat := DecimalRoundExt(_Zola2_nat, 2, drHalfPos);

    // найдем золу на а.с.в. и округлим до сотых
    _Zola1_acb := _Zola1_nat * 100 / ( 100 - Vlaga );
    _Zola2_acb := _Zola2_nat * 100 / ( 100 - Vlaga );
    _Zola1_acb := DecimalRoundExt(_Zola1_acb, 2, drHalfPos);
    _Zola2_acb := DecimalRoundExt(_Zola2_acb, 2, drHalfPos);

    // Найдем среднюю золу и округлим до сотых
    _Zola_acb := ( _Zola1_acb + _Zola2_acb ) / 2;
    _Zola_acb := DecimalRoundExt(_Zola_acb, 2, drHalfPos);

    // Найдем разницу
    Delta := Abs( _Zola1_acb - _Zola2_acb );
    Delta := DecimalRoundExt(Delta, 2, drHalfPos);
  until ( ( Delta <= Limited ) and ( Abs(Zola_acb - _Zola_acb) < 0.001 ) );

  if ( Delta = Limited ) then znak1 := '='
  else znak1 := '<';

  // Зола HCl
  repeat
    // разобьем параллели
    repeat
      i := Random(4);
      j := Random(2);

      if ( j = 0 ) then
        begin
          Zola_1_acb := Zola_acb2 - DecimalRoundExt(0.51 * i / 10, 2, drHalfPos);
          Zola_2_acb := Zola_acb2 + DecimalRoundExt(0.49 * i / 10, 2, drHalfPos);
        end
      else
        begin
          Zola_2_acb := Zola_acb2 - DecimalRoundExt(0.51 * i / 10, 2, drHalfPos);
          Zola_1_acb := Zola_acb2 + DecimalRoundExt(0.49 * i / 10, 2, drHalfPos);
        end;
      Delta2 := Abs(Zola_1_acb - Zola_2_acb);
    until Delta2 <= Limited2;

    // найдем золу на нат. влагу
    Zola_1_nat := Zola_1_acb * ( 100 - Vlaga ) / 100;
    Zola_2_nat := Zola_2_acb * ( 100 - Vlaga ) / 100;

    // генерируем массу тигля 1,2
    m_1 := RandomRange(410000, 480000) / 10000;
    m_2 := RandomRange(410000, 480000) / 10000;

    // генерируем массу тигля 1,2 с навеской
    m_1Obr := m_1 + RandomRange(49900, 50100) / 10000;
    m_2Obr := m_2 + RandomRange(49900, 50100) / 10000;

    // найдем массу тигля 1,2 с золой
    m_1Zol := ( Zola_1_nat * ( m_1Obr - m_1 ) + 100 * m_1 ) / 100;
    m_2Zol := ( Zola_2_nat * ( m_2Obr - m_2 ) + 100 * m_2 ) / 100;

    // Теперь нужно сделать прямой расчет и сравнить

    // Найдем золу на нат. влагу
    _Zola_1_nat := 100 * (  m_1Zol - m_1 ) / ( m_1Obr - m_1 );
    _Zola_2_nat := 100 * (  m_2Zol - m_2 ) / ( m_2Obr - m_2 );

    // округлим до сотых
    _Zola_1_nat := DecimalRoundExt(_Zola_1_nat, 2, drHalfPos);
    _Zola_2_nat := DecimalRoundExt(_Zola_2_nat, 2, drHalfPos);

    // найдем золу на а.с.в. и округлим до сотых
    _Zola_1_acb := _Zola_1_nat * 100 / ( 100 - Vlaga );
    _Zola_2_acb := _Zola_2_nat * 100 / ( 100 - Vlaga );
    _Zola_1_acb := DecimalRoundExt(_Zola_1_acb, 2, drHalfPos);
    _Zola_2_acb := DecimalRoundExt(_Zola_2_acb, 2, drHalfPos);

    // Найдем среднюю золу и округлим до сотых
    _Zola_acb2 := ( _Zola_1_acb + _Zola_2_acb ) / 2;
    _Zola_acb2 := DecimalRoundExt(_Zola_acb2, 2, drHalfPos);

    // Найдем разницу
    Delta2 := Abs( _Zola_1_acb - _Zola_2_acb );
  until ( ( Delta2 <= Limited2 ) and ( Abs(Zola_acb2 - _Zola_acb2) < 0.001 ) );

  if ( Delta2 = Limited2 ) then znak2 := '='
  else znak2 := '<';

  // Вывод для общей золы
  sRichEdit1.Clear;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clGreen;
  sRichEdit1.Lines.Add('                                                        Общая зола');
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.Lines.Add(' m(тигля), г.   m(т.+навеска), г.   m(т.+зола), г.   Xнат., %    W, %    Xа.с.в.,%  Разница  Xср.(а.с.в.), %    Вывод');
  sRichEdit1.Lines.Add('');
  // 1-я параллель
  sRichEdit1.Lines.Add('1: '
  + FloatToStrF(m1, ffFixed, 6, 4)
  + '           '
  + FloatToStrF(m1Obr, ffFixed, 8, 4)
  + '           '
  + FloatToStrF(m1Zol, ffFixed, 8, 4)
  + '         '
  + FloatToStrF(_Zola1_nat, ffFixed, 5, 2)
  + '     '  + probel
  + FloatToStrF(Vlaga, ffFixed, 6, 2)
  + '      '
  + FloatToStrF(_Zola1_acb, ffFixed, 6, 2)
  + '       '
  + FloatToStrF(Delta, ffFixed, 6, 2)
  + '       '
  + FloatToStrF(Zola_acb, ffFixed, 6, 2)
  + '        '
  + FloatToStrF(Delta, ffFixed, 6, 2)
  + ' ' + znak1 + ' '
  + FloatToStrF(Limited, ffFixed, 6, 2)
  );
  // 2-я параллель
  sRichEdit1.Lines.Add('2: '
  + FloatToStrF(m2, ffFixed, 6, 4)
  + '           '
  + FloatToStrF(m2Obr, ffFixed, 8, 4)
  + '           '
  + FloatToStrF(m2Zol, ffFixed, 8, 4)
  + '         '
  + FloatToStrF(_Zola2_nat, ffFixed, 5, 2)
  + '                '
  + FloatToStrF(_Zola2_acb, ffFixed, 6, 2));

  // Вывод для золы HCl
  sRichEdit1.Lines.Add('');
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clGreen;
  sRichEdit1.Lines.Add('                                                Зола нерастворимая в HCl');
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
  sRichEdit1.SelAttributes.Color := clRed;
  sRichEdit1.Lines.Add(' m(тигля), г.   m(т.+навеска), г.   m(т.+зола), г.   Xнат., %    W, %    Xа.с.в.,%  Разница  Xср.(а.с.в.), %    Вывод');
  sRichEdit1.Lines.Add('');
  // 1-я параллель
  sRichEdit1.Lines.Add('1: '
  + FloatToStrF(m_1, ffFixed, 6, 4)
  + '           '
  + FloatToStrF(m_1Obr, ffFixed, 8, 4)
  + '           '
  + FloatToStrF(m_1Zol, ffFixed, 8, 4)
  + '         '
  + FloatToStrF(_Zola_1_nat, ffFixed, 5, 2)
  + '     ' + probel
  + FloatToStrF(Vlaga, ffFixed, 6, 2)
  + '      '
  + FloatToStrF(_Zola_1_acb, ffFixed, 6, 2)
  + '       '
  + FloatToStrF(Delta2, ffFixed, 6, 2)
  + '       '
  + FloatToStrF(Zola_acb2, ffFixed, 6, 2)
  + '        '
  + FloatToStrF(Delta2, ffFixed, 6, 2)
  + ' ' + znak2 + ' '
  + FloatToStrF(Limited2, ffFixed, 6, 2)
  );
  // 2-я параллель
  sRichEdit1.Lines.Add('2: '
  + FloatToStrF(m_2, ffFixed, 6, 4)
  + '           '
  + FloatToStrF(m_2Obr, ffFixed, 8, 4)
  + '           '
  + FloatToStrF(m_2Zol, ffFixed, 8, 4)
  + '         '
  + FloatToStrF(_Zola_2_nat, ffFixed, 5, 2)
  + '                '
  + FloatToStrF(_Zola_2_acb, ffFixed, 6, 2));

end;

end.
