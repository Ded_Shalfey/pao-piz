﻿unit Unit9;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, Math, ExtCtrls, sSkinManager, sButton,
  ImgList, acAlphaHints, sRichEdit, sBitBtn, acAlphaImageList, Printers;

type
  TW1W2Form = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Edit10: TEdit;
    Edit11: TEdit;
    Edit12: TEdit;
    Vlaga: TLabel;
    Sor: TLabel;
    Label7: TLabel;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Edit13: TEdit;
    Edit14: TEdit;
    Edit15: TEdit;
    Edit16: TEdit;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Edit17: TEdit;
    Edit18: TEdit;
    Edit19: TEdit;
    Edit20: TEdit;
    GroupBox4: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Edit21: TEdit;
    Edit22: TEdit;
    Edit23: TEdit;
    Edit24: TEdit;
    BitBtn1: TBitBtn;
    GroupBox5: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Edit25: TEdit;
    Edit26: TEdit;
    Edit27: TEdit;
    Edit28: TEdit;
    Edit29: TEdit;
    Edit30: TEdit;
    Edit31: TEdit;
    Edit32: TEdit;
    Edit33: TEdit;
    Edit34: TEdit;
    Edit35: TEdit;
    Edit36: TEdit;
    GroupBox6: TGroupBox;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Edit37: TEdit;
    Edit38: TEdit;
    Edit39: TEdit;
    Edit40: TEdit;
    GroupBox7: TGroupBox;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Edit41: TEdit;
    Edit42: TEdit;
    Edit43: TEdit;
    Edit44: TEdit;
    GroupBox8: TGroupBox;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Edit45: TEdit;
    Edit46: TEdit;
    Edit47: TEdit;
    Edit48: TEdit;
    GroupBox9: TGroupBox;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Edit49: TEdit;
    Edit50: TEdit;
    Edit51: TEdit;
    Edit52: TEdit;
    BitBtn2: TBitBtn;
    GroupBox10: TGroupBox;
    Label53: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Edit53: TEdit;
    Edit54: TEdit;
    GroupBox11: TGroupBox;
    Label57: TLabel;
    Edit55: TEdit;
    GroupBox12: TGroupBox;
    Label52: TLabel;
    Edit56: TEdit;
    GroupBox13: TGroupBox;
    Label54: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Edit57: TEdit;
    Edit58: TEdit;
    GroupBox14: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    GroupBox15: TGroupBox;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    GroupBox16: TGroupBox;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    GroupBox17: TGroupBox;
    RadioButton9: TRadioButton;
    RadioButton10: TRadioButton;
    sAlphaHints1: TsAlphaHints;
    GroupBox18: TGroupBox;
    RadioButton11: TRadioButton;
    RadioButton12: TRadioButton;
    GroupBox19: TGroupBox;
    RadioButton13: TRadioButton;
    RadioButton14: TRadioButton;
    sSkinManager1: TsSkinManager;
    sRichEdit1: TsRichEdit;
    sBitBtn1: TsBitBtn;
    sAlphaImageList1: TsAlphaImageList;
    ImageList1: TImageList;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Edit59: TEdit;
    Edit60: TEdit;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Edit61: TEdit;
    Edit62: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit7Exit(Sender: TObject);
    procedure Edit7KeyPress(Sender: TObject; var Key: Char);
    procedure Edit13Exit(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure inputer(editer: TEdit);
    procedure inputersem(editer: TEdit);
    procedure inputsor(editer2: TEdit);
    function AddColorText(Text: String; Color: TColor): String;
    procedure UnderAndBold;
    procedure printSemkaVlaga;
    procedure printMezVlaga;
    procedure printSemkaSor;
    procedure PrintRichEd(ARichEd: TRichEdit; APortrait: boolean);
    { Public declarations }
  end;

var
  W1W2Form: TW1W2Form;
  m1, m2, m3, m_1, m_2, m_3, w1, w2, w_sred, w_true, s1, s2, s_sred, s_true: Extended;
  t_8_20, t_10_22, t_11_23, t_12_00, t_14_02, t_16_04, t_17_05, t_18_06 : string;
  znak, buks : Integer;
  TypeZavod, Time: String;

implementation

uses DecimalRounding_JH1;

{$R *.dfm}

function TW1W2Form.AddColorText(Text: String; Color: TColor): String;
begin
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style+[fsBold];
  sRichEdit1.SelAttributes.Color := Color;
  sRichEdit1.SelText := Text;
  sRichEdit1.SelAttributes.Color := clBlack;
end;

procedure TW1W2Form.BitBtn1Click(Sender: TObject);
begin
  W1W2Form.Height := 560;
  W1W2Form.Width := 702;

  if RadioButton1.Checked = True then
    znak := 2
  else
    if RadioButton2.Checked = True then
      znak := 3
    else
      if RadioButton3.Checked = True then
        znak := 4;

  if RadioButton4.Checked = True then
    begin
      t_8_20 := '08:00';
      t_10_22 := '10:00';
      t_11_23 := '11:00';
      t_12_00 := '12:00';
      t_14_02 := '14:00';
      t_16_04 := '16:00';
      t_17_05 := '17:00';
      t_18_06 := '18:00';
    end
  else
    begin
      t_8_20 := '20:00';
      t_10_22 := '22:00';
      t_11_23 := '23:00';
      t_12_00 := '00:00';
      t_14_02 := '02:00';
      t_16_04 := '04:00';
      t_17_05 := '05:00';
      t_18_06 := '06:00';
    end;

  if RadioButton11.Checked = True then
    buks := 0
  else
    buks := 1;

  sRichEdit1.Lines.Clear;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clRed;
      sRichEdit1.SelAttributes.Size := 12;
  sRichEdit1.Lines.Add('Результаты. Завод МЭП-1000.');
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clGreen;
      sRichEdit1.SelAttributes.Size := 8;
  sRichEdit1.Lines.Add('r = |w1 - w2|; r = |s1 - s2| - Расхождение между параллелями');
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clGreen;
      sRichEdit1.SelAttributes.Size := 8;
  sRichEdit1.Lines.Add('W'' = (w1 + w2)/2; S'' = (s1 + s2)/2 - Усредненная(-ый) влажность/сор до 2-го знака'+ #13#10);
  // СП /08:00/20:00/ W
  inputersem(Edit1);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // СП /08:00/20:00/ Сор
  inputsor(Edit7);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_8_20;
      printSemkaSor;
    end;

  // ШПТН /08:00/20:00/ W
  inputer(Edit13);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТН. МЭП-1000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // ШПТН(б) /08:00/20:00/ W
  inputer(Edit17);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТН(б). МЭП-1000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // ШПТГ /08:00/20:00/ W
  inputer(Edit21);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТГ. МЭП-1000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // ЛПГ /08:00/20:00/ W
  inputer(Edit53);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ЛПГ. МЭП-1000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // СП /10:00/22:00/ W
  inputersem(Edit2);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_10_22;
      printMezVlaga;
    end;

  // СП /10:00/22:00/ Сор
  inputsor(Edit8);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_10_22;
      printSemkaSor;
    end;

  // ШПТН /11:00/23:00/ W
  inputer(Edit14);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТН. МЭП-1000. ';
      Time := t_11_23;
      printMezVlaga;
    end;

  {
  // ШПТН(б) /11:00/23:00/ W
  inputer(Edit18);
  sRichEdit1.Lines.Add('ШПТН(б). МЭП-1000. ' + t_11_23 + '. W = ' + FloatToStrF(w_sred, ffFixed, 5, 1) +  ' % :');
  sRichEdit1.Lines.Add('1: ' + FloatToStrF(m1, ffFixed, znak + 3, znak) + '/'
  + FloatToStrF(m2, ffFixed, znak + 3, znak) + '/' + FloatToStrF(m3, ffFixed, znak + 3, znak) + '/'
  + FloatToStrF(w1, ffFixed, 5, 2));
  sRichEdit1.Lines.Add('2: ' + FloatToStrF(m_1, ffFixed, znak + 3, znak) + '/'
  + FloatToStrF(m_2, ffFixed, znak + 3, znak) + '/' + FloatToStrF(m_3, ffFixed, znak + 3, znak) + '/'
  + FloatToStrF(w2, ffFixed, 5, 2) + #13#10);
  }

  // ШПТГ /11:00/23:00/ W
  inputer(Edit22);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТГ. МЭП-1000. ';
      Time := t_11_23;
      printMezVlaga;
    end;

    // ЛПГ /11:00/23:00/ W
  inputer(Edit54);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ЛПГ. МЭП-1000. ';
      Time := t_11_23;
      printMezVlaga;
    end;

  // СП /12:00/00:00/ W
  inputersem(Edit3);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_12_00;
      printMezVlaga;
    end;

  // СП /12:00/00:00/ Сор
  inputsor(Edit9);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_12_00;
      printSemkaSor;
    end;

  // СП /14:00/02:00/ W
  inputersem(Edit4);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // СП /14:00/02:00/ Сор
  inputsor(Edit10);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_14_02;
      printSemkaSor;
    end;

  // ШПТН /02:00/14:00/ W
  inputer(Edit15);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТН. МЭП-1000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // ШПТН(б) /02:00/14:00/ W
  inputer(Edit19);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТН(б). МЭП-1000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // ШПТГ /02:00/14:00/ W
  inputer(Edit23);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТГ. МЭП-1000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // ЛПГ /14:00/02:00/ W
  inputer(Edit59);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ЛПГ. МЭП-1000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // СП /16:00/04:00/ W
  inputersem(Edit5);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_16_04;
      printMezVlaga;
    end;

  // СП /16:00/04:00/ Сор
  inputsor(Edit11);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_16_04;
      printSemkaSor;
    end;

  // ЛПГ /16:00/04:00/ W
  inputer(Edit60);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ЛПГ. МЭП-1000. ';
      Time := t_16_04;
      printMezVlaga;
    end;

  // ШПТН /05:00/17:00/ W
  inputer(Edit16);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТН. МЭП-1000. ';
      Time := t_17_05;
      printMezVlaga;
    end;

  {
  // ШПТН(б) /05:00/17:00/ W
  inputer(Edit20);
  sRichEdit1.Lines.Add('ШПТН(б). МЭП-1000. ' + t_17_05 + '. W = ' + FloatToStrF(w_sred, ffFixed, 5, 1) +  ' % :');
  sRichEdit1.Lines.Add('1: ' + FloatToStrF(m1, ffFixed, znak + 3, znak) + '/'
  + FloatToStrF(m2, ffFixed, znak + 3, znak) + '/' + FloatToStrF(m3, ffFixed, znak + 3, znak) + '/'
  + FloatToStrF(w1, ffFixed, 5, 2));
  sRichEdit1.Lines.Add('2: ' + FloatToStrF(m_1, ffFixed, znak + 3, znak) + '/'
  + FloatToStrF(m_2, ffFixed, znak + 3, znak) + '/' + FloatToStrF(m_3, ffFixed, znak + 3, znak) + '/'
  + FloatToStrF(w2, ffFixed, 5, 2) + #13#10);
  }

  // ШПТГ /05:00/17:00/ W
  inputer(Edit24);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТГ. МЭП-1000. ';
      Time := t_17_05;
      printMezVlaga;
    end;

  // СП /18:00/06:00/ W
  inputersem(Edit6);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_18_06;
      printMezVlaga;
    end;

  // СП /18:00/06:00/ Сор
  inputsor(Edit12);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-1000. ';
      Time := t_18_06;
      printSemkaSor;
    end;

  // ЛПН / ССП / W
  inputer(Edit55);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ЛПН. МЭП-1000. ';
      Time := 'ССП';
      printMezVlaga;
    end;

  PageControl1.ActivePage := TabSheet3;
  sRichEdit1.SelStart:=0;
  sRichEdit1.SelLength := 0;
  sRichEdit1.Refresh;
end;

procedure TW1W2Form.BitBtn2Click(Sender: TObject);
begin
  W1W2Form.Height := 560;
  W1W2Form.Width := 702;

  if RadioButton6.Checked = True then
    znak := 2
  else
    if RadioButton7.Checked = True then
      znak := 3
    else
      if RadioButton8.Checked = True then
        znak := 4;

  if RadioButton9.Checked = True then
    begin
      t_8_20 := '08:00';
      t_10_22 := '10:00';
      t_11_23 := '11:00';
      t_12_00 := '12:00';
      t_14_02 := '14:00';
      t_16_04 := '16:00';
      t_17_05 := '17:00';
      t_18_06 := '18:00';
    end
  else
    begin
      t_8_20 := '20:00';
      t_10_22 := '22:00';
      t_11_23 := '23:00';
      t_12_00 := '00:00';
      t_14_02 := '02:00';
      t_16_04 := '04:00';
      t_17_05 := '05:00';
      t_18_06 := '06:00';
    end;

  if RadioButton13.Checked = True then
    buks := 0
  else
    buks := 1;

  sRichEdit1.Lines.Clear;
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clRed;
      sRichEdit1.SelAttributes.Size := 12;
  sRichEdit1.Lines.Add('Результаты. Завод МЭП-3000.');
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clGreen;
      sRichEdit1.SelAttributes.Size := 8;
  sRichEdit1.Lines.Add('r = |w1 - w2|; r = |s1 - s2| - Расхождение между параллелями');
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style + [fsBold];
      sRichEdit1.SelAttributes.Color := clGreen;
      sRichEdit1.SelAttributes.Size := 8;
  sRichEdit1.Lines.Add('W'' = (w1 + w2)/2; S'' = (s1 + s2)/2 - Усредненная(-ый) влажность/сор до 2-го знака'+ #13#10);

  // СП /08:00/20:00/ W
  inputersem(Edit25);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // СП /08:00/20:00/ Сор
  inputsor(Edit31);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_8_20;
      printSemkaSor;
    end;

  // ШПТН /08:00/20:00/ W
  inputer(Edit37);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТН. МЭП-3000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // ШПТГ /08:00/20:00/ W
  inputer(Edit41);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТГ. МЭП-3000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // Мезга из кондиционера /08:00/20:00/ W
  inputer(Edit45);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'МезК. МЭП-3000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // Мезга из жаровни /08:00/20:00/ W
  inputer(Edit49);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'МезЖ. МЭП-3000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // ЛПГ /08:00/20:00/ W
  inputer(Edit57);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ЛПГ. МЭП-3000. ';
      Time := t_8_20;
      printMezVlaga;
    end;

  // СП /10:00/22:00/ W
  inputersem(Edit26);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_10_22;
      printMezVlaga;
    end;

  // СП /10:00/22:00/ Сор
  inputsor(Edit32);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_10_22;
      printSemkaSor;
    end;

  // ШПТН /11:00/23:00/ W
  inputer(Edit38);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТН. МЭП-3000. ';
      Time := t_11_23;
      printMezVlaga;
    end;

  // ШПТГ /11:00/23:00/ W
  inputer(Edit42);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТГ. МЭП-3000. ';
      Time := t_11_23;
      printMezVlaga;
    end;

  // Мезга из кондиционера /11:00/23:00/ W
  inputer(Edit46);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'МезК. МЭП-3000. ';
      Time := t_11_23;
      printMezVlaga;
    end;

  // Мезга из жаровни /11:00/23:00/ W
  inputer(Edit50);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'МезЖ. МЭП-3000. ';
      Time := t_11_23;
      printMezVlaga;
    end;

  // ЛПГ /11:00/23:00/ W
  inputer(Edit58);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ЛПГ. МЭП-3000. ';
      Time := t_11_23;
      printMezVlaga;
    end;

  // СП /12:00/00:00/ W
  inputersem(Edit27);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_12_00;
      printMezVlaga;
    end;

  // СП /12:00/00:00/ Сор
  inputsor(Edit33);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_12_00;
      printSemkaSor;
    end;

  // СП /14:00/02:00/ W
  inputersem(Edit28);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // СП /14:00/02:00/ Сор
  inputsor(Edit34);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_14_02;
      printSemkaSor;
    end;

  // ШПТН /02:00/14:00/ W
  inputer(Edit39);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТН. МЭП-3000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // ШПТГ /02:00/14:00/ W
  inputer(Edit43);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТГ. МЭП-3000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // Мезга из кондиционера /14:00/02:00/ W
  inputer(Edit47);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'МезК. МЭП-3000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // Мезга из жаровни /14:00/02:00/ W
  inputer(Edit51);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'МезЖ. МЭП-3000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // ЛПГ /14:00/02:00/ W
  inputer(Edit61);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ЛПГ. МЭП-3000. ';
      Time := t_14_02;
      printMezVlaga;
    end;

  // СП /16:00/04:00/ W
  inputersem(Edit29);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_16_04;
      printMezVlaga;
    end;

  // СП /16:00/04:00/ Сор
  inputsor(Edit35);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_16_04;
      printSemkaSor;
    end;

  // ЛПГ /16:00/04:00/ W
  inputer(Edit62);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ЛПГ. МЭП-3000. ';
      Time := t_16_04;
      printMezVlaga;
    end;

  // ШПТН /05:00/17:00/ W
  inputer(Edit40);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТН. МЭП-3000. ';
      Time := t_17_05;
      printMezVlaga;
    end;

  // ШПТГ /05:00/17:00/ W
  inputer(Edit44);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ШПТГ. МЭП-3000. ';
      Time := t_17_05;
      printMezVlaga;
    end;

  // Мезга из кондиционера /05:00/17:00/ W
  inputer(Edit48);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'МезК. МЭП-3000. ';
      Time := t_17_05;
      printMezVlaga;
    end;

  // Мезга из жаровни /05:00/17:00/ W
  inputer(Edit52);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'МезЖ. МЭП-3000. ';
      Time := t_17_05;
      printMezVlaga;
    end;

  // СП /18:00/06:00/ W
  inputersem(Edit30);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_18_06;
      printMezVlaga;
    end;

  // СП /18:00/06:00/ Сор
  inputsor(Edit36);
  if (s_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'СП. МЭП-3000. ';
      Time := t_18_06;
      printSemkaSor;
    end;

  // ЛПН / ССП / W
  inputer(Edit56);
  if (w_sred <> 0) then
    begin
      UnderAndBold;
      TypeZavod := 'ЛПН. МЭП-3000. ';
      Time := 'ССП';
      printMezVlaga;
    end;
  PageControl1.ActivePage := TabSheet3;
  sRichEdit1.SelStart:=0;
  sRichEdit1.SelLength := 0;
end;

procedure TW1W2Form.Edit13Exit(Sender: TObject);
begin
  if (TEdit(Sender).Text = '') or (TEdit(Sender).Text = '0,0') or (TEdit(Sender).Text = '0,') then
     TEdit(Sender).Text := '0';
  if (TEdit(Sender).Text[Length(TEdit(Sender).Text)] = ',') then
    TEdit(Sender).Text := TEdit(Sender).Text + '0';
  if (TEdit(Sender).Text <> '0') and (TEdit(Sender).Text[Length(TEdit(Sender).Text) - 1] <> ',') then
    TEdit(Sender).Text := TEdit(Sender).Text + ',0';
  if (StrToFloat(TEdit(Sender).Text) > 99.7 ) then
    TEdit(Sender).Text := '0';
end;

procedure TW1W2Form.Edit1Click(Sender: TObject);
begin
  if (TEdit(Sender).Text = '0') or (TEdit(Sender).Text = '0,0') then
     TEdit(Sender).Text := '';
end;

procedure TW1W2Form.Edit1Exit(Sender: TObject);
begin
  if (TEdit(Sender).Text = '') or (TEdit(Sender).Text = '0,0') or (TEdit(Sender).Text = '0,') then
     TEdit(Sender).Text := '0';
  if (TEdit(Sender).Text[Length(TEdit(Sender).Text)] = ',') then
    TEdit(Sender).Text := TEdit(Sender).Text + '0';
  if (TEdit(Sender).Text <> '0') and (TEdit(Sender).Text[Length(TEdit(Sender).Text) - 1] <> ',') then
    TEdit(Sender).Text := TEdit(Sender).Text + ',0';
  if (StrToFloat(TEdit(Sender).Text) > 99.7 ) then
    TEdit(Sender).Text := '0';
end;

procedure TW1W2Form.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_INSERT then key := 0;
    TEdit(Sender).ReadOnly:=(Shift=[ssShift]) or (Shift=[ssCtrl]);
end;

procedure TW1W2Form.Edit1KeyPress(Sender: TObject; var Key: Char);
var  vrPos, vrLength, vrSelStart: byte;
const
  I: byte = 0; //2 символа после decimalseparator-а
  //Множество цифр, допустимых для ввода в edit:
  Digit: set of Char=['1'..'9', '0'];
//Множество символов, воспринимаемых как символ-разделитель:
  Separator: set of Char=['/', '.', ',', 'ю', 'Ю', 'б', 'Б'];
begin
 with Sender as TEdit do
  begin
    vrLength := Length(Text);
    vrPos := Pos(FormatSettings.DecimalSeparator, Text);
    vrSelStart := SelStart;
  end;
  case Key of
    '0'..'9':
      begin
        if (vrPos > 0) and (vrLength - vrPos > I) and (vrSelStart >= vrPos) then
          Key := #0;
      end;
    ',', '.':
      begin
        if (vrPos > 0) or (vrSelStart = 0) or (vrLength = 0) then
          Key := #0
        else
          Key := FormatSettings.decimalseparator;
      end;
    #8: ;
  else
    Key := #0;
  end;
  with (Sender as TEdit) do
  begin
    if (Key in Separator)
     then Key:=FormatSettings.DecimalSeparator //Delphi-константа типа Char, равная символу-разделителю Windows
     else
       if (not(Key in Digit))
       then Key:=#0;
    if ((Key=FormatSettings.DecimalSeparator)and(pos(FormatSettings.DecimalSeparator, Text)<>0))
      then Key:=#0;
   if (Sender as TEdit).Text = '0' then
      if Key = '0' then
      (Sender as TEdit).Text:= '';
  end;
end;

procedure TW1W2Form.Edit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var n: Integer;
    S: String;
begin
with (Sender as TEdit) do
  begin
    if pos(FormatSettings.DecimalSeparator, Text)=1 then
      begin
        Text:='0'+Text;
        SelStart:=Length(Text);
      end;
    if (pos(FormatSettings.DecimalSeparator, Text)<>Length(Text)) then
      if Text[Length(Text)]<>'0' then
        if FloatToStr(StrToFloat(Text))<>'0' then
          Text:=FloatToStr(StrToFloat(Text));
    if Key=8 then
      begin
        n:=SelStart;
        S:=Text;
        Delete(S, n, 1);
        Text:=S;
      end;
    SelStart:=Length(Text);
  end;
end;

procedure TW1W2Form.Edit7Exit(Sender: TObject);
begin
if (TEdit(Sender).Text = '') or (TEdit(Sender).Text = '0,0') or (TEdit(Sender).Text = '0,') then
     TEdit(Sender).Text := '0';
  if (TEdit(Sender).Text[Length(TEdit(Sender).Text)] = ',') then
    TEdit(Sender).Text := TEdit(Sender).Text + '0';
  if (TEdit(Sender).Text <> '0') and (TEdit(Sender).Text[Length(TEdit(Sender).Text) - 1] <> ',') then
    TEdit(Sender).Text := TEdit(Sender).Text + ',0';
  if (StrToFloat(TEdit(Sender).Text) > 99.7 ) then
    TEdit(Sender).Text := '0';
end;

procedure TW1W2Form.Edit7KeyPress(Sender: TObject; var Key: Char);
var  vrPos, vrLength, vrSelStart: byte;
const
  I: byte = 0; //2 символа после decimalseparator-а
  //Множество цифр, допустимых для ввода в edit:
  Digit: set of Char=['1'..'9', '0'];
//Множество символов, воспринимаемых как символ-разделитель:
  Separator: set of Char=['/', '.', ',', 'ю', 'Ю', 'б', 'Б'];
begin
 with Sender as TEdit do
  begin
    vrLength := Length(Text);
    vrPos := Pos(FormatSettings.decimalseparator, Text);
    vrSelStart := SelStart;
  end;
  case Key of
    '0'..'9':
      begin
        if (vrPos > 0) and (vrLength - vrPos > I) and (vrSelStart >= vrPos) then
          Key := #0;
      end;
    ',', '.':
      begin
        if (vrPos > 0) or (vrSelStart = 0) or (vrLength = 0) then
          Key := #0
        else
          Key := FormatSettings.decimalseparator;
      end;
    #8: ;
  else
    Key := #0;
  end;
  with (Sender as TEdit) do
  begin
    if (Key in Separator)
     then Key:=FormatSettings.DecimalSeparator //Delphi-константа типа Char, равная символу-разделителю Windows
     else
       if (not(Key in Digit))
       then Key:=#0;
    if ((Key=FormatSettings.DecimalSeparator)and(pos(FormatSettings.DecimalSeparator, Text)<>0))
      then Key:=#0;
   if (Sender as TEdit).Text = '0' then
      if Key = '0' then
      (Sender as TEdit).Text:= '';
  end;
end;

procedure TW1W2Form.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //PostMessage(Application.Handle, WM_QUIT, 0, 0);
end;

procedure TW1W2Form.FormCreate(Sender: TObject);
begin
  Randomize;
  PageControl1.ActivePage := TabSheet1;
  W1W2Form.Height := 520;
  W1W2Form.Width := 702;
end;

procedure TW1W2Form.inputer(editer: TEdit);
var k, i, m0, m00: integer;
w1_new, w2_new: Double;
begin
  w_sred := StrToFloat(editer.Text);
  if w_sred = 0 then
    begin
      m1 := 0.0000;
      m2 := 0.0000;
      m3 := 0.0000;
      m_1 := 0.0000;
      m_2 := 0.0000;
      m_3 := 0.0000;
      w1 := 0.00;
      w2 := 0.00;
      w_true:=0.00;
      Exit;
    end;
  repeat
    if buks = 0 then
      begin
        m0 := RandomRange(13, 18);
        m00 := RandomRange(13, 18);
      end
    else
      begin
        m0 := 27;
        m00 := 27;
      end;
        if znak = 4 then
          begin
            m1 := m0 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100 + RandomRange(0, 9)/1000 + RandomRange(0, 9)/10000;
            m_1 := m00 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100 + RandomRange(0, 9)/1000 + RandomRange(0, 9)/10000;
            k := random(2);
            if k = 0 then
              begin
                m2 := m1 + 5 + RandomRange(0, 5)/100 - RandomRange(0, 9)/1000 - RandomRange(0, 9)/10000;
                m_2 := m_1 + 5 + RandomRange(0, 5)/100 - RandomRange(0, 9)/1000 - RandomRange(0, 9)/10000;
              end
            else
              begin
                m2 := m1 + 5 - RandomRange(0, 5)/100 + RandomRange(0, 9)/1000 + RandomRange(0, 9)/10000;
                m_2 := m_1 + 5 - RandomRange(0, 5)/100 + RandomRange(0, 9)/1000 + RandomRange(0, 9)/10000;
              end;
          end;
        if znak = 3 then
          begin
            m1 := m0 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100 + RandomRange(0, 9)/1000;
            m_1 := m00 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100 + RandomRange(0, 9)/1000;
            k := random(2);
            if k = 0 then
              begin
                m2 := m1 + 5 + RandomRange(0, 5)/100 - RandomRange(0, 9)/1000;
                m_2 := m_1 + 5 + RandomRange(0, 5)/100 - RandomRange(0, 9)/1000;
              end
            else
              begin
                m2 := m1 + 5 - RandomRange(0, 5)/100 + RandomRange(0, 9)/1000;
                m_2 := m_1 + 5 - RandomRange(0, 5)/100 + RandomRange(0, 9)/1000;
              end;
          end;
        if znak = 2 then
          begin
            m1 := m0 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100;
            m_1 := m00 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100;
            k := random(2);
            if k = 0 then
              begin
                m2 := m1 + 5 + RandomRange(0, 5)/100;
                m_2 := m_1 + 5 + RandomRange(0, 5)/100;
              end
            else
              begin
                m2 := m1 + 5 - RandomRange(0, 5)/100;
                m_2 := m_1 + 5 - RandomRange(0, 5)/100;
              end;
          end;

     k := random(2);
     if k = 0 then
       begin
         w_true := w_sred + random(5)/100;
       end
     else
       begin
         w_true := w_sred - random(6)/100;
       end;

    k := random(2);
    i := Random(21);
    if k = 0 then
      begin
        w1 := w_true - 0.51 * i/100;
        w2 := w_true + 0.49 * i/100;
        w1 := DecimalRoundExt(w1, 2, drHalfPos);
        w2 := DecimalRoundExt(w2, 2, drHalfPos);
      end
    else
      begin
        w2 := w_true - 0.51 * i/100;
        w1 := w_true + 0.49 * i/100;
        w1 := DecimalRoundExt(w1, 2, drHalfPos);
        w2 := DecimalRoundExt(w2, 2, drHalfPos);
      end;


    m3 := m2 - w1*(m2 - m1)/100;
    m_3 := m_2 - w2*(m_2 - m_1)/100;
    m3 := DecimalRoundExt(m3, znak, drHalfPos);
    m_3 := DecimalRoundExt(m_3, znak, drHalfPos);

    m1 := m2 - 100*(m2 - m3)/w1;
    m_1 := m_2 - 100*(m_2 - m_3)/w2;

    m1 := DecimalRoundExt(m1, znak, drHalfPos);
    m_1 := DecimalRoundExt(m_1, znak, drHalfPos);

    w1_new := 100 * (m2 - m3) / (m2 - m1);
    w2_new := 100 * (m_2 - m_3) / (m_2 - m_1);
    w1_new := DecimalRoundExt(w1_new, 2, drHalfPos);
    w2_new := DecimalRoundExt(w2_new, 2, drHalfPos);

  until ( (Abs(m2 - m1 - 5) <= 0.05 ) and ( Abs(m_2 - m_1 - 5) <= 0.05) and ( Abs(w1 - w1_new) < 0.01) and ( Abs(w2 - w2_new) < 0.01));
end;

procedure TW1W2Form.inputersem(editer: TEdit);
var k, i, m0, m00: integer;
w1_new, w2_new: Double;
begin
  w_sred := StrToFloat(editer.Text);
  if w_sred = 0 then
    begin
      m1 := 0.0000;
      m2 := 0.0000;
      m3 := 0.0000;
      m_1 := 0.0000;
      m_2 := 0.0000;
      m_3 := 0.0000;
      w1 := 0.00;
      w2 := 0.00;
      w_true:=0.00;
      Exit;
    end;
  repeat
    if buks = 0 then
      begin
        m0 := RandomRange(13, 18);
        m00 := RandomRange(13, 18);
      end
    else
      begin
        m0 := 27;
        m00 := 27;
      end;
        if znak = 4 then
          begin
            m1 := m0 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100 + RandomRange(0, 9)/1000 + RandomRange(0, 9)/10000;
            m_1 := m00 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100 + RandomRange(0, 9)/1000 + RandomRange(0, 9)/10000;
            k := random(2);
            if k = 0 then
              begin
                m2 := m1 + 5 + RandomRange(0, 5)/100 - RandomRange(0, 9)/1000 - RandomRange(0, 9)/10000;
                m_2 := m_1 + 5 + RandomRange(0, 5)/100 - RandomRange(0, 9)/1000 - RandomRange(0, 9)/10000;
              end
            else
              begin
                m2 := m1 + 5 - RandomRange(0, 5)/100 + RandomRange(0, 9)/1000 + RandomRange(0, 9)/10000;
                m_2 := m_1 + 5 - RandomRange(0, 5)/100 + RandomRange(0, 9)/1000 + RandomRange(0, 9)/10000;
              end;
          end;
        if znak = 3 then
          begin
            m1 := m0 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100 + RandomRange(0, 9)/1000;
            m_1 := m00 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100 + RandomRange(0, 9)/1000;
            k := random(2);
            if k = 0 then
              begin
                m2 := m1 + 5 + RandomRange(0, 5)/100 - RandomRange(0, 9)/1000;
                m_2 := m_1 + 5 + RandomRange(0, 5)/100 - RandomRange(0, 9)/1000;
              end
            else
              begin
                m2 := m1 + 5 - RandomRange(0, 5)/100 + RandomRange(0, 9)/1000;
                m_2 := m_1 + 5 - RandomRange(0, 5)/100 + RandomRange(0, 9)/1000;
              end;
          end;
        if znak = 2 then
          begin
            m1 := m0 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100;
            m_1 := m00 + RandomRange(0, 9)/10 + RandomRange(0, 9)/100;
            k := random(2);
            if k = 0 then
              begin
                m2 := m1 + 5 + RandomRange(0, 5)/100;
                m_2 := m_1 + 5 + RandomRange(0, 5)/100;
              end
            else
              begin
                m2 := m1 + 5 - RandomRange(0, 5)/100;
                m_2 := m_1 + 5 - RandomRange(0, 5)/100;
              end;
          end;

     k := random(2);
     if k = 0 then
       begin
         w_true := w_sred + random(5)/100;
       end
     else
       begin
         w_true := w_sred - random(6)/100;
       end;

    k := random(2);
    i := Random(26);
    if k = 0 then
      begin
        w1 := w_true - 0.51 * i/100;
        w2 := w_true + 0.49 * i/100;
        w1 := DecimalRoundExt(w1, 2, drHalfPos);
        w2 := DecimalRoundExt(w2, 2, drHalfPos);
      end
    else
      begin
        w2 := w_true - 0.51 * i/100;
        w1 := w_true + 0.49 * i/100;
        w1 := DecimalRoundExt(w1, 2, drHalfPos);
        w2 := DecimalRoundExt(w2, 2, drHalfPos);
      end;


    m3 := m2 - w1*(m2 - m1)/100;
    m_3 := m_2 - w2*(m_2 - m_1)/100;
    m3 := DecimalRoundExt(m3, znak, drHalfPos);
    m_3 := DecimalRoundExt(m_3, znak, drHalfPos);

    m1 := m2 - 100*(m2 - m3)/w1;
    m_1 := m_2 - 100*(m_2 - m_3)/w2;

    m1 := DecimalRoundExt(m1, znak, drHalfPos);
    m_1 := DecimalRoundExt(m_1, znak, drHalfPos);

    w1_new := 100 * (m2 - m3) / (m2 - m1);
    w2_new := 100 * (m_2 - m_3) / (m_2 - m_1);
    w1_new := DecimalRoundExt(w1_new, 2, drHalfPos);
    w2_new := DecimalRoundExt(w2_new, 2, drHalfPos);

  until ( (Abs(m2 - m1 - 5) <= 0.05 ) and ( Abs(m_2 - m_1 - 5) <= 0.05) and ( Abs(w1 - w1_new) < 0.01) and ( Abs(w2 - w2_new) < 0.01));
end;

procedure TW1W2Form.inputsor(editer2: TEdit);
var k: integer;
  s1t, s2t, s1t1, s2t2, delta, struetemp : Extended;
begin
  s_sred := StrToFloat(editer2.Text);
  if s_sred = 0 then
    begin
      m1 := 0;
      m2 := 0;
      m_1 := 0;
      m_2 := 0;
      s1 := 0;
      s2 := 0;
      Exit;
    end;
  repeat
    Randomize;
    m1 := 99.50 + Random(10)/10 + Random(10)/100;
    m_1 := 99.50 + Random(10)/10 + Random(10)/100;

    k := random(2);
    if k = 0 then
      begin
        s_true := s_sred + RandomRange(1, 5)/100;
      end
    else
      begin
        s_true := s_sred - RandomRange(1, 6)/100;
      end;

    k := random(2);
    if k = 0 then
      begin
        s1 := s_true + RandomRange(1, 11)/100;
        s2 := s_true - RandomRange(1, 11)/100;
      end
    else
      begin
        s1 := s_true - RandomRange(1, 11)/100;
        s2 := s_true + RandomRange(1, 11)/100;
      end;

    s1t := s1;
    s2t := s2;

    m2 := m1 * s1/100;
    m_2 := m_1 * s2/100;

    m2 := DecimalRoundExt(m2, 2, drHalfPos);
    m_2 := DecimalRoundExt(m_2, 2, drHalfPos);

    s1 := 100*m2/m1;
    s2 := 100*m_2/m_1;

    s1t1 := s1;
    s2t2 := s2;

    s1 := DecimalRoundExt(s1, 2, drHalfPos);
    s2 := DecimalRoundExt(s2, 2, drHalfPos);

    s_true := (s1 + s2)/2;
    s_true := DecimalRoundExt(s_true, 2, drHalfPos);

    delta := Abs(s1 - s2);

    struetemp := DecimalRoundExt(s_true, 1, drHalfPos);

  until (delta < 0.20) and ( Abs(struetemp - s_sred) < 0.005 ) and ( Abs(s1t1 - s1t) < 0.005) and ( Abs(s2t2 - s2t) < 0.005);

end;

procedure TW1W2Form.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePage = TabSheet1 then
    begin
      W1W2Form.Height := 520;
      W1W2Form.Width := 702;
    end;
  if PageControl1.ActivePage = TabSheet2 then
    begin
      W1W2Form.Height := 600;
      W1W2Form.Width := 702;
    end;
  if PageControl1.ActivePage = TabSheet3 then
    begin
      W1W2Form.Height := 560;
      W1W2Form.Width := 702;
    end;
end;

procedure TW1W2Form.printMezVlaga;
begin
  sRichEdit1.Lines.Add(TypeZavod + Time + '. W = ' + FloatToStrF(w_sred, ffFixed, 5, 1) +  ' % :');
      sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(w1 - w2), ffFixed, 5, 2) + ' % / W'' = ' + FloatToStrF(w_true, ffFixed, 5, 2) + ' %', clGreen));
      sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, znak + 3, znak) + '/'
      + FloatToStrF(m2, ffFixed, znak + 3, znak) + '/' + FloatToStrF(m3, ffFixed, znak + 3, znak) + '/'
      + FloatToStrF(w1, ffFixed, 5, 2), clRed));
      sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m_1, ffFixed, znak + 3, znak) + '/'
      + FloatToStrF(m_2, ffFixed, znak + 3, znak) + '/' + FloatToStrF(m_3, ffFixed, znak + 3, znak) + '/'
      + FloatToStrF(w2, ffFixed, 5, 2) + #13#10, clBlue));
end;

procedure TW1W2Form.PrintRichEd(ARichEd: TRichEdit; APortrait: boolean);
var
   dlg            : TPrintDialog;
   Margins        : TRect;
   iPixPerInchX   : integer;
   iPixPerInchY   : integer;
   prn            : TPrinter;
const
   CmPerInch      = 2.54;
begin
   dlg:=TPrintDialog.Create(nil);
   try
      prn:=Printer();

      if APortrait then
         prn.Orientation:=poPortrait
      else
         prn.Orientation:=poLandscape;

      if dlg.Execute() then
      begin
         iPixPerInchX:=GetDeviceCaps(prn.Handle, LOGPIXELSX);
         iPixPerInchY:=GetDeviceCaps(prn.Handle, LOGPIXELSY);

         //поля страницы в Pixel (Inches->Pixel)
         Margins.Left:=trunc(4.0/CmPerInch*iPixPerInchX);
         Margins.Top:=trunc(0.5/CmPerInch*iPixPerInchX);
         Margins.Right:=prn.PageWidth-trunc(0.5/CmPerInch*iPixPerInchX);
         Margins.Bottom:=prn.PageHeight-trunc(1.0/CmPerInch*iPixPerInchX);

         ARichEd.PageRect:=Margins;

         ARichEd.Print('PrintTask');
      end;
   finally
      dlg.Free();
   end;
end;

procedure TW1W2Form.printSemkaSor;
begin
  sRichEdit1.Lines.Add(TypeZavod + Time + '. S = ' + FloatToStrF(s_sred, ffFixed, 5, 1) +  ' % :');
      sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(s1 - s2), ffFixed, 5, 2) + ' % / S'' = ' + FloatToStrF(s_true, ffFixed, 5, 2) + ' %', clGreen));
      sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, 6, 2) + '/'
      + FloatToStrF(m2, ffFixed, 6, 2) + '/'
      + FloatToStrF(s1, ffFixed, 5, 2), clRed));
      sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m_1, ffFixed, 6, 2) + '/'
      + FloatToStrF(m_2, ffFixed, 6, 2) + '/'
      + FloatToStrF(s2, ffFixed, 5, 2) + #13#10, clBlue));
end;

procedure TW1W2Form.printSemkaVlaga;
begin
    sRichEdit1.Lines.Add(TypeZavod + Time + '. W = ' + FloatToStrF(w_sred, ffFixed, 5, 1) +  ' % :');
      sRichEdit1.Lines.Add(AddColorText('r = ' + FloatToStrF(abs(w1 - w2), ffFixed, 5, 2) + ' % / W'' = ' + FloatToStrF(w_sred, ffFixed, 5, 2) + ' %', clGreen));
      sRichEdit1.Lines.Add(AddColorText('1: ' + FloatToStrF(m1, ffFixed, znak + 3, znak) + '/'
      + FloatToStrF(m2, ffFixed, znak + 3, znak) + '/' + FloatToStrF(m3, ffFixed, znak + 3, znak) + '/'
      + FloatToStrF(w1, ffFixed, 5, 2), clRed));
      sRichEdit1.Lines.Add(AddColorText('2: ' + FloatToStrF(m_1, ffFixed, znak + 3, znak) + '/'
      + FloatToStrF(m_2, ffFixed, znak + 3, znak) + '/' + FloatToStrF(m_3, ffFixed, znak + 3, znak) + '/'
      + FloatToStrF(w2, ffFixed, 5, 2) + #13#10, clBlue));
end;

procedure TW1W2Form.sBitBtn1Click(Sender: TObject);
begin
  PrintRichEd(sRichEdit1, True);
end;

procedure TW1W2Form.UnderAndBold;
begin
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style+[fsUnderline];
  sRichEdit1.SelAttributes.Style := sRichEdit1.SelAttributes.Style+[fsBold];
end;

end.
